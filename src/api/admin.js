import api from './request';
import config from '../config';

const Admin = {
  addCategory: (data, isBulk) => isBulk ? api.postFormData(`categories/bulk`, data) : api.post(`categories`, data),
  categories: (query) => api.get(`${config.adminUrl}/categories`, query),
  editCategory: (id, data) => api.put(`categories/${id}`, data),
  deleteCategory: (id) => api.delete(`categories/${id}`),
  addBrand: (data, isBulk) => isBulk ? api.postFormData(`brands/bulk`, data) : api.post(`brands`, data),
  brands: (query) => api.get(`${config.adminUrl}/brands`, query),
  editBrand: (id, data) => api.put(`brands/${id}`, data),
  deleteBrand: (id) => api.delete(`brands/${id}`),
  users: (query) => api.get(`${config.adminUrl}/users`, query),
  updateUserRole: (id, role) => api.put(`${config.adminUrl}/users/${id}/role`, role),
  products: (query) => api.get(`${config.adminUrl}/products`, query),
  orders: (query) => api.get(`${config.adminUrl}/orders`, query),
  reports: (query) => api.get(`${config.adminUrl}/reports`, query),
  payouts: (query) => api.get(`${config.adminUrl}/payouts`, query),
  orderItem: (id) => api.get(`${config.adminUrl}/order/${id}`),
};

export default Admin;
