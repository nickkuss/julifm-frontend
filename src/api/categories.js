import api from './request';

const Categories = {
  get: (query) => api.get('categories'),
  adminGet: () => api.get('categories/admin'),
};

export default Categories;
