import api from './request';

const User = {
  get: () => api.get('user'),
  feed: () => api.get('user/feed'),
  getByUsername: (username, query) => api.get(`user/${username}`, query),
  update: (data) => api.put('user/', data),
  topUp: (data) => api.post('topup', data),
  topUps: (query) => api.get('user/topups', query),
  payout: (data) => api.post('payout', data),
  payouts: (query) => api.get('user/payouts', query),
  updatePassword: (data) => api.post('auth/change-password', data),
  getCart: (cartId) => api.get(`cart/${cartId}`),
  notifications: () => api.get('user/notifications'),
  bookmarks: () => api.get('user/bookmarks'),
  getCheckoutInfo: (location) => api.get(`checkout?location=${location}`),
  getItemDeliveryCost: (data) => api.post('calculate-cost', data),
  checkout: (data) => api.post(`checkout`, data),
  follow: (userId) => api.post(`user/${userId}/follow`),
  unfollow: (userId) => api.delete(`user/${userId}/unfollow`),
  getOrders: (query) => api.get('orders', query),
  updateOrder: (id, data) => api.put(`order/${id}`, data),
  updatePhoto: (data) => api.postFormData('user/update-photo', data),
};

export default User;
