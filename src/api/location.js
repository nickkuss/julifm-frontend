import api from './request';

const Location = {
  getProvinces: () => api.get('location/province'),
  getCouriers: () => api.get('location/couriers'),
  getCities: (province) => api.get('location/city', {province}),
  getSubdistrict: (city) => api.get('location/subdistrict', {city}),
};

export default Location;
