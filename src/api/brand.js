import api from './request';

const Brands = {
  get: (brand) => api.get('brands', brand ? {q: brand} : null),
  adminGet: () => api.get('brands/admin'),
};

export default Brands;
