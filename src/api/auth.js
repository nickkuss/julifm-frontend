import api from './request';

const Auth = {
  login: (data) => api.post('auth/login', data),
  fbLogin: (access_token) => api.post('auth/facebook', { access_token }),
  register: (data) => api.post('auth/register', data),
};

export default Auth;
