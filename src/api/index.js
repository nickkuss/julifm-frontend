import auth from './auth';
import product from './product';
import categories from './categories';
import brand from './brand';
import user from './user';
import location from './location';
import admin from './admin';

export default {
	auth,
	product,
	categories,
	brand,
	user,
	location,
	admin
}