import api from './request';

const Products = {
  get: (query) => api.get('products', query),
  getByCategory: (query) => api.get('products', query),
  getBySlug: (slug) => api.get(`products/${slug}`),
  getReviews: (id) => api.get(`products/${id}/reviews`),
  postReview: (id, data) => api.post(`products/${id}/review`, data),
  getById: (id) => api.get(`products/${id}`),
  submitBid: (id, amount) => api.put(`products/${id}/bid`, { amount }),
  addNew: (data) => api.postFormData('products/', data),
  shareProduct: (id) => api.post(`products/${id}/share`),
  addBookmark: (id) => api.post(`products/${id}/bookmark`),
  removeBookmark: (id) => api.delete(`products/${id}/bookmark`),
  deleteProduct: (id) => api.delete(`products/${id}`),
  updateCart: (cartId, data) => api.put(`cart/${cartId}/update`, data),
};

export default Products;
