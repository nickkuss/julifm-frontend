import React, { Component } from 'react';
import { connect } from 'react-redux';
import { InstantSearch, Stats } from 'react-instantsearch/dom';
import { withRouter } from 'react-router'
// import { InstantSearch, Hits, Pagination, SortBy, Stats } from 'react-instantsearch/dom';
import Collapse from 'reactstrap/lib/Collapse';
import Sidebar from './sidebar';
import ProductGroup from '../product/product-group';
import classnames from 'classnames';
import config from '../../config';
import categoryUtils from '../../utils/categories';
import productActions from '../../actions/product'
import Spinner from '../spinner';

import './style.css';


const mapStateToProps = (state) => ({
	categories: state.shared.categories,
	isLoading: state.listing.isLoading,
	products: state.listing.products,
	error: state.listing.error
})

const mapDispatchToProps = (dispatch) => ({
	getCategoryItems: (category) => dispatch(productActions.getProductsForCategory(category)),
	infiniteScrollAction: (category, page) => productActions.getProductsForCategoryInfinite(category, page),
})

class CatalogListing extends Component {

	constructor(props) {
		super(props);
		this.state = {
			showMobileFilter: false,
			isSmallScreen: (window.innerWidth <= 991)
		};

		this.infiniteScrollAction = this.infiniteScrollAction.bind(this);
		this.showSidebar = this.showSidebar.bind(this);
		this.hideSidebar = this.hideSidebar.bind(this);
		this.sizeListener = this.sizeListener.bind(this);
	}

	componentDidMount() {
		this.watchWindowSize();
		this.getProductsForCategory()
	}
	
	getProductsForCategory() {
		console.log('prrrrr', this.props)
		const { categories } = this.props;
		const slug = this.props.match.params.slug
	
		// return if no category data (i.e component mounted quicker). This function will be recalled on componentUpdate 
		if (!categories || !categories.length) return;
	
		const category = categoryUtils.findCategoryBySlug(categories, slug);
		if (!category || !category.name) return;
	
		this.props.getCategoryItems(category.name.toLowerCase())
	}

	componentDidUpdate(prevProps) {
		const { isLoading, match: { params: { slug } }, products, categories } = this.props;
		if (!isLoading) {
			if (!prevProps.categories && categories && !products) this.getProductsForCategory(); // component mounted before categories fetched
			else if (prevProps.match.params.slug !== slug) this.getProductsForCategory(); // component updated ie breadcrumb or category sidemenu clicked
		}
	}

	infiniteScrollAction(page) {
		const products = this.props.products;
		if (products && products.items && products.items.length) {
			return this.props.infiniteScrollAction(products.items[0].category, page);    
		}
  }

	showSidebar() {
		// only called when filter button clicked on small screens
		this.setState({showMobileFilter: true, isSmallScreen: true})
	}

	hideSidebar() {
		// only called when filter button clicked on small screens. 
		this.setState({showMobileFilter: false, isSmallScreen: true})
	}

	watchWindowSize(func) {
		window.addEventListener('resize', this.sizeListener)
	}

	sizeListener(event) {
		const currentWidth = event.target.innerWidth;
		if (currentWidth !== this.lastResizeWidth) {
			this.lastResizeWidth = currentWidth;
			this.setState({isSmallScreen: currentWidth <= 991});
		}
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.sizeListener);
	}

	shouldShowSidebar() {
		return !this.state.isSmallScreen || (this.state.isSmallScreen && this.state.showMobileFilter)
	}

	render() {
		console.log('render', this.props)
		const { slug, products, isLoading, error } = this.props;
		const category = categoryUtils.findCategoryBySlug(this.props.categories, slug);

		if (isLoading && !error) return <Spinner />
		if (!category  || !products || !products.items) return null;
		if (!products.items.length) return <p>No results for {category ? category.name : slug}</p>;

		return(
			<InstantSearch
				appId={config.algoliaAppId}
				apiKey={config.algoliaApiKey}
				indexName={config.algoliaProductIndexName}
				>
				<div className="row sidebar-wrapper pt-3">
					<Collapse className="col-lg-2 sidebar sticky-top"
						isOpen={this.shouldShowSidebar()} >
						<div className="text-right">
							<button className="btn-primary btn d-lg-none pt-1 pb-1" onClick={this.hideSidebar}>Hide</button>						
						</div>
						<Sidebar />
					</Collapse>
					<div className={classnames("col-md-12 col-lg-10", 
						{'d-none': this.state.showMobileFilter && this.state.isSmallScreen})}>
						<div className="text-right actions-bar d-flex justify-content-between justify-content-lg-ends mb-2">
							<button className="btn-primary btn d-lg-none pt-1 pb-1" onClick={this.showSidebar}>Filter</button>
							{
								category ? <h5 className="text-uppercase d-none d-lg-flex">{category.name}</h5> : (
									<h5 className="text-uppercase d-none d-lg-flex">Showing Results for '{slug}'</h5>
								)
							}
							<div className="sorter">
								<span className="action-label mr-3 text-uppercase font-weight-bold">sort by: </span>
								<select name="" id="" className="d-inline-flex price-sort">
									<option value="">Default</option>
									<option value="">Lowest Price</option>
									<option value="">Highest Price</option>
								</select>
								{/* <SortBy className="d-inline-flex price-sort"
									defaultRefinement="default_search"
									items={[
										{ value: 'default_search', label: 'Default' },
										{ value: 'price_asc', label: 'Lowest Price' },
										{ value: 'price_desc', label: 'Highest Price' }
									]}
								/> */}
							</div>
						</div>
						{
							category ? <h5 className="text-uppercase d-lg-none">{category.name}</h5> : (
								<h5 className="text-uppercase d-lg-none">Showing Results for '{slug}'</h5>
							)
						}
						<Stats />
						{/* <ProductGroup /> */}
						{ products && 
							<ProductGroup products={products} action={this.infiniteScrollAction} className="col-lg-3 col-md-4 col-sm-4 col-6 mb-3"/>
						}
						{/* <div className="row product-group">
							{items.products.map(product => <Product key={product.slug} product={product} className="col-lg-3 col-md-4 col-sm-4 col-6 mb-3"/>)}
						</div> */}
					</div>
				</div>
			</InstantSearch>			
		)
	}
}

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(CatalogListing)
) 