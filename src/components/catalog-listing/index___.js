import React, { Component } from 'react';
import { connect } from 'react-redux';
import { InstantSearch, Stats } from 'react-instantsearch/dom';
import { withRouter } from 'react-router'
import Collapse from 'reactstrap/lib/Collapse';
import Algolia from './algolia'
import Sidebar from './algolia/sidebar'
import ProductGroup from '../product/product-group';
import classnames from 'classnames';
import config from '../../config';
import categoryUtils from '../../utils/categories';
import productActions from '../../actions/product'
import Spinner from '../spinner';

import './style.css';


const mapStateToProps = (state) => ({
	categories: state.shared.categories,
	isLoading: state.listing.isLoading,
	products: state.listing.products,
	error: state.listing.error
})

const mapDispatchToProps = (dispatch) => ({
	getCategoryItems: (category) => dispatch(productActions.getProductsForCategory(category)),
	infiniteScrollAction: (category, page) => productActions.getProductsForCategoryInfinite(category, page),
})

class CatalogListing extends Component {

	constructor(props) {
		super(props);
		this.state = {
			showMobileFilter: false,
			isSmallScreen: (window.innerWidth <= 991)
		};

		this.infiniteScrollAction = this.infiniteScrollAction.bind(this);
		this.showSidebar = this.showSidebar.bind(this);
		this.hideSidebar = this.hideSidebar.bind(this);
		this.sizeListener = this.sizeListener.bind(this);
	}

	componentDidMount() {
		this.watchWindowSize();
		this.getProductsForCategory()
	}
	
	getProductsForCategory() {
		console.log('prrrrr', this.props)
		const { categories } = this.props;
		const slug = this.props.match.params.slug
	
		// return if no category data (i.e component mounted quicker). This function will be recalled on componentUpdate 
		if (!categories || !categories.length) return;
	
		const category = categoryUtils.findCategoryBySlug(categories, slug);
		if (!category || !category.name) return;
	
		this.props.getCategoryItems(category.name.toLowerCase())
	}

	componentDidUpdate(prevProps) {
		const { isLoading, match: { params: { slug } }, products, categories } = this.props;
		if (!isLoading) {
			if (!prevProps.categories && categories && !products) this.getProductsForCategory(); // component mounted before categories fetched
			else if (prevProps.match.params.slug !== slug) this.getProductsForCategory(); // component updated ie breadcrumb or category sidemenu clicked
		}
	}

	infiniteScrollAction(page) {
		const products = this.props.products;
		if (products && products.items && products.items.length) {
			return this.props.infiniteScrollAction(products.items[0].category, page);    
		}
  }

	showSidebar() {
		// only called when filter button clicked on small screens
		this.setState({showMobileFilter: true, isSmallScreen: true})
	}

	hideSidebar() {
		// only called when filter button clicked on small screens. 
		this.setState({showMobileFilter: false, isSmallScreen: true})
	}

	watchWindowSize(func) {
		window.addEventListener('resize', this.sizeListener)
	}

	sizeListener(event) {
		const currentWidth = event.target.innerWidth;
		if (currentWidth !== this.lastResizeWidth) {
			this.lastResizeWidth = currentWidth;
			this.setState({isSmallScreen: currentWidth <= 991});
		}
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.sizeListener);
	}

	shouldShowSidebar() {
		return !this.state.isSmallScreen || (this.state.isSmallScreen && this.state.showMobileFilter)
	}

	render() {
		console.log('render', this.props)
		const { slug, products, isLoading, error } = this.props;
		const category = categoryUtils.findCategoryBySlug(this.props.categories, slug);

		// if (isLoading && !error) return <Spinner />
		// if (!category  || !products || !products.items) return null;
		// if (!products.items.length) return <p>No results for {category ? category.name : slug}</p>;

		return <div className="row sidebar-wrapper"> <Algolia /> </div>

	}
}

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(CatalogListing)
) 