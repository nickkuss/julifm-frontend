import React from 'react';
import {
  RefinementList,
  RatingMenu,
  ToggleRefinement,
  HierarchicalMenu,
  SearchBox,
  NumericMenu,
  ClearRefinements
} from 'react-instantsearch/dom';
import RangeSlider from './rangeSlider';
import { SidebarWrapper } from './sidebarWrapper';

export default ({ setVoice, onCategoryClearClick, categorySlug }) => (
  <SidebarWrapper className="isoAlgoliaSidebar">
    <SearchBox translations={{ placeholder: 'Search here' }} />
    {/* <VoiceRecognition setVoice={setVoice} /> */}

    <div className="isoAlgoliaSidebarItem">
      <h3 className="isoAlgoliaSidebarTitle">Price Range</h3>
      <NumericMenu
        attribute="salePrice"
        items={[
          { end: 100000, label: '<Rp100k' },
          { start: 100000, end: 1000000, label: 'Rp100k-Rp1JT' },
          { start: 1000000, end: 5000000, label: 'Rp100-Rp5JT' },
          { start: 5000000, label: '>Rp5JT' }
        ]}
      />
    </div>

    <div className="isoAlgoliaSidebarItem">
      <h3 className="isoAlgoliaSidebarTitle" style={{ marginBottom: 10 }}>
        Price Range
      </h3>
      <RangeSlider attribute="salePrice" />
    </div>

    <div className="isoAlgoliaSidebarItem">
      <h3 className="isoAlgoliaSidebarTitle">Category</h3>
      {categorySlug &&
        <small>
          <p className="mb-1 link__display" onClick={onCategoryClearClick}>Clear filter</p>
        </small>
      }
      <RefinementList attribute="category" searchable={true} />
    </div>

    <div className="isoAlgoliaSidebarItem">
      <h3 className="isoAlgoliaSidebarTitle">Brand</h3>
      <RefinementList attribute="brand" searchable={true} />
    </div>

    {/* <div className="isoAlgoliaSidebarItem">
      <HierarchicalMenu
        attributes={[
          'hierarchicalCategories.lvl0',
          'hierarchicalCategories.lvl1',
          'hierarchicalCategories.lvl2'
        ]}
      />
    </div> */}
    <div className="isoAlgoliaSidebarItem">
      <h3 className="isoAlgoliaSidebarTitle">Rating</h3>
      <RatingMenu attribute="rating" style={{ background: '#ff0000' }} />
    </div>
    <div className="isoAlgoliaSidebarItem isoInlisne">
      <h3 className="isoAlgoliaSidebarTitle">More Filters</h3>
      <ToggleRefinement attribute="isAuthentic" label="Authentic Products" value={true} />
    </div>

    <ClearRefinements />
  </SidebarWrapper>
);
