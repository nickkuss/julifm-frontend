import React, { Component } from 'react';
import { Hits, Pagination, SortBy, Stats } from 'react-instantsearch/dom';
import Hit from './hit';
import { withRouter } from 'react-router'
import config from '../../../config'
import {
  ContentWrapper,
  TopbarWrapper,
  PaginationStyleWrapper
} from './algoliaComponent.style';

class ContentElement extends Component {
  
  scrollToTop() {
    if (!this.changer) return;
    this.changer.scrollIntoView({ behavior: 'smooth' });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.location.search !== this.props.location.search) this.scrollToTop()
  } 

  render() {

    const { ecommerceView, changeView } = this.props;
    return (
      <ContentWrapper className="isoAlgoliaContentWrapper">
        <TopbarWrapper className="isoAlgoliaTopBar">
          <Stats />
          <SortBy
            defaultRefinement={config.algoliaProductIndexName}
            items={[
              { value: config.algoliaProductIndexName, label: 'Default' },
              { value: 'price_asc', label: 'Lowest Price' },
              { value: 'price_desc', label: 'Highest Price' }
            ]}
          />
          {false && 
            <div className="isoViewChanger" ref={(el) => { this.changer = el; }}>
              <button
                type="button"
                className={
                  ecommerceView === 'gridView' ? (
                    'isoGridView active'
                  ) : (
                    'isoGridView'
                  )
                }
                onClick={() => changeView('gridView')}
              >
                <i className="ion-grid" />
              </button>
              <button
                type="button"
                className={
                  ecommerceView === 'gridView' ? (
                    'isoListView'
                  ) : (
                    'isoListView active'
                  )
                }
                onClick={() => changeView('listView')}
              >
                <i className="ion-navicon-round" />
              </button>
            </div>
          }
        </TopbarWrapper>
        <Hits hitComponent={Hit} {...this.props} />

        <PaginationStyleWrapper className="isoAlgoliaPagination">
          <Pagination showLast />
        </PaginationStyleWrapper>
      </ContentWrapper>
    );
  }
}

export default withRouter(ContentElement)