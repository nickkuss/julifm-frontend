import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connectRange } from 'react-instantsearch/connectors';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';

class Range extends Component {
  static propTypes = {
    min: PropTypes.number,
    max: PropTypes.number,
    currentRefinement: PropTypes.object,
    refine: PropTypes.func.isRequired,
    canRefine: PropTypes.bool.isRequired
  };

  state = { currentValues: { min: this.props.min, max: this.props.max } };

  componentWillReceiveProps(nextProps) {
    if (nextProps.canRefine) {
      this.setState({
        currentValues: {
          min: nextProps.currentRefinement.min,
          max: nextProps.currentRefinement.max
        }
      });
    }
  }

  onValuesUpdated = sliderState => {
    this.setState({
      currentValues: { min: sliderState.values[0], max: sliderState.values[1] }
    });
  };

  onChange = sliderState => {
    if (
      this.props.currentRefinement.min !== sliderState.min ||
      this.props.currentRefinement.max !== sliderState.max
    ) {
      this.props.refine({
        min: sliderState.min,
        max: sliderState.max
      });
    }
  };

  render() {
    const { min, max, currentRefinement } = this.props;
    const { currentValues } = this.state;
    return min !== max
      ? <div className="isoAlRangeSlider">
          <div className="isoAlRangeNumber">
            <span>{currentValues.min}</span> -
            <span>{currentValues.max}</span>
          </div>
          <InputRange
            minValue={min}
            maxValue={max}
            value={currentValues}
            onChange={this.onChange}
            />
        </div>
      : null;
  }
}

const RangeSlider = connectRange(Range);
export default RangeSlider;
