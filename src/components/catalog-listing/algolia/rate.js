import { Rate } from 'antd';
import Ratings from './rating.style';
import WithDirection from './withDirection';

const AntRating = Ratings(Rate);
const Rating = WithDirection(AntRating);
export default Rating;
