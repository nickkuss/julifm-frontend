import React, { Component } from 'react';
import { withRouter } from 'react-router'
import { InstantSearch, Configure } from 'react-instantsearch/dom';
import qs from 'qs';
import Collapse from 'reactstrap/lib/Collapse';
import classnames from 'classnames';
import Sidebar from './sidebar';
import Content from './content';
import { AlgoliaSearchConfig } from './settings';
import config from '../../../config'
import { setUrl, getInitData } from './urlSync';
import './instantSearch.css';
import AlgoliaSearchPageWrapper from './algolia.style';
import { searchInitData, getCategoryFromUrlSlug } from '../../../utils/app';
import { findCategoryBySlug } from '../../../utils/categories';


class AlgoliaMain extends Component {
  constructor(props) {
		super(props);
		this.state = {
      searchState: searchInitData(props.categories),
			showMobileFilter: false,
			isSmallScreen: (window.innerWidth <= 991)
    };
    
    const { pathname } = window.location;
    const slug = pathname.split('catalog/')[1];
	  this.categorySlug = slug && slug !== 'search' ? slug : undefined

		this.showSidebar = this.showSidebar.bind(this);
		this.hideSidebar = this.hideSidebar.bind(this);
    this.sizeListener = this.sizeListener.bind(this);
    this.onSearchStateChange = this.onSearchStateChange.bind(this)
    this.onCategoryClearClick = this.onCategoryClearClick.bind(this)
	}
  setVoice = query => {
    const searchState = {
      ...this.state.searchState,
      page: '1',
      query
    };
    this.setState({ searchState });
    setUrl(searchState);
  };

  componentDidMount() {
    this.watchWindowSize();
    if (this.categorySlug) {
      const { history, categories } = this.props
      const { searchState } = this.state
      const category = getCategoryFromUrlSlug(categories)
      if (!category) {
        history.push({
          pathname: '/catalog/search',
          search: qs.stringify({ ...searchState, query: this.categorySlug }),
          state: searchState
        })
      }
    }
	}

  watchWindowSize() {
		window.addEventListener('resize', this.sizeListener)
	}

	sizeListener(event) {
		const currentWidth = event.target.innerWidth;
		if (currentWidth !== this.lastResizeWidth) {
      this.lastResizeWidth = currentWidth;
			this.setState({isSmallScreen: currentWidth < 768});
		}
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.sizeListener);
	}

  shouldShowSidebar() {
		return !this.state.isSmallScreen || (this.state.isSmallScreen && this.state.showMobileFilter)
  }
  
  showSidebar() {
		// only called when filter button clicked on small screens
		this.setState({showMobileFilter: true, isSmallScreen: true})
	}

	hideSidebar() {
		// only called when filter button clicked on small screens. 
		this.setState({showMobileFilter: false, isSmallScreen: true})
  }
  
  onSearchStateChange(searchState, pathname) {
    console.log('search changed', searchState)
    const { history } = this.props
    this.setState({ searchState });
    history.push({
      pathname,
      search: qs.stringify(searchState),
      state: searchState
    })
  }

  onCategoryClearClick() {
    this.onSearchStateChange(this.state.searchState, '/catalog/search')
  }

  componentWillReceiveProps(nextProps) {
    const { pathname } = window.location;
	  this.categorySlug = pathname.split('catalog/')[1]
    if (nextProps.slug !== this.props.slug) {
      // we clicked on new category from subheader, etc
      this.setState({ searchState: searchInitData(nextProps.categories) })
    }
  }

  render() {
    const { searchState } = this.state
    const searchInfo = {
      appId: config.algoliaAppId,
      apiKey: config.algoliaApiKey,
      indexName: config.algoliaProductIndexName,
      searchState: searchState,
      urlSync: true,
      onSearchStateChange: this.onSearchStateChange
    };

    const { showMobileFilter, isSmallScreen } = this.state;
    if (!AlgoliaSearchConfig.appId) return null;

    const configureParams = {}
    if (this.categorySlug) {
      const categoryFromSlug = getCategoryFromUrlSlug(this.props.categories)
      configureParams.facetFilters = categoryFromSlug ? [`category:${categoryFromSlug}`] : undefined
      
      // force refinement on categories if on category url. Not doing this 
      // makes the algolia query forget and ignore the original category facetFilters
      if (searchState.refinementList) {
        if (searchState.refinementList) searchState.refinementList.category = [categoryFromSlug]
		    else searchState.refinementList = { category : [categoryFromSlug] }
      }
    }

    return (
      <AlgoliaSearchPageWrapper className={classnames("isoAlgoliaSearchPage", {
        sidebarOpen: isSmallScreen && showMobileFilter
        })} style={{ width: isSmallScreen && showMobileFilter ? '100%' : undefined }}>
        <React.Fragment>
          {!showMobileFilter && <button className="btn-primary btn d-md-none pt-1 pb-1" onClick={this.showSidebar}>Filter</button> }
          <InstantSearch {...searchInfo}>
            {this.categorySlug && 
              <Configure {...configureParams} />
            }
            <div className="isoAlgoliaMainWrapper">
              { isSmallScreen ?  
                <Collapse className="col-lg-2 sidebar sticky-top p-0"
                  isOpen={this.shouldShowSidebar()} >
                  <div className="text-right">
                    <button className="btn-primary btn d-lg-none pt-1 pb-1 mb-2" onClick={this.hideSidebar}>Hide</button>						
                  </div>
                  <Sidebar setVoice={this.setVoice} categorySlug={!!this.categorySlug} onCategoryClearClick={this.onCategoryClearClick}/>
                </Collapse > : <Sidebar setVoice={this.setVoice} setVoice={this.setVoice} categorySlug={!!this.categorySlug} onCategoryClearClick={this.onCategoryClearClick}/>
              }
              {(!isSmallScreen || (isSmallScreen && !showMobileFilter)) && <Content {...this.props} />}
              
            </div>
            {/* <Footer /> */}
          </InstantSearch>
        </React.Fragment>
      </AlgoliaSearchPageWrapper>
    );
  }
}

export default withRouter(AlgoliaMain)