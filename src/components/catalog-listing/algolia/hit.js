import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Highlight, Snippet } from 'react-instantsearch/dom';
import Rate from './rate';
import Button from './button';
import { GridListViewWrapper } from './algoliaComponent.style';
import ecommerceActions from './ecommerce/actions';
import Product from '../../product'

const { addToCart, changeViewTopbarCart } = ecommerceActions;

class Hit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addCartLoading: false
    };
  }
  render() {
    const { hit } = this.props;
    const className =
      this.props.view === 'gridView'
        ? 'isoAlgoliaGrid GridView'
        : 'isoAlgoliaGrid ListView';
    let addedTocart = false;
    this.props.productQuantity.forEach(product => {
      if (product.objectID === hit.objectID) {
        addedTocart = true;
      }
    });

		const wrapperClass = "pl-0 pr-0 pt-1"
    return <Product product={hit} wrapperClass={wrapperClass}/>
    return (
      <GridListViewWrapper className={className}>
        <div className="isoAlGridImage">
          <img alt="#" src={hit.image} />
          {!addedTocart ? (
            <Button
              onClick={() => {
                this.setState({ addCartLoading: true });
                const update = () => {
                  this.props.addToCart(hit);
                  this.setState({ addCartLoading: false });
                };
                setTimeout(update, 1500);
              }}
              type="primary"
              className="isoAlAddToCart"
              loading={this.state.addCartLoading}
            >
              <i className="ion-android-cart" />
              Add to cart
            </Button>
          ) : (
            <Button
              onClick={() => this.props.changeViewTopbarCart(true)}
              type="primary"
              className="isoAlAddToCart"
            >
              View Cart
            </Button>
          )}
        </div>
        <div className="isoAlGridContents">
          <div className="isoAlGridName">
            <Highlight attribute="name" hit={hit} />
          </div>

          <div className="isoAlGridPriceRating">
            <span className="isoAlGridPrice">${hit.price}</span>

            <div className="isoAlGridRating">
              <Rate disabled count={6} defaultValue={hit.rating} />
            </div>
          </div>

          <div className="isoAlGridDescription">
            <Snippet attribute="description" hit={hit} />
          </div>
        </div>
      </GridListViewWrapper>
    );
  }
}
function mapStateToProps(state) {
  const { view, productQuantity } = state.ecommerce.toJS();
  return {
    view,
    productQuantity
  };
}
export default connect(mapStateToProps, { addToCart, changeViewTopbarCart })(
  Hit
);
