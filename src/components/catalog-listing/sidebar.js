import React from 'react';
import { Hits } from 'react-instantsearch/dom';


import {
  RefinementList,
  RatingMenu,
	RangeSlider,
  HierarchicalMenu,
  SearchBox,
  NumericMenu,
  ClearAll
} from 'react-instantsearch/dom';


const Product2 = ({ hit }) => {
  return <div>{hit.name}</div>;
}


export default (props) => (
  props ? (
    <div>
      <div className="criteria">
        <h5 className="criteria-label">CATEGORY</h5>
        <ul className="list-group">
          <li className="list-group-item">item1</li>
          <li className="list-group-item">item2</li>
          <li className="list-group-item">item3</li>
          <li className="list-group-item">item4</li>
        </ul>
      </div>
      <hr/>
      <div className="criteria">
        <h5 className="criteria-label">PRICE</h5>
        <div className="form group"><input type="range" className="form-control"/></div>
      </div>
      <hr/>
      <div className="criteria">
        <h5 className="criteria-label">BRANDS</h5>
        <div className="form-group">
          <input type="text" className="form-control" placeholder="Search brands"/>
        </div>
        <ul className="list-group">
          <li className="list-group-item">item1</li>
          <li className="list-group-item">item2</li>
          <li className="list-group-item">item3</li>
          <li className="list-group-item">item4</li>
        </ul>
      </div>

      <hr/>

      <div className="criteria">
        <h5 className="criteria-label">RATING</h5>
        <ul className="list-group">
          <li className="list-group-item">
            <i className="ion-ios-star"></i> 1star & up
          </li>
          <li className="list-group-item">
            <i className="ion-ios-star"></i> 2star & up
          </li>
          <li className="list-group-item">
            <i className="ion-ios-star"></i> 3star & up
          </li>
          <li className="list-group-item">
            <i className="ion-ios-star"></i> 4star & up
          </li>
        </ul>
      </div>
    </div>
  ) : (
    <div>
      <Hits hitComponent={Product2} />
          <SearchBox translations={{ placeholder: 'Search here' }} />

      <div className="isoAlgoliaSidebarItem">
        <h3 className="isoAlgoliaSidebarTitle">Multi Range</h3>
        <NumericMenu
          attribute="price"
          items={[
            { end: 10, label: '<$10' },
            { start: 10, end: 100, label: '$10-$100' },
            { start: 100, end: 500, label: '$100-$500' },
            { start: 500, label: '>$500' }
          ]}
        />
      </div>

      <div className="isoAlgoliaSidebarItem">
        <h3 className="isoAlgoliaSidebarTitle" style={{ marginBottom: 10 }}>
          Slider
        </h3>
        <RangeSlider />
      </div>

      <div className="isoAlgoliaSidebarItem">
        <h3 className="isoAlgoliaSidebarTitle">Category</h3>
        <RefinementList attribute="categories" />
      </div>

      <div className="isoAlgoliaSidebarItem">
        <h3 className="isoAlgoliaSidebarTitle">Brand</h3>
        <RefinementList attribute="brand" withSearchBox />
      </div>

      <div className="isoAlgoliaSidebarItem">
        <HierarchicalMenu
          attributes={[
            'hierarchicalCategories.lvl0',
            'hierarchicalCategories.lvl1',
            'hierarchicalCategories.lvl2'
          ]}
        />
      </div>
      <div className="isoAlgoliaSidebarItem">
        <h3 className="isoAlgoliaSidebarTitle">Rating</h3>
        <RatingMenu attribute="rating" style={{ background: '#ff0000' }} />
      </div>
      <div className="isoAlgoliaSidebarItem isoInline">
        <h3 className="isoAlgoliaSidebarTitle">Toggle</h3>
        {/* <Toggle attribute="free_shipping" label="Free Shipping" /> */}
      </div>

      <ClearAll />
    </div>
  )
)