import React from 'react';
import Alert from 'reactstrap/lib/Alert';

export default  (props) => {
	return (
		<Alert color="dark" className="alert-error">
			{props.errors ? ( props.errors.errors ? 
				<ul className="list-group">
					{Object.keys(props.errors.errors).map((key, index) => {
						return <li className="list-group-item p-1" key={index}>
							{key} {props.errors.errors[key]}
						</li>
					})}
				</ul> : (props.errors.message || 'AN ERROR OCCURED')
				): 'AN ERROR OCCURED'
			}
		</Alert>
	)
}