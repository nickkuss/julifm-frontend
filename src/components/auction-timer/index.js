import React from 'react';
import Countdown from 'react-countdown-now';
import moment from 'moment';

class AuctionTimer extends React.Component {

  constructor(props) {
    super(props);

    this.auctionTimerRenderer = this.auctionTimerRenderer.bind(this)
  }
  
  auctionTimerRenderer ({ days, hours, minutes, seconds, completed }) {
    if (completed) {
      if (this.props.onComplete) this.props.onComplete()
      
      return null
    } else {
      const daysText = `${Math.floor(Number(days)) }d`;
      const hoursText = `${Number(hours)}h`;
      const minutesText = `${Number(minutes)}m`;
      const secondsText = `${Number(seconds)}s`;
  
      let text =  secondsText;
      if (Number(minutes) > 0 ) text = `${minutesText} ${text}`
      if (Number(hours) > 0) text = `${hoursText} ${text}`
      if (Number(days) > 0) text = `${daysText}  ${text}`
  
      return <span>{text}</span>
    }
  };

  render() {
    const { auction, showRealDate } = this.props;
    if (!auction) return null;
		const auctionEnded = (new Date().toISOString() >= auction.auctionEnd)

    return (
      !auction.auctionEnd || auctionEnded ? 
        <span className="text-danger font-weight-bold">Expired</span> :
        <React.Fragment>
          <Countdown
            date={moment.utc(auction.auctionEnd).valueOf()}
            renderer={this.auctionTimerRenderer}
          />
          {showRealDate &&
            <span class="ml-1 text-small" style={{ fontSize: '.5rem' }}>
              ({moment(auction.auctionEnd).format('MMM Do, H:m')})
            </span>
          }
        </React.Fragment>
    )
  }
}

export default AuctionTimer;