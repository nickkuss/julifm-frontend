import React from 'react';
import './subheader.css';
import Link from 'react-router-dom/Link';

const oneColumnChild = (category) => (
  <div>
    <div style={{ minWidth: 200 }} className="text-left">
      <ul>
        {category.subs.map(s => (
          <li key={s.slug}>
            <Link to={`/catalog/${s.slug}`}>{s.name}</Link>
            {s.subs && s.subs.length > 0 &&
              <ul id="firstlev2" style={{ left: '100%' }}>
                {s.subs.map(k => (
                  <li key={k.slug}><Link to={`/catalog/${k.slug}`}>{k.name}</Link></li>
                ))}
              </ul>
            }
          </li>
        ))}
      </ul>
    </div>
  </div>
)

const twoColumnChild = (category) => {
  const halfway = Math.ceil(category.subs.length / 2);

  return (
    <div className="holder">
      <div className="leftblock">
        <ul>
          {category.subs.slice(0, halfway).map((s, i) => (
            <li key={s.slug}>
              <Link to={`/catalog/${s.slug}`}>{s.name}</Link>
              {s.subs && s.subs.length > 0 &&
                <ul style={{ top: i === 0 ? undefined : `${(-100 * i)}%` }}>
                  {s.subs.map(k => (
                    <li key={k.slug}><Link to={`/catalog/${k.slug}`}>{k.name}</Link></li>
                  ))}
                </ul>
              }
            </li>
          ))}
        </ul>
      </div>

      <div className="rightblock">
        <ul>
          {category.subs.slice(halfway).map((s, i) => (
            <li key={s.slug}>
              <Link to={`/catalog/${s.slug}`}>{s.name}</Link>
              {s.subs && s.subs.length > 0 &&
                <ul style={{ top: i === 0 ? undefined : `${(-100 * i)}%` }}>
                  {s.subs.map(k => (
                    <li key={k.slug}><Link to={`/catalog/${k.slug}`}>{k.name}</Link></li>
                  ))}
                </ul>
              }
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}


const SubHeader = (props) => (
  <div className="menuBackground">
    <div className="center">
      <ul>
        {console.log('gdshgjfsf', props.categories, props)}
        <div className="dropDownMenu">
          {props.categories ?
            <React.Fragment>
              {props.categories.slice(0, 7).map((c, i) => (
                <li id={`category_${c.slug}`} key={c.slug}>
                  <Link to={`/catalog/${c.slug}`}>{c.name}</Link>
                  {c.subs && c.subs.length > 0 && c.subs.length < 10 && oneColumnChild(c)}
                  {c.subs && c.subs.length > 0 && c.subs.length > 10 && twoColumnChild(c)} 
                </li>
              ))}
            </React.Fragment> : null
          }
        </div>
      </ul>
    </div>
  </div>
)

export default SubHeader;