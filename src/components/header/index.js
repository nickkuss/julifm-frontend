import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'
import Navbar from 'reactstrap/lib/Navbar';
import Input from 'reactstrap/lib/Input';
import InputGroup from 'reactstrap/lib/InputGroup';
import InputGroupAddon from 'reactstrap/lib/InputGroupAddon';
import Button from 'reactstrap/lib/Button';
import classnames from 'classnames';
import headerActions from '../../actions/header'
import UncontrolledDropdown from 'reactstrap/lib/UncontrolledDropdown'
import DropdownToggle from 'reactstrap/lib/DropdownToggle'
import Dropdown from 'reactstrap/lib/Dropdown'
import DropdownMenu from 'reactstrap/lib/DropdownMenu'
import DropdownItem from 'reactstrap/lib/DropdownItem'
import Link from 'react-router-dom/Link';
import config from '../../config';
import { InstantSearch, Hits, SearchBox, Configure, Index, Snippet } from 'react-instantsearch/dom';
import { createConnector } from 'react-instantsearch';
import { connectHits } from 'react-instantsearch/connectors';
import { findCategoryByName } from '../../utils/categories';
import SubHeader from './subheader';
import storage from '../../utils/storage';


import logo from '../../assets/logo.png'
import './style.css'
import { getCategoryFromUrlSlug } from '../../utils/app';

const Products = ({ hits, onLinkClick }) => {
	if (!hits) return null;

	return hits.map(hit => 
		<li key={hit.objectID} className="search-hit ais-Hits-item">
			<Link to={'/i/' + hit.slug} onClick={onLinkClick}> 
					<img src={hit.image} alt={hit.name} />
					<p className="mb-1 font-weight-bold d-flex justify-content-between">
						<span>{hit.name}</span>
						<span className="float-right">Rp {hit.salePrice}</span>
					</p>
					<Snippet attribute="description" hit={hit}/>
			</Link>
		</li>
	)
}

class SearchResults extends React.Component {
	constructor(props) {
		super(props);

		this.state = { activeTab: 'products' }
	}

	generateTabs(path) {
		const { activeTab } = this.state;
		return (
			<div className="nav-tabs">
				<div onClick={() => this.toggle('products')}
					className={classnames('nav-item pointer', {'active': activeTab === 'products'})}>
					Product
				</div>
				<div onClick={() => this.toggle('users')}
					className={classnames('nav-item pointer', {'active': activeTab === 'users'})}>
						Profile
				</div>
			</div>
		);
	}

	toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
	}
	
	render() {
		const { status, isOpen, toggle, results, query, categories, onViewMoreClick, onLinkClick } = this.props;

		const { activeTab } = this.state;
		return (
			<Dropdown 
				isOpen={isOpen}
				tag="div"
				toggle={toggle} nav={true} inNavbar={true}>
				<DropdownMenu
					className="mt-0 typeahead-dropdown pb-4 p-2">
					{status === 'pending' && <p>searching...</p>}
					{status === 'empty' && <p>No results match your query</p>}
					{status === 'data' && 
						<React.Fragment>
							{this.generateTabs()}
							<div className="clearfix"></div>
							<div className={classnames('mt-1', {'d-none': activeTab === 'users'})}>
								{results[config.algoliaProductIndexName].facets && results[config.algoliaProductIndexName].facets.length > 0 && 
									<ul className="list-group d-block">
										{Object.keys(results[config.algoliaProductIndexName].facets[0].data).map(key => {
											const category = findCategoryByName(categories, key);
											return <li className="mb-1 list-group-item" key={key}>
												<span className="font-weight-bold">{results[config.algoliaProductIndexName].facets[0].data[key]} results </span> 
												in {category ? <Link onClick={onLinkClick} to={`/catalog/${category.slug}?query=${query}`}>{key}</Link> : <span>{key}</span>}
											</li>
										})}
									</ul>
								}
								<Products hits={results[config.algoliaProductIndexName].hits} onLinkClick={onLinkClick}/>
								{results[config.algoliaProductIndexName].nbHits > results[config.algoliaProductIndexName].hitsPerPage &&
									<div className="text-center">
										<button onClick={onViewMoreClick} className="btn btn-secondary mt-2">View More</button>
									</div>
								}
							</div>
							<div className={classnames('user-searches mt-1', {'d-none': activeTab === 'products'})}>
								{ results[config.algoliaUserIndexName].hits.map(user =>
										<p key={user.objectID} className="mb-1">
											<Link  onClick={onLinkClick} to={`/users/${user.username}`}>
												<img src={user.photo} width="50px" height="100px" className="pull-left mr-2"/>
												<span className="font-weight-bold">{user.username}</span>
											</Link>
										</p>
									)
								}
							</div>
						</React.Fragment>
					}			
				</DropdownMenu>
			</Dropdown>
		)
	}
}

SearchResults = connect(state => ({
	categories: state.shared.categories
}))(SearchResults)

const Suggestions = createConnector({
  displayName: 'CustomResults',
  getProvidedProps(props, searchState, searchResults) {
		console.log('searchState', searchState)
		console.log('searchResults', searchResults)
		let status = 'initial';
		if (searchResults.searching) status = 'pending';
		else if (searchResults.results) {
			const productsResult = searchResults.results[config.algoliaProductIndexName]
			const usersResult = searchResults.results[config.algoliaUserIndexName]
			if ((productsResult && productsResult.nbHits) || (usersResult && usersResult.nbHits)) status = 'data'
			else if (productsResult && !productsResult.nbHits && usersResult && !usersResult.nbHits) status = 'empty'
		}
		
		return { query: searchState.query, status, results: searchResults.results };
  }
})(({ status, query, results, ...props }) => {
	console.log('ladnameke')
	console.log(status);
	console.log('query', query);
	console.log('htis', results);
	console.log(props);

	if (!status || status === 'initial' || !query || query.length < 2) return null;

	return <SearchResults {...props} status={status} results={results} query={query}/>
});

const mapStateToProps = (state) => ({
	currentUser: state.shared.currentUser,
	notifications: state.shared.notifications,
	cart: state.cart.cart,
	categories: state.shared.categories,
})

const mapDispatchToProps = (dispatch) => ({
	toggleNotificationMenu: () => dispatch(headerActions.toggleNotificationMenu()),
	logout: () => dispatch(headerActions.logout()),
})

class Header extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			search: {},
			showSuggestions: false,
			selectedCategory: '',
		}
		this.onCategorySelect = this.onCategorySelect.bind(this);
		this.onSearchStateChange = this.onSearchStateChange.bind(this);
		this.onSearchSubmit = this.onSearchSubmit.bind(this);
		this.hideSuggestions = this.hideSuggestions.bind(this);
		this.toggle = this.toggle.bind(this);
		this.logout = this.logout.bind(this);
	}

	logout(e) {
		e.preventDefault();
		storage.removeToken();
		this.props.logout();
		// window reload
		window.location.pathname = '/'
	}

	toggle(value) {
		console.log('toggle', value);
		this.setState({ showSuggestions: false })
	}

	onCategorySelect(e) {
		this.setState({ 
			selectedCategory: e.target.value
		})
	}

	onSearchSubmit(e) {
		// algolia searchbox sends the form event itself
		// const input = [...e.target.children].find(el => el.type === 'search')
		e.preventDefault();
		const query = this.searchBox.state.props.currentRefinement
		const url = `/catalog/${this.state.selectedCategory || 'search'}?query=${query}`
		console.log('redirect to ', url)
		this.props.history.push(url)

		if (this.state.showSuggestions) this.hideSuggestions()
	}

	hideSuggestions() {
		this.setState({ showSuggestions: false })
	}

	getAuthenticatedLinks({ currentUser, ...props }) {
		return(
			<div className="mr-2 d-none d-md-flex d-lg-flex nav-links" >
				<UncontrolledDropdown className="nav-link-item">
					<DropdownToggle tag="span">
						<span className="nav-link-item">
							<i className="ion-person"></i>
							<span>Account</span>
						</span>
					</DropdownToggle>
					<DropdownMenu className="auth__drop__down pt-0 pb-0">
						{currentUser.role !== 'admin' && currentUser.role !== 'editor' ? 
							<React.Fragment>
								<DropdownItem className="auth__drop__down__item" tag="span"><Link to="/customer/account">Profile</Link></DropdownItem>
								{/* <DropdownItem className="auth__drop__down__item" tag="span"><Link to="/customer/history">Orders</Link></DropdownItem> */}
								<DropdownItem className="auth__drop__down__item" tag="span"><Link to="/customer/history">Transactions</Link></DropdownItem>
								<DropdownItem divider className="m-0" />
								<DropdownItem className="auth__drop__down__item" tag="span"><Link to="/customer/products">My Products</Link></DropdownItem>
								<DropdownItem className="auth__drop__down__item" tag="span"><Link to="/customer/sell">Sell</Link></DropdownItem>
							</React.Fragment> :
							<DropdownItem className="auth__drop__down__item" tag="span"><Link to="/admin/users">Office</Link></DropdownItem>

						}
						<DropdownItem className="auth__drop__down__item" tag="span"><Link to="/messages">Inbox</Link></DropdownItem>
						<DropdownItem className="auth__drop__down__item" tag="span">
							<Link to="/logout" onClick={this.logout}>Logout</Link>
						</DropdownItem>
					</DropdownMenu>
				</UncontrolledDropdown>

				<span className="d-md-none d-lg-flex">
					{this.notificationHeaderItem(true, { fontSize: 2 + 'rem' }, props)}
				</span>
				<span className="d-none d-md-flex d-lg-none">
					{this.notificationHeaderItem(false, { fontSize: 2 + 'rem' }, props)}
				</span>
		
				<Link to="/cart" className="nav-link-item">
					<i className="ion-ios-cart"></i>
					{
						props.cart && props.cart.itemCount > 0 &&
						<i className="notification-badge">{props.cart.itemCount}</i>
					}
					<span className="d-none d-lg-flex">Cart</span>
				</Link>
			</div>
		)
	};

	getUnauthenticatedLinks() {
		return (
			<div className="mr-2 d-none d-md-flex d-lg-flex nav-links">
				<Link to="/login" className="nav-link-item">
					<i className="ion-android-exit"></i>
					<span className="d-none d-lg-flex">Login</span>
				</Link>
				<Link to="/register" className="nav-link-item">
					<i className="ion-person-add"></i>
					<span className="d-none d-lg-flex">Register</span>
				</Link>
				<Link to="/cart" className="nav-link-item">
					<i className="ion-ios-cart"></i>
					<span className="d-none d-lg-flex">Cart</span>
				</Link>
				<Link to="/customer/sell" className="nav-link-item">
					<i className="ion-ios-plus"></i>
					<span className="d-none d-lg-flex">Sell</span>
				</Link>
			</div>
		);
	}
	
	getMobileSearchInput() {
		return (
			<div style={{ width: 100 + '%' }} className="d-md-none d-lg-none">
				<InputGroup className="m-0">
					<Input placeholder={config.searchInputPlaceholder} className="m-search-input" />
					<InputGroupAddon addonType="append"><Button color="secondary" className="ion-ios-search"></Button></InputGroupAddon>
				</InputGroup>
			</div>
		);
	}
	
	getDesktopSearchInput(props) {
		return (
			<div className="d-none d-md-block d-lg-block mr-2 mr-lg-5 d-search-input" style={{ width: 60 + '%' }}>
				<select name="category" id="" className="search-select float-left" onChange={this.onCategorySelect}>
					<option value="">All</option>
					{props.categories && props.categories.map(x => <option value={x.slug} key={x.slug}>{x.name}</option>)}
				</select>
				<SearchBox
					ref={(el) => this.searchBox = el}
					onSubmit={this.onSearchSubmit}
					translations={{ placeholder: config.searchInputPlaceholder}}
					showLoadingIndicator={true}
					reset={false}
					searchOnEnterKeyPressOnly={true}
				/>
				<button 
					className="search-btn btn d-none d-lg-inline-block float-left" 
					type="submit" onClick={this.onSearchSubmit}
				>Search</button>
			</div>
		)
	}
	
	notificationHeaderItem(showLabel = false, style, props) {
		return (
			<span className="nav-link-item hover-fill"
				onClick={props.toggleNotificationMenu}
				style={{ paddingRight: 10 + 'px', paddingLeft: 10 + 'px' }}>
				<i className="ion-android-notifications"></i>
				{props.notifications && props.notifications.filter(n => !n.isSeen).length > 0 &&
					<i className="ion-globe notification-badge">
						{props.notifications.filter(n => !n.isSeen).length}
					</i>
				}
				{showLabel && <span>Notifications</span>}
			</span>
		)
	}
	
	onSearchStateChange(search) {
		this.setState({ search, showSuggestions: search && search.query && search.query.length > 1 ? true : false })
	}
	
	render() {
		console.log('rendersss', this.state);
		const { props } = this;
		const { selectedCategory } = this.state
		const categoryFromSlug = getCategoryFromUrlSlug(props.categories, selectedCategory)
		const configureParams = {
			hitsPerPage:10,
			facets:['category'],
			maxValuesPerFacet:3,
			attributesToHighlight:['name', 'description'],
		}
		if (selectedCategory && categoryFromSlug) {
      configureParams.facetFilters = categoryFromSlug ? [`category:${categoryFromSlug}`] : undefined
		}
		return (
			<InstantSearch
				appId={config.algoliaAppId}
				apiKey={config.algoliaApiKey}
				indexName={config.algoliaProductIndexName}
				onSearchStateChange={this.onSearchStateChange}
				>
				<Index indexName={config.algoliaUserIndexName} ></Index>
				<Configure {...configureParams }/>
				<Navbar color="faded" light className="sticky-top">
					<span onClick={props.toggleNavbar} className="mr-2 navbar-toggler ion-navicon d-md-none d-lg-none" style={{ border: 'none', fontSize: '2.5rem', paddingLeft: 0 }}></span>
					<Link to="/" className="mr-auto ml-md-4 navbar-brand">
						<img src={logo} style={{ maxWidth: 75 }} />
					</Link>
					{this.getDesktopSearchInput(props)}
		
					{props.cart && storage.setCartId(props.cart.id)}
		
					<div className="mr-2 d-md-none d-lg-none" style={{ display: 'flex', widtdh: 80, justifyContent: 'space-between' }}>
						<UncontrolledDropdown>
							<DropdownToggle tag="span">
								<i className="ion-ios-person hover-fill" style={{ fontSize: 2 + 'rem', paddingRight: 10, paddingLeft: 10, cursor: 'pointer' }}></i>
							</DropdownToggle>
							<DropdownMenu className="auth__drop__down pt-0 pb-0">
								{props.currentUser &&
									<DropdownItem divider className="auth__drop__down__item" tag="span">
										Hi, {props.currentUser.username}
									</DropdownItem>
								}
								<DropdownItem divider className="m-0" />
								<DropdownItem className="auth__drop__down__item" tag="span"><Link to="/login">login</Link></DropdownItem>
								<DropdownItem className="auth__drop__down__item" tag="span"><Link to="/register">register</Link></DropdownItem>
							</DropdownMenu>
						</UncontrolledDropdown>
		
						{props.currentUser && this.notificationHeaderItem(false, { fontSize: '1.6rem', paddingRight: 10, paddingLeft: 10 }, props)}
		
						<Link to="/cart" className="ion-ios-cart nav-link-item" style={{ fontSize: '1.6rem', paddingRight: 10, paddingLeft: 10 }}>
							{
								props.cart && props.cart.itemCount > 0 &&
								<i className="notification-badge">{props.cart.itemCount}</i>
							}
						</Link>
						{!props.currentUser &&
							<Link to="/customer/sell" className="ion-ios-plus nav-link-item" style={{ fontSize: '1.6rem', paddingRight: 10, paddingLeft: 10 }}>
							</Link>
						}
		
					</div>
		
					{props.currentUser ? this.getAuthenticatedLinks(props) : this.getUnauthenticatedLinks()}
		
					{this.getMobileSearchInput()}
				</Navbar>
				<div className="d-nones d-md-">
					<Suggestions isOpen={this.state.showSuggestions} toggle={this.toggle} onViewMoreClick={this.onSearchSubmit} onLinkClick={this.hideSuggestions}/>
				</div>
				<div className="d-none d-md-block"><SubHeader categories={props.categories}/></div>
				
			</InstantSearch>
		)
	}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
