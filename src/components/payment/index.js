import React from 'react';
import { connect } from 'react-redux';
import UncontrolledTooltip from 'reactstrap/lib/UncontrolledTooltip';
import Spinner from '../spinner';
import ListErrors from '../list-error';
import Alert from '../alert';
import classnames from 'classnames';
import config from '../../config';
import { formatAmount } from '../../utils/app';

const bankTransferOptions = [
  { name: 'Permata Virtual Account', value: 'permata' },
  { name: 'BCA Virtual Account', value: 'bca' },
  { name: 'Mandiri Bill Payment', value: 'echannel' },
  { name: 'BNI Virtual Account', value: 'bni' }
];

const internetBankingOptions = [
  { name: 'BCA Klikpay', value: 'bca_klikpay' },
  { name: 'Klikbca', value: 'bca_klikbca' },
  { name: 'Mandiri Clickpay', value: 'mandiri_clickpay' },
  { name: 'Epay BRI', value: 'bri_epay' },
  { name: 'CIMB Clicks', value: 'cimb_clicks' },
  { name: 'Danamon Online Banking', value: 'danamon_online' }
]

// window.Veritrans.url = config.midTransUrl;
// window.Veritrans.client_key = config.midTransKey;

class Payment extends React.Component {
  constructor(props) {
    super(props);

    const today = new Date()
    let day = today.getDate()
    if (day < 10) day = '0' + day
    this.state = {
      data: {
        paymentMethod: 'card',
        bankName: 'permata',
        internetBankName: 'bca_klikpay'
      },
      cardInfo: { expMonth: String(day), expYear: String(today.getFullYear()) },
    }

    this.handleChange = this.handleChange.bind(this);
    this.submit = this.submit.bind(this);
    this.getCard = this.getCard.bind(this);
    this.callback = this.callback.bind(this);
  }

  getCard() {
    return {
      card_number: this.state.cardInfo.cardNumber,
      card_cvv: this.state.cardInfo.cardCVV,
      card_exp_month: this.state.cardInfo.expMonth,
      card_exp_year: this.state.cardInfo.expYear
    }
  }

  callback(response) {
    let state = { error: this.state.error, isLoading: false };
    if (response && response.status_code === '400' && response.validation_messages && response.validation_messages.length) {
      state.error = { message: response.validation_messages.join('\n') };
    } else if (response && (response.status_code === '200' || response.status_code === '201')) {
      state.error = null;
      state.data = { ...this.state.data, tokenId: response.token_id }
      this.props.onSubmit(state.data);
    } else {
      state.error = { message: 'an unexpected error occured' }
    }
    console.log('sss', state)
    this.setState(state)
  }

  submit(event) {
    event.preventDefault();
    event.stopPropagation(); // prevent parent form from submitting

    if (this.state.data.paymentMethod === 'card') {
      if (!this.state.cardInfo.cardName || !this.state.cardInfo.cardNumber || !this.state.cardInfo.expYear || !this.state.cardInfo.expMonth || !this.state.cardInfo.cardCVV) {
        this.setState({
          error: { message: 'all fields required' }
        })
      } else {
        window.Veritrans.url = config.midTransUrl;
        window.Veritrans.client_key = config.midTransKey;
        this.setState({ isLoading: true, error: false })
        window.Veritrans.token(this.getCard, this.callback);
      }
    } else {
      if (this.state.data.internetBankName === 'bca_klikbca' && !this.state.data.klikbcaId) {
        return this.setState({ error: { message: 'Klikbca User ID required' } })
      }
      this.props.onSubmit(this.state.data);
    }
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    if (name === 'cardName' || name === 'cardNumber' || name === 'expMonth' || name === 'expYear' || name === 'cardCVV') {
      this.setState({ cardInfo: { ...this.state.cardInfo, [name]: value }, tokenId: null });
    } else {
      this.setState({ data: { ...this.state.data, [name]: value }, error: null });
    }
  }

  renderWalletBalance(type = 'wallet') {
    const { user, disabled, amount } = this.props;
    const { data: { paymentMethod } } = this.state;
    const htmlId = type === 'wallet' ? 'payWithWallet' : 'payWithBalance'
    const insufficient = (!user[type] || !amount || (user[type] < amount))

    return (
      <div className="form-group">
        <input type="radio" id={htmlId} value={type}
          checked={paymentMethod === type}
          disabled={disabled || insufficient}
          name="paymentMethod" onChange={this.handleChange} />

        {insufficient &&
          <UncontrolledTooltip placement="right" target={`${htmlId}Label`}>
            You need to have at least {formatAmount(amount, 'IDR')} to use this option
          </UncontrolledTooltip>
        }

        <label htmlFor={htmlId} id={`${htmlId}Label`}>
          Use my {type} 
          {insufficient &&
            <span className="text-danger text-small ml-2" style={{ fontSize: '.8rem' }}>
              Insufficient amount in {type}
            </span>
          }
        </label>
        <span className="float-right"> {formatAmount(user[type])}</span>
      </div>
    )
  }

  render() {
    const { user, bodyClass, footerClass, disabled, showToggler, toggleChangeMethod, excludeWallet, excludeBalance } = this.props;
    if (!user) return <Spinner />;

    const { data: { paymentMethod }, cardInfo, error, isLoading } = this.state;

    let months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
      .map(i => <option key={i} value={i}>{i}</option>);
    let years = ['2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027']
      .map(i => <option key={i} value={i}>{i}</option>);
    return (
      <React.Fragment>
        {error && !isLoading && <Alert options={{ autoClose: false, type: 'error' }}><ListErrors {...error} /></Alert>}
        {isLoading && <Alert options={{ autoClose: false, type: 'default' }}
          className='loader'><Spinner /></Alert>}
        <div className="card">
          <p className="card-header font-weight-bold">
            <span className="font-weight-bold">Payment Method</span>
            {showToggler &&
              <span className="underline pointer float-right"
                onClick={toggleChangeMethod}>Change Method</span>
            }
          </p>
          <div className={classnames('card-body', {
            [bodyClass]: bodyClass
          })}>
            <React.Fragment>
              {!excludeWallet && this.renderWalletBalance('wallet')}
              {!excludeBalance && this.renderWalletBalance('balance')}
              <div className="form-group">
                <input type="radio" id="payWithCard" value="card"
                  checked={paymentMethod === 'card'} disabled={disabled}
                  name="paymentMethod" onChange={this.handleChange} />
                <label htmlFor="payWithCard">Pay with card</label>
              </div>
              <div className="form-group">
                <input type="radio" id="payWithBank" value="bank"
                  checked={paymentMethod === 'bank'} disabled={disabled}
                  name="paymentMethod" onChange={this.handleChange} />
                <label htmlFor="payWithBank">Bank Transfer</label>
              </div>
              <div className="form-group">
                <input type="radio" id="payWithInternet" value="internet"
                  checked={paymentMethod === 'internet'} disabled={disabled}
                  name="paymentMethod" onChange={this.handleChange} />
                <label htmlFor="payWithInternet">Internet Banking</label>
              </div>
            </React.Fragment>
            <React.Fragment>
              <form className={paymentMethod !== 'card' ? 'd-none' : ''}>
                <fieldset disabled={disabled}>
                  <div className="row">
                    <div className="col-md-6">
                      <label htmlFor="cardName">Card Name</label>
                      <input type="text" id="cardName" name="cardName" onChange={this.handleChange} />
                    </div>
                    <div className="col-md-6">
                      <label htmlFor="cardNumber">Card Number</label>
                      <input type="text" id="cardNumber" name="cardNumber" maxLength="19" pattern="0-9" onChange={this.handleChange} />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor="expMonth">Month</label>
                      <select name="expMonth" id="expMonth" value={cardInfo.expMonth}
                        onChange={this.handleChange}>{months}</select>
                    </div>
                    <div className="col-4">
                      <label htmlFor="expYear">Year</label>
                      <select name="expYear" id="expYear" value={cardInfo.expYear}
                        onChange={this.handleChange}>{years}</select>
                    </div>
                    <div className="col-4">
                      <label htmlFor="cardCVV">
                        Card CVV
                        <i style={{ position: 'absolute', left: 75 }}
                          className="ion-help-circled cursor ml-1" id="cvvHelp"></i>
                        <UncontrolledTooltip placement="right" target="cvvHelp">
                          Your CVV code is a 3-digit number located on the back of your card
                        </UncontrolledTooltip>
                      </label>
                      <input type="number" id="cardCVV" name="cardCVV" maxLength="3" onChange={this.handleChange} />
                    </div>
                  </div>
                </fieldset>
              </form>

              <form className={paymentMethod !== 'bank' ? 'd-none' : ''}>
                <fieldset disabled={disabled}>
                  <div className="row">
                    <div className="col-md-12">
                      <label htmlFor="cardName">Select Bank</label>
                      <select name="bankName" id="" onChange={this.handleChange}>
                        {bankTransferOptions.map(b => <option key={b.value} value={b.value}>{b.name}</option>)}
                      </select>
                    </div>
                  </div>
                </fieldset>
              </form>

              <form className={paymentMethod !== 'internet' ? 'd-none' : ''}>
                <fieldset disabled={disabled}>
                  <div className="row">
                    <div className="col-md-12">
                      <label htmlFor="cardName">Choose Method</label>
                      <select name="internetBankName" id="" onChange={this.handleChange}>
                        {internetBankingOptions.map(b => <option key={b.value} value={b.value}>{b.name}</option>)}
                      </select>
                      {this.state.data.internetBankName === 'bca_klikbca' &&
                        <input type="text" id="klikbcaId" name="klikbcaId" onChange={this.handleChange} className="mt-1" placeholder="Enter Klikbca User ID" />
                      }
                    </div>
                  </div>
                </fieldset>
              </form>

            </React.Fragment>
          </div>
          <div className={classnames('card-footer text-right p-1', {
            [footerClass]: footerClass
          })}>
            <button className="btn btn-secondary text-uppercase btn-block"
              onClick={this.submit} disabled={disabled}>Proceed</button>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.shared.currentUser
})

export default connect(mapStateToProps)(Payment);