import React from 'react';
import { connect } from 'react-redux';
import Link from 'react-router-dom/Link';
import DocumentTitle from 'react-document-title';
import cartActions from '../../actions/cart';
import ListErrors from '../list-error';
import Alert from '../alert';
import { formatAmount, pageTitleText } from '../../utils/app';
import Spinner from '../spinner';
import './style.css';


export default connect((state) => ({
  cart: state.cart
}), (dispatch) => ({
  updateCart: (data) => dispatch(cartActions.updateCart(data)),
}))(
  ({ cart: { cart, isLoading, error }, ...props }) => (
    <DocumentTitle title={pageTitleText('Cart')}>
      <React.Fragment>
        {(isLoading || (error && !isLoading)) &&
          <Alert options={{ autoClose: false, type: error ? 'error' : 'default',
            }} className={isLoading ? 'loader' : ''}>
            {isLoading ? <Spinner /> : <ListErrors {...error} />}
          </Alert> 
        }
        {cart  ?
          <div className="card" id="cart">
            <div className="card-body">
              <h5>
                Cart ({cart.items.length})
                {cart.items.length > 0 &&
                  <Link to="/checkout" className="btn btn-secondary float-right btn-sm">CHECKOUT</Link>
                }
              </h5>
              {!cart.items.length &&
                <div className="text-center">
                  <p className="text-danger font-weight-bold" style={{ fontSize: '2rem' }}>
                    Cart is empty
                  </p>
                  <p class="font-weight-bold link__display" style={{ fontSize: '1rem' }}>
                    <Link to="/">Go Home</Link>
                  </p>
                </div>
              }
              <ul className="cart-item-detail">
                  {cart.items.map(item => (
                    <li className="item_" key={item.id}>
                      <span className="item">
                        <Link to={'/i/' + item.url}>
                          <img src={item.image} style={{ backgroundImage: 'url(' + item.image + ')' }} alt={item.name}/>
                          <b className="title">{item.name}</b>
                        </Link>
                      </span>
                      <span className="info">
                        Sold by <Link to={"/user/" + item.sellerName}>{item.sellerName}</Link>
                      </span>
                      <span className="left_stock">Only 1 left</span>
                      <span className="price ">{formatAmount(item.price)}</span>
                      <span className="qty">
                        <select disabled={item.isAuction}
                          onChange={(e) => props.updateCart({ product: item.id, quantity: e.target.value})}
                          className="select-boxes2" value={item.quantity}> 
                          {
                            [1,2,3,4,5,6,7,8,9,10].map(i => 
                              <option value={i} key={i} >{i}</option>
                            )
                          }
                        </select>
                      </span>
                      <span className="unavailable  invisible"></span>
                      {item.courier && <span className="float-left">Ships via {item.courier}</span>}
                      <p className="action float-right mb-0">
                        {/* <span className=" action-item wishlist wishlist-item_">Save for later</span> */}
                        {item.isAuction ?
                          <span className="text-success action-item text-mute">Auctioned item</span> :
                          <span 
                            onClick={() => {
                              if (window.confirm('Confirm removing this item?'))
                                props.updateCart({ product: item.id, quantity: 0})
                            }}
                            className="action-item remove remove-item_ text-danger link-display"
                          >Remove</span>
                        }
                      </p>
                    </li>
                  ))}
              </ul>
              <span className="float-right font-weight-bold">
                  <span>Subtotal: {formatAmount(cart.totalPrice)}</span>
              </span>

              <hr/>
              {cart.items.length > 0 && <Link to="/checkout" className="btn btn-secondary btn-block">CHECKOUT</Link>}
            </div>
          </div > : (<p>loading....</p>)
        }
      </React.Fragment >
    </DocumentTitle>
  ))