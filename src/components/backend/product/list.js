import React, { Component } from 'react';
import { connect } from 'react-redux'
import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import paginationFactory from 'react-bootstrap-table2-paginator';
import filterFactory from 'react-bootstrap-table2-filter';
import Spinner from '../../spinner';
import Link from 'react-router-dom/Link';
import config from '../../../config';
import productActions from '../../../actions/product';
import userActions from '../../../actions/user';
import { formatDate, formatAmount } from '../../../utils/app';
import { formatWeight } from '../../../utils/product';

const mapStateToProps = (state) => ({
	userProducts: state.product.userProducts,
	userBookmarks: state.shared.bookmarks,
	currentUser: state.shared.currentUser
})

const mapDispatchToProps = (dispatch) => ({
	getUserProducts: (id, query) => dispatch(productActions.getProductsForUser(id, query)),
	deleteProduct: (id) => dispatch(productActions.deleteProduct(id)),
	getUserBookmarks: (id) => dispatch(userActions.getBookmarks())
})

class ProductList extends Component {

	constructor(props) {
		super(props);
		this.state = { selected: [] };

		this.onPageChange = this.onPageChange.bind(this);
		this.onTableChange = this.onTableChange.bind(this);

		const bookmarkColumns = ['id', 'images', 'salePrice', 'action', 'savedAt']

		this.columns = [
			{
				dataField: 'id',
				text: '#',
				formatter: this.idFormatter
			},
			{
				dataField: 'images.0.url',
				text: 'Photo',
				formatter: this.imageFormatter
			},
			{
				dataField: 'name',
				text: 'Name',
				sort: true,
				formatter: (cell, row) => {
					return <Link to={'/i/' + row.slug}>{cell}</Link>
				},
				headerSortingClasses: this.headerSortingClasses,
				// filter: textFilter(),
				onSort: (field, order) => {
				}
			},
			{
				dataField: 'salePrice',
				text: 'Price',
				sort: true,
				headerSortingClasses: this.headerSortingClasses,
				formatter: this.priceFormatter
			},
			{
				dataField: 'quantity',
				text: 'Qty'
			},
			{
				dataField: 'weight',
				text: 'Weight',
				formatter: cell => <span>{formatWeight(cell) || '-'}</span>
			},
			{
				dataField: 'meta.averageRating',
				text: 'Rating',
				formatter: (cell, row) => {
					return <span><i className="ion-star mr-1" style={{ color: config.starRatingColor }}></i> {cell}</span>
				}
			},
			{
				dataField: 'meta.sellCount',
				text: 'Sales',
				sort: true,
				headerSortingClasses: this.headerSortingClasses,
			},
			{
				dataField: 'saleFormat',
				text: 'Format'
			},
			{
				dataField: 'isActive',
				text: 'Active',
				formatter: (cell, row) => {
					return (
						row.isActive ? <span>Yes</span> : <span>No</span>
					)
				}
			},
			{
				dataField: 'isFeatured',
				text: 'Featured',
				formatter: (cell, row) => {
					return (
						row.isFeatured ? <span>Yes</span> : <span>No</span>
					)
				}
			},
			{
				dataField: 'createdAt',
				text: 'Uploaded',
				sort: true,
				headerSortingClasses: this.headerSortingClasses,
				formatter: (cell, row) => {
					return (
						<span>{formatDate(row.createdAt)}</span>
					)
				}
			},
			{
				dataField: 'action',
				text: 'Action',
				formatter: this.actionFormatter.bind(this)
			}
		];

		this.bookmarkColumns = this.columns.filter(col => {
			return bookmarkColumns.indexOf(col.dataField.split('.')[0]) > -1
		})

		this.selectRow = {
			mode: 'checkbox',
			clickToSelect: true,
			selected: this.state.selected,
			onSelect: this.handleOnSelect,
			onSelectAll: this.handleOnSelectAll
		};
	}

	idFormatter(cell, row, rowIndex, formatExtraData) {
		console.log('abcdedddf', formatExtraData)
		return (
			<span>{rowIndex + 1} </span>
		);
	}
	
	imageFormatter(cell, row, rowIndex, formatExtraData) {
		return (
			<Link to={'/' + row.slug}>
				<img src={cell} style={{ maxWidth: '50px' }} alt={row.name}/>
			</Link>
		);
	}
	
	priceFormatter(cell, row) {
		if (row.onSale) {
			return (
				<span>
					<strong style={{ color: 'red' }}> {formatAmount(cell)} (Sales!!)</strong>
				</span>
			);
		}
	
		return (
			<span>{formatAmount(cell)}</span>
		);
	}
	
	actionFormatter(cell, row, rowIndex, formatExtraData) {
		const isBookmarks = this.props.tableType === 'bookmark'
		return (
			<div className="d-flex">
				{!isBookmarks && 
					<Link to={'/customer/product/edit/' + row.slug}
						className="btn btn-default btn-sm mr-1" style={{ width: '30px' }}>
						<i className="ion-edit pointer"></i>
					</Link>
				}
				<a className="btn btn-default btn-sm" 
					onClick={() => this.deleteProduct(row.id)}
					style={{ width: '30px' }}>
					<i className="ion-trash-b pointer"></i>
				</a>
				{/* {isBookmarks &&
					row.auction ? <button className="btn btn-default btn-sm" onClick={this.}>Bid</button> :
						<button className="btn btn-default btn-sm">Buy</button>
				} */}
			</div>
		);
	}
	
	headerSortingClasses(column, sortOrder, isLastSorting, colIndex) {
		return sortOrder === 'asc' ? 'ion-chevron-up' : 'ion-chevron-down'
	}
	
	noDataIndication() {
		return (
			<div>
				<p>No results found.</p>
				<p>
					<Link to="sell" className="font-weight-bold underline">Add New product</Link>
				</p>
			</div>
		)
	}

	handleBtnClick = () => {
		if (!this.state.selected.includes(2)) {
			this.setState(() => ({
				selected: [...this.state.selected, 2]
			}));
		} else {
			this.setState(() => ({
				selected: this.state.selected.filter(x => x !== 2)
			}));
		}
	}

	handleOnSelect = (row, isSelect) => {
		if (isSelect) {
			this.setState(() => ({
				selected: [...this.state.selected, row.id]
			}));
		} else {
			this.setState(() => ({
				selected: this.state.selected.filter(x => x !== row.id)
			}));
		}
	}

	handleOnSelectAll = (isSelect, rows) => {
		const ids = rows.map(r => r.id);
		if (isSelect) {
			this.setState(() => ({
				selected: ids
			}));
		} else {
			this.setState(() => ({
				selected: []
			}));
		}
	}

	componentDidMount() {
		if (this.props.tableType === 'bookmark') {
			this.props.getUserBookmarks(this.props.currentUser.id)
		} else {
			this.props.getUserProducts(this.props.currentUser.id, {})
		}
	}

	onPageChange(page) {
		console.log('page change')
		if (this.props.tableType === 'bookmark') {
			this.props.getUserBookmarks(this.props.currentUser.id, { page })
		} else {
			this.props.getUserProducts(this.props.currentUser.id, { page })
		}
	}

	onTableChange(type, newState) {
		console.log('table changed', type, newState)
		const { tableType } = this.props
		if (type === 'sort') {
			tableType === 'bookmark' ?
				this.props.getUserBookmarks(this.props.currentUser.id, { 
					page: newState.page,
					sort: newState.sortField, direction: newState.sortOrder
				 }) :
				this.props.getUserProducts(this.props.currentUser.id, { 
					page: newState.page,
					sort: newState.sortField, direction: newState.sortOrder
				}) 
		}
	}

	deleteProduct(id) {
		if (window.confirm('Do you really want to delete this item')) {
			this.props.deleteProduct(id);
		}
	}

	render() {
		let products, columns;
		const isBookmarks = this.props.tableType === 'bookmark';
		console.log('thsss', isBookmarks)
		if (isBookmarks) {
			columns = this.bookmarkColumns
			products = this.props.userBookmarks;
		} else {
			columns = this.columns
			products = this.props.userProducts;
		}
		if (!products) return <Spinner />;

		const paginationOptions = {
			// showTotal: true,
			hideSizePerPage: true,
			hidePageListOnlyOnePage: true,
			onPageChange: this.onPageChange,
			sizePerPage: products.perPage || products.count,
			totalSize: products.totalResults,
			page: products.page
		}

		return (
			<div className="pt-3">
				<h5>
					{isBookmarks ? 'Bookmarked Items' : 'My Uploaded Products'} ({products.totalResults})
				</h5>
				<BootstrapTable keyField='id' filter={filterFactory()} hover striped condensed
					className='table-responsive'
					// defaultSorted={[{
					// 	dataField: 'createdAt',
					// 	order: 'desc'
					// }]}
					defaultSortDirection='asc'
					remote={ { pagination: true, filter: false, sort: true } }
					pagination={ paginationFactory(paginationOptions) }
					data={products.items} columns={columns}
					onTableChange={this.onTableChange}
					noDataIndication={this.noDataIndication} />
			</div>
		)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductList)
