import React, { Component } from 'react';

class CategoryDropdown extends Component {

	constructor(props) {
		super(props);

		this.onSelect = this.onSelect.bind(this);
		this.onClick = this.onClick.bind(this);
		this.onChange = this.onChange.bind(this);
	}

	onSelect() {
		console.log('here', this.props.item)
		this.props.onSelect()
		console.log('here', this.props.item)
	}

	onClick() {
		console.log('here', this.props.item)
		this.props.onSelect()
		console.log('here', this.props.item)
	}

	onChange() {
		console.log('here', this.props.item)
		this.props.onSelect()
		console.log('here', this.props.item)
	}

	render() {
		return (
			<option value={this.props.option.name}
				onSelect={this.onSelect}
				onChange={this.onChange}
				onClick={this.onClick}>
				{this.props.option.name}
			</option>
		)
	}
}

export default CategoryDropdown;