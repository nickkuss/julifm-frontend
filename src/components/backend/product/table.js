import React, { Component } from 'react';
import { connect } from 'react-redux'
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory from 'react-bootstrap-table2-filter';
import Spinner from '../../spinner';
import classnames from 'classnames';
import Link from 'react-router-dom/Link';
import productActions from '../../../actions/product';
import { formatDate } from '../../../utils/app';
import { formatWeight } from '../../../utils/product';


const mapStateToProps = (state) => ({
	userProducts: state.product.userProducts,
	isSharedLoading: state.shared.isLoading,
	currentUser: state.shared.currentUser
})

const mapDispatchToProps = (dispatch) => ({
	getUserProducts: (id, query) => dispatch(productActions.getProductsForUser(id, query)),
	getUserBookmarks: (id) => dispatch(productActions.getBookmarksForUser(id))
})

class ProductListTable extends Component {

	constructor(props) {
		super(props);
		this.state = { selected: [] };

		const bookmarkColumns = ['id, images, salePrice', 'savedAt']

		this.columns = [
			{
				dataField: 'id',
				text: '#',
				formatter: this.idFormatter
			},
			{
				dataField: 'images.0.url',
				text: 'Photo',
				formatter: this.imageFormatter
			},
			{
				dataField: 'name',
				text: 'Name',
				sort: true,
				formatter: (cell, row) => {
					return <Link to={'/' + row.slug}>{cell}</Link>
				},
				headerSortingClasses: this.headerSortingClasses,
				// filter: textFilter(),
				onSort: (field, order) => {
				}
			},
			{
				dataField: 'salePrice',
				text: 'Price',
				formatter: this.priceFormatter
			},
			{
				dataField: 'quantity',
				text: 'Qty'
			},
			{
				dataField: 'weight',
				text: 'Weight',
				formatter: cell => {
					const value = formatWeight(cell)
					return <span className={classnames({ 'text-center': !value })}>{value || '-'}</span>
				}
			},
			{
				dataField: 'meta.averageRating',
				text: 'Rating',
				formatter: (cell, row) => {
					return <span><i className="ion-ios-star"></i> {cell}</span>
				}
			},
			{
				dataField: 'meta.sellCount',
				text: 'Sales'
			},
			{
				dataField: 'saleFormat',
				text: 'format'
			},
			{
				dataField: 'isFeatured',
				text: 'Featured',
				formatter: (cell, row) => {
					return (
						row.isFeatured ? <span>Yes</span> : <span>No</span>
					)
				}
			},
			{
				dataField: 'createdAt',
				text: 'Uploaded',
				formatter: (cell, row) => {
					return (
						<span>{formatDate(row.createdAt)}</span>
					)
				}
			},
			{
				dataField: 'action',
				text: 'Action',
				formatter: this.actionFormatter
			}
		];

		this.bookmarkColumns = this.columns.filter(col => {
			return bookmarkColumns.indexOf(col.dataField.split('.')[0] > -1)
		})

		this.selectRow = {
			mode: 'checkbox',
			clickToSelect: true,
			selected: this.state.selected,
			onSelect: this.handleOnSelect,
			onSelectAll: this.handleOnSelectAll
		};
	}

	idFormatter(cell, row, rowIndex, formatExtraData) {
		return (
			<span>{rowIndex + 1} </span>
		);
	}
	
	imageFormatter(cell, row, rowIndex, formatExtraData) {
		return (
			<Link to={'/' + row.slug}>
				<img src={cell} style={{ maxWidth: '50px' }} alt={row.name}/>
			</Link>
		);
	}
	
	priceFormatter(cell, row) {
		if (row.onSale) {
			return (
				<span>
					<strong style={{ color: 'red' }}>Rp {cell} (Sales!!)</strong>
				</span>
			);
		}
	
		return (
			<span>Rp {cell}</span>
		);
	}
	
	actionFormatter(cell, row, rowIndex, formatExtraData) {
		return (
			<div className="d-flex">
				<Link to={'/customer/product/edit/' + row.slug}
					className="btn btn-default btn-sm mr-1" style={{ width: '30px' }}>
					<i className="ion-edit pointer"></i>
				</Link>
				<a className="btn btn-default btn-sm" style={{ width: '30px' }}>
					<i className="ion-trash-b pointer"></i>
				</a>
			</div>
		);
	}
	
	headerSortingClasses(column, sortOrder, isLastSorting, colIndex) {
		return sortOrder === 'asc' ? 'ion-chevron-up' : 'ion-chevron-down'
	}
	
	noDataIndication() {
		return (
			<div>
				<p>No results found.</p>
				<p>
					<Link to="sell" className="font-weight-bold underline">Add New product</Link>
				</p>
			</div>
		)
	}

	handleBtnClick = () => {
		if (!this.state.selected.includes(2)) {
			this.setState(() => ({
				selected: [...this.state.selected, 2]
			}));
		} else {
			this.setState(() => ({
				selected: this.state.selected.filter(x => x !== 2)
			}));
		}
	}

	handleOnSelect = (row, isSelect) => {
		if (isSelect) {
			this.setState(() => ({
				selected: [...this.state.selected, row.id]
			}));
		} else {
			this.setState(() => ({
				selected: this.state.selected.filter(x => x !== row.id)
			}));
		}
	}

	handleOnSelectAll = (isSelect, rows) => {
		const ids = rows.map(r => r.id);
		if (isSelect) {
			this.setState(() => ({
				selected: ids
			}));
		} else {
			this.setState(() => ({
				selected: []
			}));
		}
	}

	componentDidMount() {
		if (this.props.tableType === 'owner') {
			this.props.getUserProducts(this.props.currentUser.id, {})
		} else {
			this.props.getUserBookmarks(this.props.currentUser.id)
		}
	}

	render() {
		const userProducts = this.props.userProducts
		if (!userProducts) return <Spinner />;

		const columns = this.props.tableType === 'owner' ? this.columns : this.bookmarkColumns
		return (
			<div className="pt-3">
				<h5>My Uploaded Products</h5>
				<BootstrapTable keyField='id' filter={filterFactory()} hover striped condensed
					data={userProducts.items} columns={columns}
					noDataIndication={this.noDataIndication} />
			</div>
		)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductListTable)
