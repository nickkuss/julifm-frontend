import React, { Component } from 'react';
import Spinner from '../../spinner';
import Alert from '../../alert';
import ListErrors from '../../list-error';
import ProductForm from './form';
import productActions from '../../../actions/product'

class ProductEdit extends Component {

	constructor(props) {
    super(props);
    this.state = {}
	}

  componentDidMount() {
    const slug = window.location.pathname.split('/').pop();
    productActions.getProductBySlug(slug)
      .then(product => this.setState({ product }))
      .catch(error => this.setState({ error }))
  }
  
	render() {
    const { product, error } = this.state;

    if (!product && !error) return <Spinner />
    console.log(this.state)
		return (
      <div>
        {error && <Alert type="danger"><ListErrors {...error} /></Alert>}
        {product && <ProductForm isEdit={true} product={product}/> }
      </div>
		)
	}
}

export default ProductEdit;