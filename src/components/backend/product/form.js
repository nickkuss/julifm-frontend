import React, { Component } from "react";
import UncontrolledTooltip from 'reactstrap/lib/UncontrolledTooltip';
import { connect } from 'react-redux';
import { EditorState } from 'draft-js';
import classnames from 'classnames';
import DocumentTitle from 'react-document-title';
import moment from 'moment';
import Redirect from 'react-router-dom/Redirect';
// import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import categoryActions from '../../../actions/categories';
import productActions from '../../../actions/product';
import { findCategoryById } from '../../../utils/categories';
import Spinner from '../../spinner';
import AddressFormModal from "../profile/address-form-modal";
import Alert from '../../alert';
import ListErrors from '../../list-error';
import Select from 'react-select';
import categoryUtils from '../../../utils/categories'
import 'react-select/dist/react-select.css';
import config from "../../../config";
import api from "../../../api";
import { isObject, pageTitleText } from "../../../utils/app";
import './product-style.css';
import NumberFormat from 'react-number-format';
import NumericInput from 'react-numeric-input';
import Dropzone from 'react-dropzone'


const Fragment = React.Fragment;

const mapStateToProps = (state) => ({
	categories: state.shared.categories,
	isSharedLoading: state.shared.isLoading,
	currentUser: state.shared.currentUser,
	isLoading: state.product.isLoading,
	product: state.product.product,
	error: state.product.error,
	couriers: state.location.couriers
})

const mapDispatchToProps = (dispatch) => ({
	getCategories: () => dispatch(categoryActions.getCategories()),
	addNewProduct: (product) => dispatch(productActions.addNewProduct(product)),
	onUnload: () => dispatch(productActions.onFormUnload()),
})

const thumbsContainer = {
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  marginTop: 16
};

const thumb = {
  display: 'inline-flex',
  borderRadius: 2,
  border: '1px solid #eaeaea',
  marginBottom: 8,
  marginRight: 8,
  width: 100,
  height: 100,
  padding: 4,
  boxSizing: 'border-box'
};

const thumbInner = {
  display: 'flex',
  minWidth: 0,
  overflow: 'hidden'
}

const img = {
  display: 'block',
  width: 'auto',
  height: '100%'
};


class ProductForm extends Component {

	constructor(props) {
		super(props);
		this.state = {
			product: {
				name: '',
				salePrice: 0,
				OriginalPrice: 0,
				onSale: false,
				isAuction: false,
				content: '',
				showAuctionTarget: false,
				isActive: true,
				inStock: true,
				isAuthentic: false,
				images: [],
				targetPrice: '',
				sku: '',
				saleFormat: 'fixed',
				condition: 'good',
				summary: '',
				description: '',
				brand: '',
				quantity: 1,
				auctionEnd: moment().add(1, 'day').toDate(),
				weight: '1000',
				location: '',
				courierDelivery: false,
				selfDelivery: false,
				category: props.currentUser.lastCategory || '',
				couriers: [],
				attributes: {}
			},
			inputChanged: false,
			canShowError: false,
			useLastCategory: false,
			selectedCategory: null,
			selectedSubCategory: null,
			sizes: null,
			formErrors: null,
			selectedBrand: null,
			disableCourierSelect: false,
			couriers: [], // multi-select lib doesn't work for nested fields
			editorState: EditorState.createEmpty(),

		}
		// bind events
		this.handleInputChange = this.handleInputChange.bind(this);
		this.onDateTimeChange = this.onDateTimeChange.bind(this);
		this.onEditorStateChange = this.onEditorStateChange.bind(this);
		this.handleBrandChange = this.handleBrandChange.bind(this);
		this.handleSizeChange = this.handleSizeChange.bind(this);
		this.handleCategoryChange1 = this.handleCategoryChange1.bind(this);
		this.handleCategoryChange2 = this.handleCategoryChange2.bind(this);
		this.handleCategoryChange3 = this.handleCategoryChange3.bind(this);
		this.handleCourierChange = this.handleCourierChange.bind(this);
		this.handleQuantityChange = this.handleQuantityChange.bind(this)
		this.handleWeightChange = this.handleWeightChange.bind(this)
		this.updateCouriers = this.updateCouriers.bind(this);
		this.showAddressModal = this.showAddressModal.bind(this);
		this.onModalClose = this.onModalClose.bind(this);
		this.onContentChange = this.onContentChange.bind(this);
		this.handleImageChange = this.handleImageChange.bind(this);
		this.isFormInvalid = this.isFormInvalid.bind(this);
		this.submitForm = this.submitForm.bind(this);
		this.setDomEditorRef = this.setDomEditorRef.bind(this);
	}

	componentDidMount() {
		if (!this.props.categories && !this.props.isSharedLoading) {
			this.props.getCategories()
		}
	}

	componentWillUnmount() {
		this.props.onUnload()
		// revoke the data uris to avoid memory leaks
		const { imagePreview } = this.state
		if (imagePreview && imagePreview.length) {
			imagePreview.forEach(image => URL.revokeObjectURL(image))
		}
	}

	static getDerivedStateFromProps(nextProps, state) {
		console.log(nextProps, state)
		const { isEdit, product } = nextProps;
		const obj = {canShowError: false}

		if (isEdit && product) {
			const newProduct = {};
			Object.keys(product).forEach(key => newProduct[key] = product[key])
			obj.useLastCategory = false;
			obj.product = {...state.product, ...newProduct  }
		} else {
			if (nextProps.currentUser && !state.inputChanged) {
				const location = nextProps.currentUser.addresses.find(a => a.isDefault);
				if (location && location.id !== state.product.location) obj.product = {...state.product, location: location.id}
				// obj.useLastCategory = nextProps.currentUser.lastCategory ? true : false  ///TURNED OFF useLastCategory by Default////
			} else obj.inputChanged = false;
		}
		if (!state.inputChanged && !nextProps.isLoading && nextProps.error) obj.canShowError = true
		
		return obj;
	}

	handleInputChange(event) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		const newState = {...this.state, inputChanged: true, product: {...this.state.product, [name]: value} }
		const product = newState.product

		console.log(name);
		if (name === 'saleFormat') {
			product.quantity = target.value === 'auction' ? 1 : product.quantity
		}

		if (name === 'useLastCategory') {
			if (target.checked) product.category = this.props.currentUser.lastCategory;
			newState.useLastCategory = !newState.useLastCategory
		}

		if (name === 'salePrice' || name === 'targetPrice' || name === 'escrowAmount') {
			product[name] = parseInt(value.replace(/Rp\s+/, '').replace(/\./g, ''), 10)
		}

		if (name === 'courierDelivery' && !target.checked) product.couriers = [];
		this.setState(newState);
	}

	handleBrandChange(brand) {
		this.setState({ 
			product: {
				...this.state.product, 
				brand: brand ? brand.name : ''
			},
			inputChanged: true,
			selectedBrand: brand
		});
	}

	handleSizeChange(sizes) {
		this.setState({ 
			sizes,
			inputChanged: true,
			product: {
				...this.state.product,
				attributes: {...this.state.product.attributes, sizes: sizes.map(s => s.value)}
			} 
		});
	}

	handleCourierChange(couriers) {
		if (couriers.length > 4) return;
		this.setState({ 
			couriers,
			disableCourierSelect: couriers.length >= 4,
			inputChanged: true,
			product: {
				...this.state.product, 
				couriers: couriers.map(c => ({ name: c.name, courierId: c.value })) // format for server
			} 
		});
	}

	updateCouriers() {
		this.setState({disableCourierSelect: false, inputChanged: true,})
	}

	onDateTimeChange(auctionEnd) {
		console.log('aaaa', auctionEnd)
		this.setState({ product: { ...this.state.product, auctionEnd }, inputChanged: true })
	}

	setDomEditorRef(ref) {
		this.domEditor = ref;
	}

	onEditorStateChange(editorState) {
		console.log(editorState);
		this.setState({ editorState });
	};

	handleCategoryChange1(event) {
		this.handleCategoryChange(event, 1)
	}
	handleCategoryChange2(event) {
		this.handleCategoryChange(event, 2);
	}
	handleCategoryChange3(event) {
		this.handleCategoryChange(event, 3)
	}

	handleCategoryChange(event, level) {
		const category = findCategoryById(this.props.categories, event.target.value);
		if (!category) return;

		const newState = {product: {...this.state.product}, inputChanged: true }

		if (level === 3 || !category.subs || !category.subs.length) newState.product.category = category.id
		else newState.product.category = '';
	
		if (level === 1) newState.selectedCategory = category;
		if (level === 2) newState.selectedSubCategory = category;

		this.setState(newState)
	}

	showAddressModal() {
		this.setState({ addressModelOpen: true })
	}

	onModalClose() {
		this.setState({ addressModelOpen: false })
	}

	onContentChange(editorState) {
		const editor = this.domEditor.editor;
		const description = editor.editor.querySelector('div[data-contents]').innerHTML
		this.setState({ product: { ...this.state.product, description } });
	};

	handleImageChange(images) {
		this.setState({
			...this.state,
			product: { 
			...this.state.product, images 
			},
			inputChanged: true,
			imagePreview: images.map(image => URL.createObjectURL(image))
		});
	}

	handleQuantityChange(valueAsNumber) {
		this.setState({ product: {...this.state.product, quantity: valueAsNumber } })
	}

	handleWeightChange(valueAsNumber) {
		this.setState({ product: {...this.state.product, weight: valueAsNumber } })
	}

	getBrands(input) {
		console.log('got input', input)
		if (!input) {
			return Promise.resolve({ options: [] });
		}
		return api.brand.get(input)
		.then((json) => {
			console.log('got json', json)
			return { options: json };
		});
	}

	isFormInvalid(event) {
		const errors = {}
		const form = this.state.form
		let hasError = false;
		const path = this.props.match.path;

		const fields = path === '/login' ? ['email', 'password'] : ['email', 'password', 'full Name', 'username', 'phone'];

		fields.forEach(item => {
			const field = item.split(' ').join(''); // handle spaces in field names
			if (!form[field] || !form[field].trim()) {
				errors[field] = item.toLowerCase() + ' is required';
				hasError = true;
			}
		})

		if (form.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(form.email)) {
			errors.email = 'invalid email address';
			hasError = true;
		}

		if (form.password && form.password.trim() && form.password.trim().length < 6) {
			errors.password = 'password should be 6 characters or more';
			hasError = true;
		}

		if (path === '/register') {
			if (form.username && !/^[a-zA-Z0-9]+$/i.test(form.username)) {
				errors.username = 'invalid username';
				hasError = true;
			}

			if (isNaN(Number(form.phone))) {
				errors.phone = 'invalid phone number';
				hasError = true;
			}

			if (!form.terms) {
				errors.terms = 'You need to accept the terms & conditions';
				hasError = true;
			}
		}
		return (hasError ? errors : false);
	}

	submitForm(event) {
		event.preventDefault();
		const product = this.state.product;
		const form = new FormData();
		Object.keys(product).forEach(key => {
			let item = product[key];
			if (item === 'category') {
				// replace value with id
				item = categoryUtils.findCategoryBySlug
			}

			if (key !== 'images') {
				if (Array.isArray(item) || isObject(item)) {
					console.log('strinfigy, item', item)
					form.append(key, JSON.stringify(item))
				} else {
					form.append(key, item)
				}
			}
		});

		// always append images last
		product.images.forEach(image => form.append('images', image))

		this.props.addNewProduct(form);

		return console.log(this.state, form);
		const formErrors = this.isFormInvalid();
		this.setState({ formValidated: true });
		this.setState({ formErrors })

		if (formErrors) return false;

		console.log('form valid');
		const action = this.props.match.path === '/login' ?
			this.props.loginSubmit : this.props.registerSubmit;
		action(this.state.form);
	}

	shouldComponentUpdate(nextProps) {
		const { history, product } = this.props;

		if (nextProps.product && !product) {
			if (history) history.push(`/i/${nextProps.product.slug}`) 
			return false;
		}
		return true;
	}

	renderError(name) {
		const { error } = this.props;
		const { formErrors } = this.state;
		let errorObj = error;

		if (error && error.errors) errorObj = error.errors;
		else if (formErrors) errorObj = formErrors
		if (!errorObj || !errorObj[name]) return null
		
		return (
			<div className="invalid-feedback">
				{errorObj[name]}
			</div>
		)
	}

	renderImagePreview() {
		const { imagePreview } = this.state;
		if (!imagePreview) return null

    return imagePreview.map((file, i) => (
      <div style={thumb} key={i}>
        <div style={thumbInner}>
          <img
            src={file}
            style={img}
          />
        </div>
      </div>
    ));
	}

	render() {
		const { isLoading, categories, currentUser, error, couriers } = this.props;
		const { canShowError } = this.state;
		console.log('current', this.state);
		if (!categories || !categories.length) return <Spinner />

		// if (this.props.savedProduct) {
		// 	return (
		// 		<Fragment>
		// 			<Alert type="success" message="Successfully saved" />
		// 			<Redirect from={this.props.match.path} to={`/${this.props.savedProduct.slug}`}/>
		// 		</Fragment>
		// 	)
		// }

		let sizesList = config.fashionSizes.map(s => (({ label: s, value: s })))
		const { product, selectedCategory } = this.state;
		const isAuction = product.saleFormat === 'auction'
		return (
			<DocumentTitle title={pageTitleText('Sell')}>
				<div className="container-fluid">
					{ canShowError && error &&
						<Alert options={{ autoClose: false, type: 'error' }}><ListErrors {...error} /></Alert>
					}

					<div className="row">
						<div className="col-lg-8 float-sm-right pt-4 offset-lg-1">
							<h4 className="title">Add Product</h4>
							<form noValidate onSubmit={this.submitForm} className={classnames('text-left product-form', { 'was-validated': this.state.formValidated })}>
								<div className="card p-3 snpcard">
									<div className="form-group">
										<label htmlFor="images" className="control-label-bold">Photos<span className="text-danger">*(Max 6)</span></label>
										<Dropzone accept={config.productImageFileTypes} required onDrop={this.handleImageChange} multiple>
											<div className="p-2">
												<p>Try dropping some files here, or click to select files to upload.</p>
												<p>Only {config.productImageFileTypes} images will be accepted</p>
											</div>
										</Dropzone>
										<div style={thumbsContainer}>{this.renderImagePreview()}</div>
									</div>
								</div>
								<div className="card p-3 snpcard">
									<br/>
									<div className="content">
											{/* Name FORM start */}
											<div className="form-group">
												<label htmlFor="name" className="control-label-bold">Name<span className="text-danger">*</span></label>
												<input type="text" className="inputstyle" id="name" name="name" placeholder="ex: Apple iPhone Xs Max 516GB Black"
													value={product.name || ''} onChange={this.handleInputChange} required />
													{this.renderError('name')}
												{/* ============================== */}
											</div>
											
											{/* Desc FORM start */}
											<div className="form-group">
												<label htmlFor="description" className="control-label-bold">Description<span className="text-danger">*</span></label>
												<span className="float-right ">
													{5000 - product.description.length} remaining
												</span>
												<textarea className="form-group inputstyle" id="description" style={{minHeight: 100 + 'px'}} 
													name="description" onChange={this.handleInputChange} required>
												</textarea>
												{/* <Editor
														editorState={this.state.editorState}
														onEditorStateChange={this.onEditorStateChange}
														onChange={this.onContentChange}
														ref={this.setDomEditorRef}
													/> */}
												{this.renderError('description')}										
												{/* =================================*/}
											</div>
											
											{/* Condition FORM start */}
											<div className="form-group">
												<label htmlFor="" className="control-label-bold d-block">Condition<span className="text-danger">*</span></label>										
													<div style={{width:'100%', textAlign:'center'}} className="nrsonsv5">
														<label className="font-weight-normal text-lowercase labelstyle">
															<input type="radio" className="form-check-inputs radiostyle" name="condition"
															value='new' checked={product.condition === 'new'}
															onChange={this.handleInputChange} required />
															<div className="textstyle">
																<div style={{width:'100%'}}>
																	<div className="textcontent"> NEW </div>
																	Baru - Masih tersegel & label - tidak pernah terpakai.
																</div>
															</div>
														</label> 
			
														
														<label className="font-weight-normal text-lowercase labelstyle">
															<input type="radio" className="form-check-inputs radiostyle" name="condition"
															value='great' checked={product.condition === 'great'}
															onChange={this.handleInputChange} required />
															<div className="textstyle">
																<div style={{width:'100%'}}>
																	<div className="textcontent"> Great </div>
																	New without tags (NTOW) No signs of wear - Unused
																</div>
															</div>
														</label>
			
														
														<label className="font-weight-normal text-lowercase labelstyle"
															><input type="radio" className="form-check-inputs radiostyle" name="condition"
															value='good' checked={product.condition === 'good'}
															onChange={this.handleInputChange} required />
															<div className="textstyle">
																<div style={{width:'100%'}}>
																	<div className="textcontent"> Good </div>
																	Gently used One / few minor flaws Functional
																</div>
															</div>
														</label>
			
														<label className="font-weight-normal text-lowercase labelstyle"
															><input type="radio" className="form-check-inputs radiostyle" name="condition"
															value='okay' checked={product.condition === 'okay'}
															onChange={this.handleInputChange} required />
															<div className="textstyle">
																<div style={{width:'100%'}}>
																	<div className="textcontent"> Okay </div>
																	Used, functional, multiple flaws / defects
																</div>
															</div>
														</label>
			
														<label className="font-weight-normal text-lowercase labelstyle"
															><input type="radio" className="form-check-inputs radiostyle" name="condition"
															value='poor' checked={product.condition === 'poor'}
															onChange={this.handleInputChange} required />
															<div className="textstyle">
																<div style={{width:'100%'}}>
																	<div className="textcontent"> Poor </div>
																	Major flaws, may be damaged, for parts
																</div>
															</div>
														</label>

														{this.renderError('condition')}
													</div>
															
															{/* <div className="form-group">
																<label htmlFor="summary" className="control-label-bold">Summary</label>
																<input type="text" className="" id="summary" name="summary"
																	value={product.summary || ''} onChange={this.handleInputChange} required />
															</div> */}
															{/* ====================================== */}
											</div>
									</div>
								</div>				

								<div className="card p-3 snpcard">
									<div className="content">
										<div className="form-group">
												<label htmlFor="category1" className="control-label-bold">Category<span className="text-danger">*</span></label>
												{this.state.useLastCategory && <input type="text" value={findCategoryById(categories, this.props.currentUser.lastCategory).originalName} readOnly={true}/>}
												{!this.state.useLastCategory &&
													<select className="" id="category1" name="category" required
														value={selectedCategory ? selectedCategory.id : product.category}
														onChange={this.handleCategoryChange1}>
														<option value=""></option>
														{categories.map((category, index) => (
															<option key={index} value={category.id}>{category.name}</option>
														))}
													</select>
												}
												{ this.props.currentUser.lastCategory && 
													<Fragment>
														{/* <label htmlFor="useLast" className="control-label-bold">
														<input type="checkbox" id="useLast" name="useLastCategory" className="radio-default" onChange={this.handleInputChange}
															checked={this.state.useLastCategory}/>
														Use Last Category</label> */}
													</Fragment>
												}
												{this.renderError('category')}
										</div>

											{!this.state.useLastCategory && selectedCategory && selectedCategory.subs.length > 0 && (
												<div className="form-group">
													{/* <label htmlFor="category2" className="control-label-bold"> SUB CATEGORY </label> */}
													<select className="" id="category2" name="category" required
														value={this.state.selectedSubCategory ? this.state.selectedSubCategory.id : product.category}
														onChange={this.handleCategoryChange2}>
														<option value=""></option>
														{selectedCategory.subs.map((category, index) => (
															<option key={index} value={category.id}>{category.name}</option>
														))}
													</select>
												</div>
											)}

											{!this.state.useLastCategory && this.state.selectedSubCategory && this.state.selectedSubCategory.subs.length > 0 && (
												<div className="form-group">
													{/* <label htmlFor="category3" className="control-label-bold">Sub-Sub Category</label> */}
													<select className="" id="category3" name="category" required
														value={product.category || ''} onChange={this.handleCategoryChange3}>
														<option value=""></option>
														{this.state.selectedSubCategory.subs.map((category, index) => (
															<option key={index} value={category.id}>{category.name}</option>
														))}
													</select>
												</div>
											)}

											<div className="row">
												<div className="form-group col-12">
													<label htmlFor="brand" className="control-label-bold">Brand</label>
													<Select.Async
														onChange={this.handleBrandChange}
														name="brand"
														labelKey="name"
														valueKey="name"
														loadOptions={this.getBrands}
														placeholder="Select brand (optional)"
														value={this.state.selectedBrand}
													/>
													{this.renderError('brand')}
												</div>
												{selectedCategory && selectedCategory.name === 'Fashion' && 
													<div className="form-group col-12">
														<label htmlFor="sku" className="control-label-bold">Sizes</label>
														{/* <input type="text" id="sku" name="sku"
															value={product.sku || ''} onChange={this.handleInputChange} /> */}
														<Select
															onChange={this.handleSizeChange}
															options={sizesList}
															multi={true}
															searchable={false}
															name="sizes"
															placeholder="Choose size (optional)"
															value={product.attributes.sizes}
														/>
														{this.renderError('sku')}
													</div>
												}
											</div>								  
									</div>
								</div>

								<div className="card p-3 snpcard">
									<div className="content">
									<div className="row">
											<div className="form-group col-6 mb-0">
												<label htmlFor="quantity" className="control-label-bold">Quantity</label>
												{/* <input type="number" className=" mb-2" id="quantity" name="quantity"
													disabled={product.isAuction}
													title={product.isAuction ? 'can only auction 1 product at a time' : ''}
													value={product.quantity || ''} onChange={this.handleInputChange} required /> */}
													<br/>
													<NumericInput 
														id="quantity" 
														name="quantity"
														disabled={isAuction}
														title={isAuction ? 'can only auction 1 product at a time' : ''}
														value={product.quantity || ''} 
														onChange={this.handleQuantityChange} 
														className="numInputStyle" 
														style={{
															input: {
																marginBottom: 0
															}
														}} 
													  min={ 1 } 
														step={ 1 } 
														required
														mobile
													/>
												{this.renderError('quantity')}
											</div>

											<div className="form-group col-6 mb-0">
												<label htmlFor="weight" className="control-label-bold mr-1">Weight (Grams)</label>
												<i className="ion-help-circled cursor" id="weightTooltip"></i>
												{/* <input type="number" className=" mb-2" id="weight" name="weight"
													value={product.weight || ''} onChange={this.handleInputChange} /> */}
													<br/>
													<NumericInput 
														className="numInputStyle"
														style={{
															input: {
																marginBottom: 0
															}
														}} 
														
														id="weight" 
														name="weight"
														value={product.weight || ''} 
														onChange={this.handleWeightChange}
														min={ 0 } 
														step={ 500 } 
														mobile
													/>
												{this.renderError('weight')}

												<UncontrolledTooltip placement="right" target="weightTooltip">
													Make sure to input actual weight if delivering by courier
													</UncontrolledTooltip>
											</div>
									</div>
									</div>
								</div>

								<div className="card slmthd1">
									<div className="content">
											<form>					
												<label htmlFor="isAuction" className="control-label-bold organizerblock labelstyle2">
												<input type="radio" className="form-check-inputs radiostyle2" id="isAuction" name="saleFormat"
													checked={product.saleFormat === 'fixed'} onChange={this.handleInputChange} value="fixed" required />
													<div className="textstyle2">
														<div style={{width:'100%'}}>
															<div className="textslmthd">
															Fixed Price
															</div>
														</div>
													</div>
												</label>

												<label htmlFor="isAuction" className="control-label-bold organizerblock labelstyle2">
												<input type="radio" className="form-check-inputs radiostyle2" id="isAuction" name="saleFormat"
													checked={product.saleFormat === 'auction'} onChange={this.handleInputChange} value="auction" required />
													<div className="textstyle2">
														<div style={{width:'100%'}}>
															<div className="textslmthd">
															Auction
															</div>
														</div>
													</div>
												</label>	
											</form>
									</div>
								</div>

								<div className="card p-3 snpcard slmthd2">
									<div className="content">
											
											

									{product.saleFormat === 'fixed' ? (
										<div className="row">
											<div className={classnames("form-group mb-0", {
												'col-6': product.onSale === true,
												'col-12': !product.onSale,
											})}>				
										
										   
											<br/>
												<label htmlFor="salePrice" className="control-label-bold">Sale Price</label><br/>
												<NumberFormat thousandSeparator={'.'} decimalSeparator={','} prefix={'Rp '}
												 className="mb-2 inputstyle" placeholder="How much are you selling it for?"
												 min={config.minProductAmount}
												 value={product.salePrice || ''}
												 id="salePrice"  name="salePrice" onChange={this.handleInputChange} />
												{this.renderError('salePrice')}
											</div>

											{product.onSale &&
												<div className="form-group col-6 mb-0">
													<label className="control-label-bold">Original Price</label>
													<input type="number" className=" mb-2" name="originalPrice"
														value={product.originalPrice || ''} onChange={this.handleInputChange} />
													{this.renderError('originalPrice')}
												</div>
											}
										</div>
									) : (
											<div className="row">
												<div className="form-group col-12 mb-0">
													<label htmlFor="targetPrice" className="control-label-bold">Target Price</label>

														<NumberFormat thousandSeparator={'.'} decimalSeparator={','} prefix={'Rp '} 
														className=" mb-2 inputstyle" id="targetPrice" name="targetPrice"
														value={product.targetPrice || ''} onChange={this.handleInputChange} 
														placeholder="The highest bidder needs to reach the target price to win the auction" required />

														{this.renderError('auction.targetPrice')}
												</div>

												<div className="form-check mb-2 pl-0">
													
														<Fragment>
															<label className="control-label-bold mr-1 organizerblock">
																<input type="checkbox" className="form-check-inputs ml-2 radio-default"
																	id="showAuctionTarget" name="showAuctionTarget"
																	checked={product.showAuctionTarget || ''} onChange={this.handleInputChange} />
																Show Target Price 
															</label>

															<i className="ion-help-circled cursor" id="showAuctionTargetTooltip"></i>
															<UncontrolledTooltip placement="right" target="showAuctionTargetTooltip">
																People will be able to see your target price.
															</UncontrolledTooltip>
														</Fragment> 
													
												</div>

												<br/>
												<br/>

												<div className="form-group col-12 mb-0"> {/*Auction End*/}
													<label className="control-label-bold">Auction End</label> 
													<DatePicker
														selected={moment(product.auctionEnd)}
														required
														onChange={this.onDateTimeChange}
														className="mb-2 inputstyle"
														showTimeSelect
														timeIntervals={15}
														minDate={moment().add(1, 'day')}
														maxDate={moment().add(30, 'days')}
														dateFormat="LLL"
														timeFormat="HH:mm"
														popperPlacement="bottom"
														popperModifiers={{
															flip: {
																	behavior: ["bottom"] // don't allow it to flip to be above
															},
															preventOverflow: {
																	enabled: false // tell it not to try to stay within the view (this prevents the popper from covering the element you clicked)
															},
															hide: {
																	enabled: false // turn off since needs preventOverflow to be enabled
															}
														}}
													/>
													{this.renderError('auctionEnd')}
												</div>

												<div className="form-check mb-2 pl-0">
													{/* {!product.isAuction && (
														<Fragment>
															<input type="checkbox" className="form-check-inputs" id="onSale" name="onSale"
																checked={product.onSale || ''} onChange={this.handleInputChange} />
															<label htmlFor="onSale" className="control-label-bold">Product is on sale</label>
														</Fragment>
													)} */}

													{product.saleFormat === 'auction' && (
														<Fragment>
																
																<label className="control-label-bold mr-1 organizerblock">
																<input type="checkbox" className="form-check-inputs ml-2 radio-default"
																	id="setEscrowAmount" name="setEscrowAmount"
																	checked={product.setEscrowAmount || ''} onChange={this.handleInputChange} />
																Set custom escrow amount</label>
															
																<i className="ion-help-circled cursor" id="escrowAmountTooltip"></i>
																<UncontrolledTooltip placement="right" target="escrowAmountTooltip">
																Choose this to set custom value for how much is deducted into escrow on initial bid
																</UncontrolledTooltip>
															
														</Fragment>
													)}

													{ product.setEscrowAmount && (
														<div className="form-group col-12 mb-0">
																<label htmlFor="escrowAmount" className="control-label-bold">Escrow Amount</label>

																<NumberFormat thousandSeparator={'.'} decimalSeparator={','} prefix={'Rp '} 
																className=" mb-2 inputstyle" id="escrowAmount" name="escrowAmount"
																value={product.escrowAmount || ''} onChange={this.handleInputChange}  
																placeholder="Deposit to join the auction" required/>
																
																{this.renderError('auction.escrowAmount')}
														</div>
													)}
												</div>

											</div>
										)
									}
									
									</div>
								</div>
								

								<div className="card p-3 snpcard">
									<div className="content">
									<div className="row">
											<div className="form-group col-12 pt-3">
												<label className="control-label-bold d-block">Delivery Methods</label>

												<label className="control-label-bold organizerblock" htmlFor="courierDelivery">
												<input type="checkbox" className="form-check-inputs radio-default" id="courierDelivery" name="courierDelivery"
													checked={product.courierDelivery || ''} onChange={this.handleInputChange} />
												Courier</label>

												<label className="control-label-bold mr-1 organizerblock" htmlFor="selfDelivery">
												<input type="checkbox" className="form-check-inputs radio-default" id="selfDelivery" name="selfDelivery"
													checked={product.selfDelivery || ''} onChange={this.handleInputChange} />
												Manual / Meet-Up</label>

												<i className="ion-help-circled cursor" id="selfDeliveryTooltip"></i>
												<UncontrolledTooltip placement="right" target="selfDeliveryTooltip">
													Choose this if you're delivering yourself, or a digital item. You
													pay any delivery fee
												</UncontrolledTooltip>
												{this.renderError('courierDelivery')}
											</div>
											<div className={classnames("form-group mb-0", {
												'col-6': product.courierDelivery,
												'col-12': !product.courierDelivery,
												})}>
												<label htmlFor="location" className="control-label-bold">Shipping From</label>
												<select name="location" id="location" required value={product.location}
													onChange={this.handleInputChange}>
													{!product.courierDelivery && <option value=""></option>}
													{currentUser.addresses.map(address =>
														<option value={address.id} key={address.id}>
															{address.address}
														</option>
													)}
												</select>
												<span className="pointer underline" onClick={this.showAddressModal}>Add New Address</span>
												<AddressFormModal isOpen={this.state.addressModelOpen} onModalClose={this.onModalClose} />
												{/* {this.state.showAddressModal && <addressFormModal />} */}
											</div>

											{product.courierDelivery &&
												<div className="form-group col-6 mb-0">
													<label htmlFor="courier" className="control-label">
														Courier
														{this.state.disableCourierSelect &&
														<span className="float-right link-display ml-3" 
															onClick={this.updateCouriers}>(Edit)</span>
														}
													</label>
													<Select
														closeOnSelect={false}
														multi
														disabled={this.state.disableCourierSelect}
														onChange={this.handleCourierChange}
														options={couriers}
														labelKey="name"
														name="couriers"
														placeholder="Choose couriers (up to 4)"
														removeSelected={true}
														value={this.state.couriers}
													/>
												</div>
											}
									</div>

										{/* <div className="row pt-3">
											<div className="form-group col-12 mb-0">
												<input type="checkbox" className="form-chseck-input" htmlFor="isActive" name="isActive"
													checked={product.isActive || ''} onChange={this.handleInputChange} />
												<label htmlFor="isActive" className="control-label-bold">Active</label>

												<input type="checkbox" className="form-chseck-input" id="inStock" name="inStock"
													checked={product.inStock || ''} onChange={this.handleInputChange} />
												<label htmlFor="inStock" className="control-label-bold">In stock</label>
											</div>
										</div> */}

										<div className="form-group">
											<label className="control-label-bold mr-1 organizerblock">
											<input type="checkbox" className="form-checked-inputs radio-default"
												id="isAuthentic" name="isAuthentic" 
												checked={product.isAuthentic || ''} onChange={this.handleInputChange} />
													Product is Authentic</label>

											<i className="ion-help-circled cursor" id="isAuthenticTooltip"></i>
											<UncontrolledTooltip placement="top" target="isAuthenticTooltip"> 
											Dengan mencentang checkbox ini anda menyatakan bahwa produk yang anda jual adalah asli dari merek tsb. Dan memiliki bukti, dokumen, atau sertifikat terkait.
											</UncontrolledTooltip>
										</div>

									</div>
								</div>
			
								<div className="card p-3 snpcard">
									<div className="content">
									<button type="submit" 
												className={classnames('btn btn-secondary btn-block rounded-0', {
														'loading': this.props.isLoading
												})} disabled={this.props.isLoading}>Submit</button>
											<div className="clearfix"></div>
									</div>
								</div>


							</form>
						</div>
					</div>
				</div>
			</DocumentTitle>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductForm);