import Profile from '../profile';
import Order from '../../admin/order';
import ProductList from '../product/list';
import ProductForm from '../product/form';
import Bookmarks from '../bookmarks';
import Transactions from '../transactions';
import TopUps from '../topups';
import AdminTable from '../../admin/table';

const userRoutes = [
  {
    path: '/account',
    name: 'Profile',
    icon: 'pe-7s-user',
		component: Profile,
		exact: true
  },
	{
    path: '/history',
    name: 'My Transactions',
    icon: 'pe-7s-note2',
    component: Transactions
  },
  {
    path: '/wallet',
    name: 'Wallet',
    icon: 'pe-7s-note2',
    component: TopUps
	},
	{
    path: '/bookmarks',
    name: 'Bookmarks',
    icon: 'pe-7s-note2',
    component: Bookmarks
	},
	{
    path: '/products',
    name: 'My Products',
    icon: 'pe-7s-note2',
    component: ProductList
  },
	{
    path: '/sell',
    name: 'Sell New Product',
    icon: 'pe-7s-note2',
    component: ProductForm
	},
  { redirect: true, path: '*', to: 'account', name: 'Profile' }
];

const adminRoutes = [
  {
    path: '/users',
    name: 'Accounts',
    icon: 'pe-7s-user',
		component: AdminTable,
		exact: true
	},
	{
    path: '/users/:id',
    name: 'Accounts',
    icon: 'pe-7s-user',
		component: AdminTable,
		exact: true
  },
  {
    path: '/products',
    name: 'Products',
    icon: 'pe-7s-note2',
		component: AdminTable,
		exact: true
	},
	{
    path: '/orders',
    name: 'Orders',
    icon: 'pe-7s-note2',
		component: AdminTable,
		exact: true
  },
  {
    path: '/orders/:id',
    name: 'Order',
		component: Order,
		exact: true
  },
  {
    path: '/reports',
    name: 'Reports',
    icon: 'pe-7s-note2',
		component: AdminTable,
		exact: true
  },
  {
    path: '/payouts',
    name: 'Payouts',
    icon: 'pe-7s-note2',
		component: AdminTable,
		exact: true
  },
	{
    path: '/categories',
    name: 'Categories',
    icon: 'pe-7s-note2',
		component: AdminTable,
		exact: true
	},
	{
    path: '/brands',
    name: 'Brands',
    icon: 'pe-7s-note2',
    component: AdminTable
	},
	{ redirect: true, path: '*', to: 'users', name: 'Profile' }
];

export default {  adminRoutes, userRoutes };
