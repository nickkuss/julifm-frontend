import React from 'react';
import Route from 'react-router-dom/Route';
import Switch from 'react-router-dom/Switch';
import Redirect from 'react-router-dom/Redirect';
import NavLink from 'react-router-dom/NavLink';
import ProductEdit from '../backend/product/edit';

import routes from './routes';

import './style.css'

export default (props) => {
  console.log(props)
  const path = props.match.path;
  const pathRoute = (path === '/admin' ? routes.adminRoutes : routes.userRoutes)
  const links = pathRoute.filter(route => route.path.indexOf('/:') === -1 && !route.redirect)

  return (
    <div className="row" id="backend">

      {/* desktop side menu */}
      <div className="sticky-top col-md-3 col-lg-2 d-none d-md-block d-lg-block pr-0 pl-0 text-left" id="sidemenu">
        <div className="sidebar-background"></div>
        <ul className="list-group mt-2 border-0 ml-0 p-2">
          {links.map((route, key) => (
            <NavLink to={`${path}${route.path}`} key={key} className="list-group-item">{route.name}</NavLink>
          ))
          }
        </ul>
      </div>
      {/* desktop side menu end*/}

      <div className="col-sm" style={{ overflow: 'auto' }}>
        <Switch>
          { path === '/customer' &&
            <Route exact path="/customer/product/edit/:slug" component={ProductEdit}/>
          }
          {pathRoute.map((route, key) => (
            route.redirect ?
              <Redirect from={route.path} to={route.to} key={key} /> :
              <Route path={`${path}${route.path}`} key={key}
                component={route.component} exact={route.exact} />
          ))}
        </Switch>
      </div>
    </div>
  )
}

