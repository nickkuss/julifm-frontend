import React, { Component } from 'react';
import classnames from 'classnames';
import config from '../../../config';

class ReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			report: { },
		}

		this.handleChange = this.handleChange.bind(this);
		this.submit = this.submit.bind(this);
	}

	submit(event) {
		event.preventDefault();
		event.stopPropagation(); // prevent parent form from submitting
		
    this.props.submit(this.props.productId, this.state.report)
	}

	handleChange(event) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		const newState = { report: {...this.state.report, [name]: value } }
		this.setState(newState);
  }
  
	render() {
    console.log(this.state)
		const { report } = this.state;
		const { isLoading, onSubmit } = this.props;
		return (
			<React.Fragment>
				<form noValidate onSubmit={(e) => {
					e.preventDefault()
					onSubmit(report)
				}}>
					<div className="row">
						<div className="col-12">
							<div className="form-group">
								<label htmlFor="rating" className="control-label font-weight-bold">Reason*</label>
                <div className="pl-2">
									{config.reportReasons.map((reason, i) => 
										<label htmlFor={`reason-${i}`} key={i} className="mb-1 d-block pointer">
											<input type="radio" name="reason" value={reason} id={`reason-${i}`} className="mr-1 mb-1" onChange={this.handleChange}/> {reason}
										</label>
									)}
                </div>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							<div className="form-group">
								<label htmlFor="comment" className="control-label font-weight-bold">Additional Info</label>
								<textarea placeholder="comment" style={{ minHeight: 50 }}
									value={report.comment || ''} id="comment" name="comment" onChange={this.handleChange} />
							</div>
						</div>
					</div>
					<button
						className={classnames('btn btn-secondary btn-block rounded-0', {
							'loading': isLoading
						})} disabled={isLoading || !report.reason}>Submit Report</button>
					<div className="clearfix"></div>
				</form>
			</React.Fragment>
		)
	}
}


export default ReportForm;
