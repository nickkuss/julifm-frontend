import React, { Component } from 'react';
import { connect } from 'react-redux';
import Modal from 'reactstrap/lib/Modal';
// import productActions from '../../actions/product';
import ReportForm from './report-form';
// import Alert from '../alert';
// import ListErrors from '../list-error';

class ReportFormModal extends Component {
	constructor(props) {
		super(props);
		this.state = { address: { isDefault: true } };
		this.close = this.close.bind(this);
		this.onClosed = this.onClosed.bind(this);
	}

	close() {
		this.setState({ isOpen: false, manualClose: true });
	}

	static getDerivedStateFromProps(nextProps, state) {
		const obj = {};

		if (!nextProps.isOpen) {
			obj.isOpen = false;
			obj.manualClose = false;
		} else obj.isOpen = !state.manualClose;

		return obj;
	}

	// componentDidUpdate(prevProps, prevState) {
	// 	console.log('repp upddd', prevProps, this.props)
	// 	if (prevProps.item && this.props.item && prevProps.item.status !== this.props.item.status) {
	// 		console.log('diffff')
	// 		this.close()
	// 	}

	// }

	onClosed() {
		this.props.onModalClose()
	}


	render() {
		const { item } = this.props
		return (
			<Modal isOpen={this.state.isOpen} onClosed={this.onClosed}
				fade={false} toggle={this.close} className={this.props.className}>
				<div className="modal-header">
					<h5 className="modal-title">Report Item</h5>
					<button type="button" className="close"
						onClick={this.close}
						aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div className="modal-body">
					{/* {error && !isLoading && <Alert options={{ autoClose: false, type: 'error' }}><ListErrors {...error} /></Alert>} */}
					<p>
						Report <span className="font-weight-bold">{item ? item.name : ''}</span>
					</p>
					<ReportForm {...this.props} />
				</div>
			</Modal>
		)
	}
}


export default ReportFormModal;
