import React, { Component } from 'react';
import { connect } from 'react-redux';
import DocumentTitle from 'react-document-title';
import Link from 'react-router-dom/Link';
import TabContent from 'reactstrap/lib/TabContent';
import TabPane from 'reactstrap/lib/TabPane';
import Collapse from 'reactstrap/lib/Collapse';
import Card from 'reactstrap/lib/Card';
import CardHeader from 'reactstrap/lib/Card';
import CardBody from 'reactstrap/lib/CardBody';
import Waypoint from 'react-waypoint';
import classnames from 'classnames';
import Spinner from '../../spinner';
import AddressItem from '../profile/address-item';
import moment from 'moment';
import userActions from '../../../actions/user';
import { formatAmount, pageTitleText } from '../../../utils/app';
import ListErrors from '../../list-error';
import Alert from '../../alert';
import './style.css';
import OrderItem from '../../order-item';
import cloneDeep from 'lodash.clonedeep';
import ReportModal from './report-modal';
import config from '../../../config';
import { parseMessage } from '../../../utils/chat';
import { onReceiveMessage, onSendChatError } from '../../../actions/chat';


class CardHeaderOrderItem extends React.Component {
  
  constructor(props) {
    super(props);
    this._onClick = this._onClick.bind(this);
  }
  
  _onClick() {
    this.props.onClick(this.props.order.id);
  }

  render() {
    const { order, isOpen } = this.props;
    return (
      <CardHeader style={{ backgroundColor: '#f5f5f5' }}
        className="pr-2 pl-2 pt-4 pb-4 pointer mb-0 font-weight-bold flex-row justify-content-between rounded-0" 
        onClick={this._onClick}>
        <span>
          <i className={`ion-chevron-${ isOpen ? 'down' : 'right'} float-left mr-2`}></i>
          #{order.id.slice(0, 7)} &nbsp;&nbsp;
        <span className="ml-3" style={{ position: 'absolute', left: 7, bottom: 2 }}>
          {moment(order.createdAt).format('MMM Do, \'YY HH:mm')}
        </span>
        </span>
        <span>
          {order.status ? order.status.toUpperCase() : null}
        </span>
        <span>{formatAmount(order.totalPrice)} &nbsp;&nbsp;</span>
      </CardHeader>
    )
  }
}


class OrderItemStatus extends React.Component {

  constructor(props) {
    super(props);
    this._onClick = this._onClick.bind(this)
  }

  _onClick() {
    const { order, item } = this.props
    this.props.onClick(order, item)
  }
  render() {
    const { item } = this.props
    const classes = 'ion-edit pr-2 pl-2 p-2 pointer mr-1'
    return (
      <i onClick={this._onClick}
        className={classnames(classes, { 'float-left': item.trackingInfo })} 
      ></i> 
    )
  }
}

class Transaction extends Component {

  constructor(props) {
    super(props);

    this.state = { 
      activeTab: 1, 
      isLoadingByMe: false,
      isLoadingForMe: false,
      selectedOrderItem: null,
      openMultiple: false,
      ordersByMe: null, ordersForMe: null,
      saving: false,
      reportModalOpen: false
    }

    this.setTab1 = this.setTab1.bind(this);
    this.setTab2 = this.setTab2.bind(this);
    this.toggle = this.toggle.bind(this);
    this.handleWayPointEnter = this.handleWayPointEnter.bind(this);
    this.handleWayPointLeave = this.handleWayPointLeave.bind(this);
    this.toggleMultiplePanel = this.toggleMultiplePanel.bind(this);
    this.onOrderItemSelect = this.onOrderItemSelect.bind(this);
    this.onItemEditCancel = this.onItemEditCancel.bind(this);
    this.onItemEditChange = this.onItemEditChange.bind(this);
    this.onItemEditSubmit = this.onItemEditSubmit.bind(this);
    this.showReportModal = this.showReportModal.bind(this)
    this.onReportSubmit = this.onReportSubmit.bind(this)
    this.onReportModalClose = this.onReportModalClose.bind(this)
    this.createSupportChat = this.createSupportChat.bind(this)
    this.sendSupportChat = this.sendSupportChat.bind(this)
    this.onReceive = this.onReceive.bind(this)
  }

  componentDidMount() {
    // force initial load of ordersforme
    this.loadOrders(1, 2)
  }

  toggleMultiplePanel() {
    this.setState({ openMultiple: !this.state.openMultiple })
  }

  onOrderItemSelect(order, item, isReport) {
    let status = isReport ? 'reported' : item.status
    this.setState({
      reportModalOpen: isReport,
      selectedOrderItem: { 
        ...item,
        order: order.id, 
        productId: item.id,
        status: status === 'started' ? (item.courier ? 'shipped' : 'delivered') : status
      } 
    })
  }

  onItemEditCancel() {
    this.setState({ selectedOrderItem: null })
  }

  onItemEditChange(event) {
    const { selectedOrderItem } = this.state;
    const { target: { name, value } } = event
    this.setState({ selectedOrderItem: { ...selectedOrderItem, [name]: value } })
  }

  onItemEditSubmit(event, report) {
    const { selectedOrderItem } = this.state;
    const orderProp = !!report ? 'ordersByMe' : 'ordersForMe'
    if (report) selectedOrderItem.report = report

    this.setState({ editError: false, saving: true })
    userActions.updateOrder(selectedOrderItem.order, selectedOrderItem)
    .then(() => {
        const items = cloneDeep(this.state[orderProp].items)
        const order = items.find(x => x.id === selectedOrderItem.order);
        const orderItem = order.items.find(x => x.id ===  selectedOrderItem.id)
        orderItem.status = selectedOrderItem.status;
        orderItem.trackingInfo = selectedOrderItem.trackingInfo;
        if (report) this.createSupportChat(selectedOrderItem)
        
        this.setState({ saving: false, selectedOrderItem: null, [orderProp]: { ...this.state[orderProp], items } })
      })
      .catch(error => {
        this.setState({ editError: error, saving: false })
      })
  }

  showReportModal(order, item) {
		this.onOrderItemSelect(order, item, true)
  }

  onReportModalClose() {
		this.setState({ reportModalOpen: false, selectedOrderItem: null })
  }
  
  onReportSubmit(report) {
    this.onItemEditSubmit(null, report)
  }

  createSupportChat(selectedOrderItem) {
    const { user } = this.props;

    if (!user || !config.supportId) return;

    if (config.supportId !== user.id) {
      const room = user.rooms.find(x => x.name === user.id + config.supportId || x.name === config.supportId + user.id)
      console.log('got roomddds', room)
      room ?
        this.sendSupportChat(selectedOrderItem, room.id) :
        user.createRoom({
          name: user.id + config.supportId,
          addUserIds: [config.supportId],
          private: true,
        }).then(room => {
          user.subscribeToRoom({
            roomId: room.id,
            hooks: { onNewMessage: this.onReceive },
          })
          this.sendSupportChat(selectedOrderItem, room.id)
        })
        .catch(e => console.log('eeee', e))
    }
  }

  sendSupportChat(selectedOrderItem, roomId) {
    if (!selectedOrderItem || !selectedOrderItem.report) return;
    
    const { user } = this.props;
    const text = `
      ====================== ${new Date().toLocaleString()} ======================

      Hello,

      I have an issue with an item. The order number is ${selectedOrderItem.order} and the item is ${selectedOrderItem.name}.
      The problem is: '${selectedOrderItem.report.reason}'. 
      ${selectedOrderItem.report.comment}
    `
    const data = {text, roomId}

    user.sendMessage(data).catch(() => null)
  }

  onReceive(data) {
    const incomingMessage = parseMessage(data)
    this.props.onReceive(incomingMessage)
  }

  handleWayPointEnter() {
    const { activeTab, ordersByMe, ordersForMe, isLoadingByMe, isLoadingForMe } = this.state;
    if ((activeTab === 1 && isLoadingByMe) || (activeTab === 2 && isLoadingForMe)) return false;
    const orders = activeTab === 1 ? ordersByMe : ordersForMe

    if (!orders) return this.loadOrders();

    const { page, hasMore } = orders;
    if (hasMore) this.loadOrders(page + 1);
  }

  handleWayPointLeave() {}

  loadOrders(page=1, tab=this.state.activeTab) {
    const action = tab === 1  ? userActions.getOrdersByMe : userActions.getOrdersForMe;
    const field = tab === 1  ? 'ordersByMe' : 'ordersForMe';
    const loaderProp = tab === 1 ? 'isLoadingByMe' : 'isLoadingForMe';
    this.setState({ [loaderProp]: true, error: null })
    action(page).then(response => {
      this.setState({
        [field]: {
          ...response,
          items: this.state[field] ? 
            [...this.state[field].items].concat(response.items) : response.items
        }, 
        [loaderProp]: false,
      })
    }).catch(error => this.setState({ error, [loaderProp]: false }))
  }

  toggle(id) {
    this.setState({ [id]: !this.state[id], selectedPanel: id });
  }

  setTab1() {
    this.setState({ activeTab: 1 })
  }

  setTab2() {
    this.setState({ activeTab: 2 })
  }

  generateTabs() {
    const { ordersByMe, ordersForMe, activeTab } = this.state;
    return (
      <div className="nav-tabs">
        <div className="nav-item pointer d-inline-block" style={{ width: '50%' }}>
          <span onClick={this.setTab1}
            className={classnames('nav-link', { active: activeTab === 1 })} >
            BY ME &nbsp;
            <span className={activeTab === 1 ? 'font-weight-bold' : undefined}>
              {ordersByMe && ordersByMe.items.length > 0 &&
                `(${ordersByMe.items.length} of ${ordersByMe.totalResults})`
              } 
            </span>
					</span>
        </div>

        <div className="nav-item pointer d-inline-block" style={{ width: '50%' }}>
          <span onClick={this.setTab2}
            className={classnames('nav-link', { active: activeTab === 2 })} >
            FOR ME &nbsp;
            <span className={activeTab === 2 ? 'font-weight-bold' : undefined}>
              {ordersForMe && ordersForMe.items.length > 0 &&
                `(${ordersForMe.items.length} of ${ordersForMe.totalResults})`
              } 
            </span>
					</span>
        </div>

      </div>
    );
  }

  generateOrdersForMe() {
    const { ordersForMe, selectedOrderItem, saving } = this.state;
    if (!ordersForMe) return <Spinner />;

    return (
      ordersForMe.items.length ?
      <table className="orders-table">
        <thead>
          <tr>
            <th>Info</th>
            <th>Product</th>
            <th>Photo</th>
            <th>Amount</th>
            <th>Qty</th>
            <th>Shipping</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {ordersForMe.items.map(order => {
            return (
              order.items.map((item, index) =>
                <tr className={index % order.items.length === 0 ? 'order-item-group' : ''} key={item.id}>
                  {index % order.items.length === 0 &&
                    <td rowSpan={order.items.length}>
                      <span>{moment(order.createdAt).format('MMM Do, \'YY')}</span>
                      <span className="font-weight-bold d-block">#{order.id.slice(0, 7)}</span>
                      <AddressItem readOnly={true} address={order.deliveryAddress} className="pl-1 pr-0"/>
                      <div>
                        <span className="mb-1" className="link__display"> 
                          <i className="ion-ios-person mr-1"></i>
                          <Link to={`/user/${order.buyer.username}`}>{order.buyer.displayName}</Link>
                        </span>
                        <span className="mb-1 d-block link__display">
                          <i className="ion-ios-telephone mr-1"></i>
                          <a href={`tel:${order.buyer.phone}`}>{order.buyer.phone}</a>
                        </span>
                      </div>
                    </td>
                  }
                  <td><Link to={'/' + item.url}>{item.name}</Link></td>
                  <td>
                    <a href={item.image} target="_blank">
                      <img src={item.image} alt={item.name} style={{ maxWidth: 50 }}/>
                    </a>
                  </td>
                  <td>{formatAmount(item.price)}</td>
                  <td>{item.quantity}</td>
                  <td>
                    { item.courier ?
                      <React.Fragment>
                        <span className="d-block">{item.courier.name}</span>
                        <span className="d-block">{item.courier.service}</span>
                      </React.Fragment> :
                      'N/A'
                    }
                    <span className="d-block">{}</span>
                  </td>
                  <td>
                    {selectedOrderItem && selectedOrderItem.id === item.id && selectedOrderItem.order === order.id  ?
                      <React.Fragment>
                        <select name="status" className="mb-1"
                          onChange={this.onItemEditChange}
                          disabled={order.status === 'delivered'} value={selectedOrderItem.status}>
                          <option disabled value="started">Started</option>
                          {item.courier && <option value="shipped">Shipped</option>}
                          {!item.courier && <option value="delivered">Delivered</option>}
                        </select>
                        { (item.trackingInfo || (item.courier && selectedOrderItem.status !== 'started')) && 
                          <input type="text" className="mb-1"
                            onChange={this.onItemEditChange}
                            style={{ fontSize: '.8rem' }}
                            name="trackingInfo"
                            placeholder="Tracking number"
                            defaultValue={selectedOrderItem.trackingInfo || item.trackingInfo}
                          />
                        }
                        <button className="btn btn-danger float-right text-center" 
                          onClick={this.onItemEditCancel} disabled={saving}>
                          <i className="ion-close-circled"></i>
                        </button>
                        <button className="btn btn-success float-right text-center mr-1" 
                          onClick={this.onItemEditSubmit} disabled={saving}>
                          <i className="ion-checkmark-round"></i>
                        </button>
                      </React.Fragment> :
                      <React.Fragment>
                        {(item.status === 'started' || item.status === 'shipped') &&
                          <OrderItemStatus item={item} order={order} onClick={this.onOrderItemSelect}/>
                        }
                        <span className={classnames({
                          'badge badge-danger': item.status === 'reported'
                        })} style={{ fontSize: item.status === 'reported' ? '.9rem' : undefined }}>{item.status}</span>
                        {item.trackingInfo && <span className="d-block" >Tracking: {item.trackingInfo}</span> }
                      </React.Fragment>
                    }

                  </td>
                </tr>
              )
            )
          })}
        </tbody>
      </table> :
      <p className="text-center font-weight-bold pt-5">You have no orders</p>
    )
  }

  generateOrdersByMe() {
    const { ordersByMe, selectedPanel, openMultiple, reportModalOpen, selectedOrderItem, saving } = this.state;
    if (!ordersByMe) return <Spinner />;
    if (!ordersByMe.items.length) return <p className="text-center font-weight-bold pt-5">You have no orders</p>
    
    return (
      <React.Fragment>
        <ReportModal 
          isOpen={reportModalOpen && selectedOrderItem}
          onModalClose={this.onReportModalClose}
          item={selectedOrderItem}
          onSubmit={this.onReportSubmit}
          isLoading={saving}
        />
          {ordersByMe.items.map(order => {
            return (
              <Card style={{ marginBottom: 1 }} key={order.id}>
                <CardHeaderOrderItem order={order} onClick={this.toggle} 
                  isOpen={openMultiple ? this.state[order.id] : selectedPanel === order.id}
                />
                <Collapse isOpen={openMultiple ? this.state[order.id] : selectedPanel === order.id}>
                  <CardBody className="pl-2" style={{ border: '1px solid #ddd' }}>
                    <OrderItem order={order} onReport={this.showReportModal}/>
                  </CardBody>
                </Collapse>
              </Card>
            )
          })}
      </React.Fragment>
    )
  }

  render() {
    const { ordersByMe, ordersForMe, error, isLoadingForMe, isLoadingByMe, editError, openMultiple } = this.state;
    const _error = error || editError
    return (
      <DocumentTitle title={pageTitleText('History')}>
        <div className="pt-3">
          {_error && 
            <Alert options={{ autoClose: false, type: 'error'}}><ListErrors {..._error} /></Alert>
          }
          <h5>
            My Transactions
            <label for="multiple" class="control-label-bold float-right">
              <input type="checkbox" id="multiple" checked={openMultiple} onChange={this.toggleMultiplePanel} className="mr-1"/>
              Open multiple
            </label>
            <div className="clearfix"></div>
          </h5>
          {this.generateTabs()}
          <TabContent activeTab={this.state.activeTab}>
            <TabPane tabId={1}>
              <div className="row pt-2">
                <div className="col-12">
                  {this.generateOrdersByMe()}
                  {(!ordersByMe || ordersByMe.hasMore) &&
                    <Waypoint
                      scrollableAncestor={'window'}
                      onEnter={this.handleWayPointEnter}
                      onLeave={this.handleWayPointLeave}
                    />
                  }
                </div>
              </div>
              {isLoadingByMe && ordersByMe && <div className="text-center font-weight-bold p-3">loading...</div>}
            </TabPane>
            <TabPane tabId={2}>
              <div className="row pt-2">
                <div className="col-12 table-responsive">
                  {this.generateOrdersForMe()}
                  {(!ordersForMe || ordersForMe.hasMore) &&
                    <Waypoint
                      scrollableAncestor={'window'}
                      onEnter={this.handleWayPointEnter}
                      onLeave={this.handleWayPointLeave}
                    />
                  }
                </div>
              </div>
            {isLoadingForMe && ordersForMe && <div className="text-center font-weight-bold p-3">loading...</div>}
            </TabPane>
          </TabContent>
        </div>
      </DocumentTitle>
    )
  }
}

const mapStateToProps = state => ({
  user: state.chat.user,
});

const mapDispatchToProps = dispatch => ({
  onSendChatError: (data) => dispatch(onSendChatError(data)),
  onReceive: (data) => dispatch(onReceiveMessage(data)),
});


export default connect(mapStateToProps, mapDispatchToProps)(Transaction)