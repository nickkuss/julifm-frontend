import React, { Component } from 'react';
import { connect } from 'react-redux'
import DocumentTitle from 'react-document-title';
import Waypoint from 'react-waypoint';
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory from 'react-bootstrap-table2-filter';
import TabContent from 'reactstrap/lib/TabContent';
import TabPane from 'reactstrap/lib/TabPane';
import classnames from 'classnames';
import Spinner from '../../spinner';
import Alert from '../../alert';
import ListErrors from '../../list-error';
import UncontrolledTooltip from 'reactstrap/lib/UncontrolledTooltip';
import userActions from '../../../actions/user';
import { formatDate, formatAmount, pageTitleText } from '../../../utils/app';
import TopUpModal from '../profile/top-up-modal';


const mapStateToProps = (state) => ({
	topUps: state.user.topUps,
	payouts: state.user.payouts,
	loadingTopups: state.user.loadingTopups,
	loadingPayouts: state.user.loadingPayouts,
	requesting: state.user.requesting,
	currentUser: state.shared.currentUser
})

const mapDispatchToProps = (dispatch) => ({
	getTopUpHistory: (query) => dispatch(userActions.getTopUpHistory(query)),
	getPayouts: (query) => dispatch(userActions.getPayouts(query)),
	requestPayout: () => dispatch(userActions.requestPayout()),
})

class TopUpHistory extends Component {

	constructor(props) {
		super(props);
		this.state = { topUp: false, activeTab: 1 };

		this.showTopUpModal = this.showTopUpModal.bind(this);
		this.onModalClose = this.onModalClose.bind(this);
		this.setTab1 = this.setTab1.bind(this);
		this.setTab2 = this.setTab2.bind(this);
		this.getColumns = this.getColumns.bind(this);
		this.handleWayPointEnter = this.handleWayPointEnter.bind(this);
		this.handleWayPointLeave = this.handleWayPointLeave.bind(this);
		this.requestPayout = this.requestPayout.bind(this)
	}

	setTab1() {
		this.setState({ activeTab: 1 })
	}

	setTab2() {
		this.setState({ activeTab: 2 })
	}

	idFormatter(cell, row, rowIndex, formatExtraData) {
		return (
			<span>{rowIndex + 1} </span>
		);
	}

	priceFormatter(cell, row) {
		return (
			<span>Rp {cell}</span>
		);
	}

	headerSortingClasses(column, sortOrder, isLastSorting, colIndex) {
		return sortOrder === 'asc' ? 'ion-chevron-up' : 'ion-chevron-down'
	}

	noDataIndication() {
		return (
			<div>
				<p>No results found.</p>
			</div>
		)
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.topUps && nextProps.topUps && nextProps.topUps.count > this.props.topUps.count) {
			this.onModalClose()
		} else if (this.props.requesting && !nextProps.requesting && this.state.activeTab !== 2) {
			// set tab 2 on successful payout request
			this.setTab2()
		}
	}

	showTopUpModal() {
		this.setState({ topUp: true })
	}

	requestPayout() {
		const { currentUser, requestPayout } = this.props;

		if (!window.confirm(`Request payout of ${formatAmount(currentUser.balance)}`)) return
		requestPayout()
	}

	onModalClose() {
		this.setState({ topUp: false, activeTab: 1 })
	}

	getColumns() {
		const { activeTab } = this.state
		const columns = [
			{
				dataField: 'id',
				text: '#',
				formatter: this.idFormatter
			},
			{
				dataField: 'createdAt',
				text: 'Date',
				formatter: (cell, row) => {
					return (
						<span>{formatDate(row.createdAt)}</span>
					)
				}
			},
			{
				dataField: 'paymentMethod',
				text: 'Method',
			},
			{
				dataField: 'amount',
				text: 'Amount',
				formatter: this.priceFormatter
			},
			{
				dataField: activeTab === 1 ? 'paymentType' : 'paymentRef',
				text: activeTab === 1 ? 'Type' : 'Ref'
			},
			{
				dataField: 'paymentInfo',
				text: 'Additional Info',
			},
			{
				dataField: 'isPaid',
				text: activeTab === 1 ? 'Confirmed' : 'Paid',
				formatter: (cell, row) => {
					return (
						row.isPaid ? <span>Yes</span> : <span>No</span>
					)
				}
			},
		];
		if (activeTab === 2) columns.push({
			dataField: 'paymentConfirmedAt',
			text: 'Date Paid',
			formatter: (cell, row) => {
				return (
					<span>{row.paymentConfirmedAt ? formatDate(row.paymentConfirmedAt): '-'}</span>
				)
			}
		})
		return columns
	}

	componentDidMount() {
		// force initial load of payouts
		this.getItems(1, 2)
	}

	getItems(page = 1, tab = this.state.activeTab) {
		const action = tab === 1 ? 'getTopUpHistory' : 'getPayouts';
		this.props[action]({ page })
	}

	handleWayPointEnter() {
		const { topUps, payouts, loadingTopups, loadingPayouts } = this.props;
		const { activeTab } = this.state;
		console.log('waypoint', activeTab, this.props)
		const data = activeTab === 1 ? topUps : payouts
		if ((activeTab === 1 && loadingTopups) || (activeTab === 2 && loadingPayouts)) return false;
		if (!data) return this.getItems();

		const { page, hasMore } = data;
		if (hasMore) this.getItems(page + 1);
	}

	handleWayPointLeave() { }

	renderWallet(type = 'wallet') {
		const { currentUser, topping, requesting } = this.props;
		const field =  type === 'wallet' ? 'wallet' : 'balance'
		return (
			<div className="col-6">
				<div className="p-3" id="wallet">
					{/* <i className="float-left ion-cash d-none d-md-block" style={{ fontSize: '2rem' }}></i> */}
					<span className="capitalize">{type}</span>
					<p className="float-right" style={{ fontSize: '1rem' }}>
						{formatAmount(currentUser[field])}
					</p>
					{currentUser.pendingWallet && type === 'wallet' ?
						<React.Fragment>
							<div className="clearfix"></div>
							<p className="mb-0 float-right" id="showPendingAmt">
								+({formatAmount(currentUser.pendingWallet)})
							</p>
							<UncontrolledTooltip placement="top" target="showPendingAmt">
								pending from bids
							</UncontrolledTooltip>
						</React.Fragment> :
						<React.Fragment>
							<div className="clearfix"></div>
							<p className="mb-0 float-right">----</p>
						</React.Fragment>
					}
					<hr />
					<button
						disabled={topping || requesting || !currentUser[field]}
						onClick={type === 'wallet' ? this.showTopUpModal : this.requestPayout}
						className="btn btn-block btn-secondary rounded-0">
						{type === 'wallet' ? 'Top Up' : 'Request Payout'}
					</button>
				</div>
			</div>
		)
	}

	generateTabs() {
		const { topUps, payouts } = this.props;
		const { activeTab } = this.state
		return (
			<div className="nav-tabs">
				<div className="nav-item pointer d-inline-block" style={{ width: '50%' }}>
					<span onClick={this.setTab1}
						className={classnames('nav-link', { active: activeTab === 1 })} >
						TOP UPS &nbsp;
            <span className={activeTab === 1 ? 'font-weight-bold' : undefined}>
							{topUps && topUps.items.length > 0 &&
								`(${topUps.items.length} of ${topUps.totalResults})`
							}
						</span>
					</span>
				</div>

				<div className="nav-item pointer d-inline-block" style={{ width: '50%' }}>
					<span onClick={this.setTab2}
						className={classnames('nav-link', { active: activeTab === 2 })} >
						PAYOUTS &nbsp;
            <span className={activeTab === 2 ? 'font-weight-bold' : undefined}>
							{payouts && payouts.items.length > 0 &&
								`(${payouts.items.length} of ${payouts.totalResults})`
							}
						</span>
					</span>
				</div>

			</div>
		);
	}

	renderTable() {
		const field = this.state.activeTab === 1 ? 'topUps' : 'payouts'
		if (!this.props[field]) return <Spinner />;
		return (
			<BootstrapTable keyField='id' filter={filterFactory()} hover striped condensed
				data={this.props[field].items} columns={this.getColumns()}
				wrapperClasses="table-responsive"
				noDataIndication={this.noDataIndication}
			/>
		)
	}

	render() {
		const { topUps, payouts, requesting, topUpError, payoutError } = this.props;
		const error = topUpError || payoutError

		return (
			<DocumentTitle title={pageTitleText('Wallet')}>
				<React.Fragment>
					{requesting && <Alert options={{ autoClose: false, type: 'default' }} className='loader'><Spinner /></Alert>}
					{error && !requesting &&
						<Alert options={{ autoClose: false, type: 'error' }}><ListErrors {...error} /></Alert>
					}
					<div className="pt-3">
						<TopUpModal isOpen={this.state.topUp} onModalClose={this.onModalClose} />
						<div className="row">
							{this.renderWallet('wallet')}
							{this.renderWallet('escrow')}
						</div>
						<hr />
						{this.generateTabs()}
						<TabContent activeTab={this.state.activeTab}>
							<TabPane tabId={1}>
								<div className="row pt-2">
									<div className="col-12">
										<h5>TOPUP HISTORY</h5>
										{this.renderTable()}
										{(!topUps || topUps.hasMore) &&
											<Waypoint
												scrollableAncestor={'window'}
												onEnter={this.handleWayPointEnter}
												onLeave={this.handleWayPointLeave}
											/>
										}
									</div>
								</div>
							</TabPane>
							<TabPane tabId={2}>
								<div className="row pt-2">
									<div className="col-12 table-responsive">
										<h5>PAYOUT HISTORY</h5>
										{this.renderTable()}
										{(!payouts || payouts.hasMore) &&
											<Waypoint
												scrollableAncestor={'window'}
												onEnter={this.handleWayPointEnter}
												onLeave={this.handleWayPointLeave}
											/>
										}
									</div>
								</div>
							</TabPane>
						</TabContent>
					</div>
				</React.Fragment>
			</DocumentTitle>

		)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(TopUpHistory)
