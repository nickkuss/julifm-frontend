import React, { Component } from "react";
import { connect } from 'react-redux';
import UncontrolledTooltip from 'reactstrap/lib/UncontrolledTooltip';
import AddressForm from './address-form';
import AddressItem from './address-item';
import classnames from 'classnames';
import Alert from '../../alert';
import Spinner from '../../spinner';
import ListErrors from '../../list-error';
import userActions from '../../../actions/user';


class Profile extends Component {
	constructor(props) {
		super(props)
		this.state = {
			newAddress: {},
			topUp: false
		}
		this.handleInputChange = this.handleInputChange.bind(this);
		this.toggleAddressForm = this.toggleAddressForm.bind(this);
		this.togglePasswordForm = this.togglePasswordForm.bind(this);
		this.handleAddressChange = this.handleAddressChange.bind(this);
		this.setDefaultAddress = this.setDefaultAddress.bind(this);
		this.submit = this.submit.bind(this);
		this.handlePhotoChange = this.handlePhotoChange.bind(this)
		this.changePhoto = this.changePhoto.bind(this)
		this.changePassword = this.changePassword.bind(this);
	}

	static getDerivedStateFromProps(nextProps, prevState) {
		if (!prevState.currentUser || (nextProps.isUpdated && !nextProps.error)) {
			const { currentUser } = nextProps;
			return { currentUser }
		}
	}

	handleInputChange(event) {
		this.handleChange(event, 'currentUser')
	}

	handleAddressChange(event) {
		this.handleChange(event, 'newAddress')
	}

	handleChange(event, field) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		const fieldValue = { ...this.state[field], [name]: value }
		const newState = { [field]: fieldValue };

		const formName = target.form && target.form.getAttribute('name');
		if (formName) newState[formName] = true;

		this.setState(newState);
	}

	toggleAddressForm() {
		this.setState({ addNewAddress: !this.state.addNewAddress });
	}

	setDefaultAddress(address) {
		this.props.updateProfile({ address: { id: address.id, isDefault: true } });
	}

	togglePasswordForm() {
		this.setState({ updatePassword: !this.state.updatePassword });
	}

	componentDidUpdate(prevProps, prevState) {
		const currentUserPrev = prevProps.currentUser;
		const currentUserCur = this.props.currentUser;
		const newState = {};
		let updated;
		if (currentUserPrev.updatedAt !== currentUserCur.updatedAt) {
			if (currentUserCur.addresses.length !== currentUserPrev.addresses.length) {
				// we just updated address
				newState.addNewAddress = false;
				updated = true;
			}
		}
		if (prevProps.isUpdated !== this.props.isUpdated && this.state.topUp) {
			newState.topUp = false;
			updated = true;
		}
		if (updated) this.setState(newState)
	}

	componentWillUnmount() {
		this.props.onUnload()
	}

	submit(event) {
		event.preventDefault();
		const formName = event.target.name;
		// this.validate()
		if (this.state[formName]) this.props.updateProfile(this.state.currentUser);
	}

	changePassword(event) {
		event.preventDefault();
		console.log('changeeeeee')
		// this.validate()
		const data = {
			password: this.state.currentUser.oldPassword,
			newPassword: this.state.currentUser.newPassword,
		}
		this.props.updatePassword(data);
	}

	handlePhotoChange(event) {
		const { target: { files } } = event
		this.setState({ photo: files[0] })
	}

	changePhoto() {
		const { photo } = this.state
		if (!photo) return;
		const form = new FormData()
		form.append('photo', photo)
		this.props.updatePhoto(form);
	}

	render() {
		const { currentUser, updatePassword, photo } = this.state;
		const { error, isLoading, isUpdated } = this.props;

		if (!currentUser) return 'loading...';

		return (
			<div className="container-fluid">
				{isUpdated && !isLoading && !error && <Alert options={{ autoClose: true, type: 'success' }} message='Profile Updated' />}
				{error && !isLoading && <Alert options={{ autoClose: false, type: 'error' }}><ListErrors {...error} /></Alert>}
				{isLoading && <Alert options={{ autoClose: false, type: 'default' }} className='loader'><Spinner /></Alert>}
				<div className="row">
					<div className="col-md-4 pt-4 order-md-2">
						<div className="m-auto text-center" style={{ maxWidth: 200 }}>
							<img src={currentUser.photo} alt={`${currentUser.username} photo`} className="thumbnail img-responsive rounded-circle"/>
							<label className="font-weight-bold" htmlFor="photo">
								<input type="file" name="photo" onChange={this.handlePhotoChange} />
							</label>
							<button className="btn btn-sm btn-secondary" disabled={!photo} onClick={this.changePhoto}>Save</button>
						</div>
						{!currentUser.isSocialAuth && !updatePassword && 
							<div>
								<hr />
								<h5 className="text-center underline pointer"
									onClick={this.togglePasswordForm}>Update Password </h5>
							</div>
						}

						{ updatePassword &&
							<React.Fragment>
								<h4 className="title">
									Change Password
									<span className="float-right ion-android-close pointer" onClick={this.togglePasswordForm}></span>
								</h4>
								<form noValidate onSubmit={this.changePassword} name="passwordForm">
									<div className="form-group">
										<label htmlFor="oldPassword">Current Password</label>
										<input type="password" id="oldPassword" name="oldPassword" onChange={this.handleInputChange} />
									</div>
									<div className="form-group">
										<label htmlFor="newPassword">New Password</label>
										<input type="password" id="newPassword" name="newPassword" onChange={this.handleInputChange} />
									</div>
									<div className="form-group">
										<label htmlFor="confirmPassword">Confirm New Password</label>
										<input type="password" id="confirmPassword" name="confirmPassword" onChange={this.handleInputChange} />
									</div>
									<button
										className={classnames('btn btn-secondary btn-block rounded-0', {
											'loading': this.props.isLoading
										})} disabled={this.props.isLoading}>Update Password</button>
								</form>
								<hr />
							</React.Fragment>
						}
					</div>

					<div className="col-md-8 float-sm-right pt-4">
						<div className="card p-3">
							<div className="content">
								<h4 className="title">Edit Profile</h4>
								<form noValidate onSubmit={this.submit} name="profileForm">
									<div className="row">
										<div className="col-md-6">
											<div className="form-group">
												<label htmlFor="firstName" className="control-label">First name</label>
												<input type="text" placeholder="First name" className="" id="firstName" name="firstName"
													value={currentUser.firstName} onChange={this.handleInputChange} />
											</div>
										</div>
										<div className="col-md-6">
											<div className="form-group">
												<label htmlFor="lastName" className="control-label">Last name</label>
												<input type="text" placeholder="Last name" className="" id="lastName" name="lastName"
													value={currentUser.lastName} onChange={this.handleInputChange} />
											</div>
										</div>
									</div>
									<div className="row">
										<div className="col-md-5">
											<div className="form-group">
												<label htmlFor="phone" className="control-label">Phone</label>
												<input type="text" placeholder="Phone" disabled="" className="" id="phone" name="phone"
													value={currentUser.phone} onChange={this.handleInputChange} />
											</div>
										</div>
										<div className="col-md-3">
											<div className="form-group">
												<label htmlFor="username" className="control-label">Username</label>
												<input type="text" placeholder="Username" className="" id="username" name="username"
													value={currentUser.username} onChange={this.handleInputChange} />
											</div>
										</div>
										<div className="col-md-4">
											<div className="form-group">
												<label htmlFor="email" className="control-label">Email</label>
												<input type="email" placeholder="Email" className="" id="email" name="email"
													value={currentUser.email} onChange={this.handleInputChange} />
											</div>
										</div>
									</div>
									<div className="row">
										<div className="col-12">
											<div className="form-group">
												<label htmlFor="bio" className="control-label">Bio</label>
												<textarea defaultValue={currentUser.bio} id="bio" name="bio" onChange={this.handleInputChange}></textarea>
											</div>
										</div>
									</div>
									<button
										className={classnames('btn btn-secondary btn-block rounded-0', {
											'loading': this.props.isLoading
										})} disabled={this.props.isLoading}>Update Profile</button>
									<div className="clearfix"></div>
								</form>
							</div>
						</div>

						<hr />

						<div className="card p-3">
							<h4 className="title">
								My Addresses
								<span className="float-right underline pointer" onClick={this.toggleAddressForm}>
									{this.state.addNewAddress ? 'Show All' : 'Add New'}
								</span>
							</h4>
							{!currentUser.addresses.length &&
								<span>
									You haven't added an address.
									<a className="pointer text-primary underline text-center"
										onClick={this.toggleAddressForm}> Add new address</a>
								</span>
							}
							{!this.state.addNewAddress && currentUser.addresses.map(address => <AddressItem key={address.id}
								address={address} onClick={this.setDefaultAddress} isLoading={isLoading} />)}
							<div style={{ display: this.state.addNewAddress ? 'block' : 'none' }}>
								<hr /><AddressForm address={this.state.newAddress} onChange={this.handleAddressChange} />
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	currentUser: state.shared.currentUser,
	isLoading: state.user.isLoading,
	isUpdated: state.user.isUpdated,
	error: state.user.error,

})

const mapDispatchToProps = (dispatch) => ({
	updateProfile: (data) => dispatch(userActions.updateProfile(data)),
	updatePassword: (data) => dispatch(userActions.updatePassword(data)),
	updatePhoto: (data) => dispatch(userActions.updatePhoto(data)),
	onUnload: () => dispatch(userActions.onUnload()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Profile);