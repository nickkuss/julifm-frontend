import React, { Component } from 'react';
import { connect } from 'react-redux';
import userActions from '../../../actions/user';
import locationActions from '../../../actions/locations';
import classnames from 'classnames';

class AddressForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			address: this.props.address || {},
		}

		this.handleChange = this.handleChange.bind(this);
		this.submit = this.submit.bind(this);
	}


	componentDidUpdate(prevProps, prevState) {
		const currentUserPrev = prevProps.currentUser;
		const currentUserCur = this.props.currentUser;
		if (currentUserCur.addresses.length !== currentUserPrev.addresses.length) {
			// we just updated address
			this.setState({address: {
				provinceId: '',
				cityId: '',
			}})
		}
	}

	submit(event) {
		event.preventDefault();
		event.stopPropagation(); // prevent parent form from submitting
		
		// const formValid = this.isValid();

		this.props.submit(this.state.address)
	}

	handleChange(event) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		const newState = { address: {...this.state.address, [name]: value } }

		if (name === 'provinceId') {
			newState.cities = this.props.cities.filter(city => city.province_id === value);
			newState.address.provinceName = target.selectedOptions[0].text;
			newState.address.cityId = '';
			newState.address.cityName = '';
		}
		
		if (name === 'cityId') {
			newState.address.cityName = target.selectedOptions[0].text;
		}

		this.setState(newState);
	}

	render() {
		const { address, cities } = this.state;
		const { isLoading, provinces } = this.props;
		return (
			<React.Fragment>
				<form noValidate onSubmit={this.submit}>
					<div className="row">
						<div className="col-md-6">
							<div className="form-group">
								<label htmlFor="firstName" className="control-label font-weight-bold">First name</label>
								<input type="text" placeholder="First name" className=""
									value={address.firstName || ''} id="firstName" name="firstName" onChange={this.handleChange} />
							</div>
						</div>
						<div className="col-md-6">
							<div className="form-group">
								<label htmlFor="lastName" className="control-label font-weight-bold">Last name</label>
								<input type="text" placeholder="Last name" className=""
									value={address.lastName || ''} id="lastName" name="lastName" onChange={this.handleChange} />
							</div>
						</div>
						<div className="col-12">
							<div className="form-group">
								<label htmlFor="phone" className="control-label font-weight-bold">Phone</label>
								<input type="text" placeholder="Phone" disabled="" className=""
									value={address.phone || ''} id="phone" name="phone" onChange={this.handleChange} />
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							<div className="form-group">
								<label htmlFor="address" className="control-label font-weight-bold">Address</label>
								<input type="text" placeholder="Address" className=""
									value={address.address || ''} id="address" name="address" onChange={this.handleChange} />
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-md-6">
							<div className="form-group">
								<label htmlFor="province" className="control-label font-weight-bold">Province</label>
								<select name="provinceId" id="province" 
									value={address.provinceId}
									onChange={this.handleChange}>
									<option value=""></option>
									{ provinces &&
										provinces.map(p => <option key={p.province_id} value={p.province_id}>{p.province}</option>)
									}
								</select>
							</div>
						</div>
						<div className="col-md-6">
							<div className="form-group">
								<label htmlFor="city" className="control-label font-weight-bold">City</label>
								<select name="cityId" id="city" 
									value={address.cityId}
									onChange={this.handleChange}>
									<option value=""></option>
									{ cities &&
										cities.map(c => <option key={c.city_id} value={c.city_id}>{c.city_name}</option>)
									}
								</select>
							</div>
						</div>
					</div>
					<div className="form-group">
						<input type="checkbox" id="isDefault" name="isDefault"
							checked={address.isDefault} value={address.isDefault} onChange={this.handleChange} />
						<label htmlFor="isDefault">Set as default: {address.isDefault}</label>
					</div>
					<button
						className={classnames('btn btn-secondary btn-block rounded-0', {
							'loading': isLoading
						})} disabled={isLoading}>Save Address</button>
					<div className="clearfix"></div>
				</form>
			</React.Fragment>
		)
	}
}


export default connect((state) => ({
	currentUser: state.shared.currentUser,
	isLoading: state.user.isLoading,
	error: state.user.error,
	locationLoading: state.location.isLoading,
	provinces: state.location.provinces,
	cities: state.location.cities
}), (dispatch) => ({
	submit: (data) => dispatch(userActions.updateProfile({ address: data }))
}))(AddressForm);
