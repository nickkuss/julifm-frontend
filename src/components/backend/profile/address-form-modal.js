import React, { Component } from 'react';
import { connect } from 'react-redux';
import Modal  from 'reactstrap/lib/Modal';
import AddressForm from './address-form';
import Alert from '../../alert';
import ListErrors from '../../list-error';

class AddressFormModal extends Component {
	constructor(props) {
    super(props);
    this.state = {address:  {isDefault: true}};
		this.close = this.close.bind(this);
		this.onClosed = this.onClosed.bind(this);
		this.handleAddressChange = this.handleAddressChange.bind(this);
  }

  close() {
    this.setState({ isOpen: false, manualClose: true});
	}

	static getDerivedStateFromProps(nextProps, state) {
		const obj = {};

		if (!nextProps.isOpen) {
			obj.isOpen = false;
			obj.manualClose = false;
		} else obj.isOpen = !state.manualClose;
		
		return obj;
	}

	handleAddressChange(event) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		const newState = { address: {...this.state.address, [name]: value } }
		this.setState(newState);
	}

	componentDidUpdate(prevProps, prevState) {
		const currentUserPrev = prevProps.currentUser;
		const currentUserCur = this.props.currentUser;
		if (currentUserPrev.updatedAt !== currentUserCur.updatedAt) {
			this.close()
			// this.setState({isOpen: false})
		}

	}

	onClosed() {
		this.props.onModalClose()
	}
	
	
	render() {
		const  { isLoading, error } = this.props;
		return (
		<Modal isOpen={this.state.isOpen}  onClosed={this.onClosed}
			fade={false} toggle={this.close} className={this.props.className}>
			<div className="modal-header">
				<h5 className="modal-title">Add New Address</h5>
				<button type="button" className="close" 
					onClick={this.close}
					aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div className="modal-body">
				{error && !isLoading && <Alert options={{ autoClose: false, type: 'error' }}><ListErrors {...error} /></Alert>}
				<AddressForm address={this.state.address} onChange={this.handleAddressChange}/>
			</div>
		</Modal>
		)
	}
}

const mapStateToProps = (state) => ({
	currentUser: state.shared.currentUser,
	isLoading: state.product.isLoading,
	error: state.user.error
})


export default connect(mapStateToProps)(AddressFormModal);
