import React, { Component } from 'react';
import classnames from 'classnames';

class AddressItem extends Component {

  constructor(props) {
    super(props);
    this._onClick = this._onClick.bind(this);
  }

  _onClick() {
    this.props.onClick(this.props.address)
  }

  render() {
    const { address, isLoading, readOnly, className } = this.props;
    return (
      <div className={`card p-2 mb-0 ${className || ''}`}>
        <span className="font-weight-bold">{address.firstName} {address.lastName}</span>
        <span>{address.address}</span>
        <span>{address.cityName} {address.cityName && address.provinceName ? ', ' : ''} {address.provinceName}</span>
        <a className="link__display" href={`tel:${address.phone}`}>{address.phone}</a>
        {!address.isDefault && !readOnly &&
          <span className={classnames('align-self-end underline pointer', {loading: isLoading})}
            onClick={this._onClick}>
            Set as default
          </span>
        }
      </div>
    )
  }
}

export default AddressItem;