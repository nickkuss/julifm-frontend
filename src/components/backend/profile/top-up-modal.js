import React, { Component } from 'react';
import { connect } from 'react-redux';
import Modal  from 'reactstrap/lib/Modal';
import Payment from '../../payment';
import Spinner from '../../spinner';
import Alert from '../../alert';
import ListErrors from '../../list-error';
import config from '../../../config';
import userActions from '../../../actions/user';

class TopUpModal extends Component {
	constructor(props) {
    super(props);
    this.state = {address:  {isDefault: true}, amount: ''};
		this.updateAmount = this.updateAmount.bind(this);
    this.close = this.close.bind(this);
    this.topUp = this.topUp.bind(this);
		this.onClosed = this.onClosed.bind(this);
  }

  close() {
    this.setState({ isOpen: false, manualClose: true});
	}

	static getDerivedStateFromProps(nextProps, state) {
		const obj = {};

		if (!nextProps.isOpen) {
			obj.isOpen = false;
			obj.manualClose = false;
		} else obj.isOpen = !state.manualClose;
		
		return obj;
	}

	onClosed() {
		this.props.onModalClose()
  }

  updateAmount(e) {
    const value = e.target.value;
    let newState = { amount: value ? Number(value) : '', amountErr: null, disableForm: false };
    if (!newState.amount || newState.amount < config.minTopUpAmount || !Number.isInteger(newState.amount)) {
      newState.amountErr = `between ${config.minTopUpAmount} and ${config.maxTopUpAmount}, without decimals`;
    } 
    this.setState(newState)
  }
  
  topUp(payment) {
    this.props.topUp({...payment, amount: this.state.amount})
  }
	
	render() {
    const  { isLoading, error } = this.props;

    console.log(this.state)
    console.log('error', error)
		return (
		<Modal isOpen={this.state.isOpen}  onClosed={this.onClosed}
			fade={false} toggle={this.close} className={this.props.className}>
			<div className="modal-header">
				<h5 className="modal-title">Add To Wallet</h5>
				<button type="button" className="close" 
					onClick={this.close}
					aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div className="modal-body">
          {error && !isLoading && <Alert options={{ autoClose: false, type: 'error' }}><ListErrors {...error} /></Alert>}
          {isLoading && <Alert options={{ autoClose: false, type: 'default'}} className='loader'><Spinner /></Alert> }
        <form >
          <div>
            <input type="number" 
              className='mb-1'
              onChange={this.updateAmount}
              placeholder="Enter amount" 
              value={this.state.amount}
              min={config.minTopUpAmount} max={config.maxTopUpAmount}/>
              {this.state.amountErr && <p className="text-small text-danger">{this.state.amountErr}</p> }
          </div>
        </form>
				<Payment 
          onSubmit={this.topUp}
          excludeWallet={true}
          excludeBalance={true}
          // showToggler={this.state.step > 3}
          // toggleChangeMethod={this.toggleChangeMethod}
          disabled={isLoading || !this.state.amount || this.state.amountErr}
        />
			</div>
		</Modal>
		)
	}
}

const mapStateToProps = (state) => ({
	currentUser: state.shared.currentUser,
	isLoading: state.user.topping
})


export default connect(mapStateToProps, (dispatch) => ({
  topUp: (data) => dispatch(userActions.topUp(data)),
}))(TopUpModal);
