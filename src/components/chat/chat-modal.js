import React, { Component } from 'react';
import { connect } from 'react-redux';
import Modal  from 'reactstrap/lib/Modal';
import ChatDetail from './detail';
import Alert from '../alert';
import ListErrors from '../list-error';
import Link from 'react-router-dom/Link';

class ChatModal extends Component {
	constructor(props) {
    super(props);
    this.state = {};
		this.close = this.close.bind(this);
		this.onClosed = this.onClosed.bind(this);
		// this.handleAddressChange = this.handleAddressChange.bind(this);
  }

  close() {
    this.setState({ isOpen: false, manualClose: true});
	}

	static getDerivedStateFromProps(nextProps, state) {
		const obj = {};

		if (!nextProps.isOpen) {
			obj.isOpen = false;
			obj.manualClose = false;
		} else obj.isOpen = !state.manualClose;
		
		return obj;
	}

	onClosed() {
		this.props.onModalClose()
	}
	
	render() {
		const  { currentUser, profile, currentRoom } = this.props;
		return (
		<Modal isOpen={this.state.isOpen}  onClosed={this.onClosed}
			fade={false} toggle={this.close} className={this.props.className}>
			<div className="modal-header">
				<h5 className="modal-title">Message {profile.username}</h5>
				<button type="button" className="close" 
					onClick={this.close}
					aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div className="modal-body">
        {!currentUser ?
          <div className="text-center mt-5 font-weight-bold">
            <Link to={'/login'} className="link__display">Login</Link> to message {profile.username}
          </div> :
          <React.Fragment>
            <ChatDetail chatMember={profile}/>
            {!currentRoom && <p className="font-weight-bold text-center">loading...</p>}
          </React.Fragment>
        }
				{/* {error && !isLoading && <Alert type="danger"><ListErrors {...error} /></Alert>} */}
			</div>
		</Modal>
		)
	}
}

const mapStateToProps = (state) => ({
  currentUser: state.shared.currentUser,
  profile: state.profile.profile,
  currentRoom: state.chat.currentRoom
})


export default connect(mapStateToProps)(ChatModal);
