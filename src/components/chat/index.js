import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import classnames from 'classnames';
import { setRoom } from '../../actions/chat';
import Spinner from '../spinner';
import ChatList from './list';
import ChatDetail from './detail';
import './style.css';

// https://pixinvent.com/free-bootstrap-template/robust-lite/html/ltr/
// https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/vertical-menu-template/chat-application.html#

moment.updateLocale('en', {
  relativeTime: {
    future: "in %s",
    past: "%s ago",
    s: '%ds',
    ss: '%ds',
    m: "%dm",
    mm: "%dm",
    h: "%dh",
    hh: "%dh",
    d: "%dd",
    dd: "%dd",
    M: "%dmonth",
    MM: "%dmonths",
    y: "a year",
    yy: "%d years"
  }
});

class Chat extends React.Component {

  renderSearch() {
    return (
      <div className="headind_srch">
        <div className="recent_heading"></div>
        <div className="srch_bar float-right">
          <div className="stylish-input-group">
            <input type="text" className="search-bar m-0 mr-3 d-inline-flex" placeholder="Search" />
            <span className="input-group-addon">
              <button type="button"> <i className="ion-search" aria-hidden="true"></i> </button>
            </span> </div>
        </div>
      </div>
    )
  }

  render() {
    const { chat: { messages, currentRoom, user: chatUser }, user } = this.props;
    const defaultAvatar = 'http://via.placeholder.com/50x50'
    const roomIds = Object.keys(messages).reverse();

    if (!chatUser) return <Spinner />

    if (!chatUser.rooms || !chatUser.rooms.length) {
      return <div className="text-center font-weight-bold mt-5"><h3>No chat at the moment</h3></div>
    }
  

    const chats = roomIds.map((roomId, index) => {
      const lastMessage = messages[roomId][messages[roomId].length - 1]
      // const lastMessage = [...messages[roomId]].reverse().find(msg => msg.user._id !== user.id)
      return {
        id: lastMessage ? lastMessage.id : index,
        roomId: lastMessage ? lastMessage.roomId : null,
        name: lastMessage ? lastMessage.user.name : 'user',
        createdAt: lastMessage ? lastMessage.createdAt : '',
        text: lastMessage ?
          (lastMessage.attachment ? 'sent a file' : lastMessage.text) : '...',
        avatar: lastMessage ? (lastMessage.user.avatar || defaultAvatar) : defaultAvatar,
        lastMessage
      }
    })

    return (
      <div className="container">
        <div className="messaging pt-4">
          <div className="inbox_msg row">
            <div className="inbox_people col-12 col-md-4 p-0 d-none d-md-block">
                <ChatList 
                  chats={chats}
                  onChatSelect={this.props.setRoom} currentRoom={currentRoom}
                />
            </div>
            {!currentRoom && 
              <div className="inbox_people col-12 col-md-4 p-0 d-block d-md-none">
                <ChatList 
                  chats={chats}
                  onChatSelect={this.props.setRoom} currentRoom={currentRoom}
                />
              </div>
            }
            {currentRoom ?
              <ChatDetail className="mesgs col-12 col-md-8 p-0 d-none d-md-block"/> :
              <div className="text-center font-weight-bold pt-5 mesgs col-12 col-md-8 p-0 d-none d-md-block"><h3>Select a chat</h3></div>
            }
            {!!currentRoom && <ChatDetail className="mesgs col-12 col-md-8 p-0 d-block d-md-none" showBackBtn={true}/>}
          </div>
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  user: state.shared.currentUser,
  chat: state.chat,
});

const mapDispatchToProps = dispatch => ({
  setRoom: (room, user) => dispatch(setRoom(room, user))
});


export default connect(mapStateToProps, mapDispatchToProps)(Chat);