import React from 'react';
import { connect } from 'react-redux';
import Chatkit from '@pusher/chatkit';
import { onReceiveMessage, setChatUser } from '../../actions/chat';
import { parseMessage } from '../../utils/chat';
import config from '../../config';

class ChatSetup extends React.Component {
  constructor(props) {
    super(props);
    const { token, user } = this.props.chat;

    this.state = {
      messages: {},
      loadEarlier: false,
      isLoadingEarlier: false,
      user: user || null,
      token,
      typing: {},
    };

    this.onReceive = this.onReceive.bind(this);
  }

  componentDidMount() {
    const { user } = this.props;
    if (user) {
      this.tokenProvider = new Chatkit.TokenProvider({
        url: `${config.pusherChatAuthEndpoint}`
      });
      this.connectChat()
    }
  }

  componentDidUpdate(prevProps) {
    const { user } = this.props;

    if (user && !this.props.chat.user && !this.isConnecting) {
      this.tokenProvider = new Chatkit.TokenProvider({
        url: `${config.pusherChatAuthEndpoint}`
      });
      this.connectChat()
    } else if (!user && this.props.chat.user) {
      this.clearSubscription()
    }
  }

  componentWillUnmount() {
    this.clearSubscription()
  }

  clearSubscription() {
    const { chat: { user } } = this.props;
    if (!user) return;

    user.disconnect(); 
    this.props.setUser(null);
  }

  connectChat() {
    this.isConnecting = true;
    const { user } = this.props;

    const chatManager = new Chatkit.ChatManager({
      instanceLocator: config.pusherChatInstanceLocator,
      userId: user.id,
      tokenProvider: this.tokenProvider
    });

    chatManager.connect()
    .then(currentUser => {
     this.isConnecting = false;
      this.props.setUser(currentUser);
      return Promise.all(
        currentUser.rooms.map(room => currentUser.subscribeToRoom({
          roomId: room.id,
          hooks: { onNewMessage: this.onReceive },
          messageLimit: config.numChatItems
        }))
      )
    }).then(rooms => {this.isConnecting = false; console.log('all connected') })
    .catch(error => {
      this.isConnecting = false;
      console.log('chatroom connect error', error)
    })
  }

  // BEGIN slack clone
  setRoom(room) {
    this.setState({ room, sidebarOpen: false })
    this.scrollToEnd()
  }

  joinRoom(room) {
    this.actions.setRoom(room)
    this.actions.subscribeToRoom(room)
    this.state.messages[room.id] &&
      this.actions.setCursor(
        room.id,
        Object.keys(this.state.messages[room.id]).pop()
      )
  }

  subscribeToRoom(room) {
    !this.state.user.roomSubscriptions[room.id] &&
    this.state.user.subscribeToRoom({
      roomId: room.id,
      hooks: { onNewMessage: this.actions.addMessage },
    })
  }

  createRoom(options) {
    this.state.user.createRoom(options).then(this.actions.joinRoom)
  }

  // createConvo(options) {
  //   if (options.user.id !== this.state.user.id) {
  //     const exists = this.state.user.rooms.find(
  //       x =>
  //         x.name === options.user.id + this.state.user.id ||
  //         x.name === this.state.user.id + options.user.id
  //     )
  //     exists
  //       ? this.actions.joinRoom(exists)
  //       : this.actions.createRoom({
  //           name: this.state.user.id + options.user.id,
  //           addUserIds: [options.user.id],
  //           private: true,
  //         })
  //   }
  // }

  setCursor(roomId, position) {
    const { chat: { user } } = this.props;
    user.setReadCursor({ roomId, position: parseInt(position, 10) })
      .then(x => this.forceUpdate())
  }

  // isTyping(room, user) {
  //   this.setState(set(this.state, ['typing', room.id, user.id], true))
  // }

  // notTyping(room, user) {
  //   this.setState(del(this.state, ['typing', room.id, user.id]))
  // }

  onReceive(data) {
    console.log('received', data)
    const { senderId, id, roomId, text, createdAt } = data;
    const { chat: { user, currentRoom } } = this.props;
    
    const incomingMessage = parseMessage(data)
    this.props.onReceive(incomingMessage)
    if (roomId === currentRoom) {
      // Update cursor if the message was read
      const cursor = user.readCursor({ roomId }) || {}
      const cursorPosition = cursor.position || 0
      cursorPosition < id && this.setCursor(roomId, id)
      // this.scrollToEnd()
    }
  }

  onSend([message]) {
    const { chat: { currentRoom, user } } = this.props;
    user.sendMessage({
      text: message.text,
      roomId: currentRoom
    });
  }

  render() {
    return null;
  }
}

const mapStateToProps = state => ({
  user: state.shared.currentUser,
  chat: state.chat
});

const mapDispatchToProps = dispatch => ({
  setUser: (data) => dispatch(setChatUser(data)),
  onReceive: (data) => dispatch(onReceiveMessage(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ChatSetup);
