import React from 'react';
import './style.css';
import { connect } from 'react-redux';
import TextareaAutosize from 'react-autosize-textarea'
import { onSendChatError, updateChatRoom, setRoom, onReceiveMessage } from '../../actions/chat';
import Waypoint from 'react-waypoint';
import config from '../../config';
import { parseMessages, parseMessage } from '../../utils/chat';

class ChatDetail extends React.Component {

  constructor(props) {
    super(props);

    this.onSend = this.onSend.bind(this);
    this.loadItems = this.loadItems.bind(this);
    this.handleWayPointEnter = this.handleWayPointEnter.bind(this);
    this.handleWayPointLeave = this.handleWayPointLeave.bind(this);
    this.onReceive = this.onReceive.bind(this);
    this.clearRoom = this.clearRoom.bind(this);
    this.state = { 
      hasMore: this.hasMoreItems(),
      page: 0, loadingVisible: false 
    }
    this.count = 0;
  }

  clearRoom() {
    this.props.setRoom(null)
  }

  hasMoreItems() {
    const {
      chat: { currentRoom, messages }
    } = this.props
    if (!currentRoom || !messages[currentRoom]) return false

    return messages[currentRoom].length >= config.numChatItems
  }

  componentDidMount() {
    this.createConvo()
  }

  createConvo() {
    console.log('creating convo', this.props)
    const {
      chat: { user, currentRoom },
      chatMember,
    } = this.props;

    if (currentRoom) return this.joinRoom(currentRoom);

    console.log('currentRoom not exists');
    if (!chatMember || !user) return;

    if (chatMember.id !== user.id) {
      console.log('here', chatMember.id, user.id);
      const room = user.rooms.find(x => x.name === user.id + chatMember.id || x.name === chatMember.id + user.id)
      console.log('room iss', room)
      room ?
        this.joinRoom(room.id) :
        user.createRoom({
          name: user.id + chatMember.id,
          addUserIds: [chatMember.id],
          private: true,
        }).then(room => {
          user.subscribeToRoom({
            roomId: room.id,
            hooks: { onNewMessage: this.onReceive },
          })
          this.joinRoom(room.id)
        })
          .catch(e => console.log('eeee', e))
    }
  }

  joinRoom(roomId) {
    console.log('joining Room')
    const { chat: { user, messages, currentRoom } } = this.props;
    this.scrollToBottom();
    
    if (user && messages[roomId] && messages[roomId].length) {
      user.setReadCursor({ roomId, position: messages[roomId][messages[roomId].length - 1].id })
    }

    if (!currentRoom) this.props.setRoom(roomId)
  }

  onReceive(data) {
    console.log('received')
    const { senderId, id, roomId, text, createdAt } = data;
    const { chat: { user, currentRoom } } = this.props;
    
    const incomingMessage = parseMessage(data)
    this.props.onReceive(incomingMessage)
    if (roomId === currentRoom) {
      // Update cursor if the message was read
      const cursor = user.readCursor({ roomId }) || {}
      const cursorPosition = cursor.position || 0
      cursorPosition < id && this.setCursor(roomId, id)
      this.scrollToBottom()
    }
  }

  setCursor(roomId, msgId) {
    const { chat: { user } } = this.props;

    user.setReadCursor({ roomId, position: msgId })
  }

  componentDidUpdate(prevProps) {
    const { 
      chat: { messages, currentRoom, hasNewMessage },
      user
    } = this.props;
    
    if (hasNewMessage) {
      this.scrollToBottom();
    }

    if (currentRoom === prevProps.chat.currentRoom) {
      if (!prevProps.chat.messages[currentRoom] || !messages[currentRoom]) return 
      if (currentRoom && prevProps.chat.messages[currentRoom].length < messages[currentRoom].length) {
        this.lastMessageId = `_msg${prevProps.chat.messages[currentRoom][0].id}`
        const lastMessageItem = document.querySelector(`#${this.lastMessageId}`)
        
        if (lastMessageItem) {
          lastMessageItem.scrollIntoView({ behavior: 'smooth' })
        }
      } 
    } 
  }

  componentWillUnmount() {
    this.messagesEnd = null;
    this.lastMessageId = null;
    this.props.setRoom(null);
  }

  userHasNextMsg(currentIndex) {
    const { 
      chat: { messages, currentRoom },
    } = this.props;

    const currentChatMsg = messages[currentRoom] || [];
    const nextMsg = currentChatMsg[currentIndex + 1]
    return (nextMsg && nextMsg.user.id === currentChatMsg[currentIndex].user.id) 
  }

  scrollToBottom = () => {
    if (!this.messagesEnd) return;
    this.messagesEnd.scrollIntoView({ behavior: 'smooth' });
  }

  onSend() {
    if (!this.textarea || !this.textarea.value || !this.textarea.value.trim()) return;
    
    const { 
      chat: { user, currentRoom },
    } = this.props;

    const data = {text: this.textarea.value.trim(), roomId: currentRoom}

    user.sendMessage(data)
    .then(() => {
      this.textarea.value = '';
      this.scrollToBottom()
    })
    .catch(err => this.props.onSendChatError(err))
  }

  handleWayPointEnter() {
    const { page, hasMore } = this.state;
    if (hasMore) this.loadItems(this.state.page);

    this.setState({ loadingVisible: true, page: page + 1 })
  }

  handleWayPointLeave() {
    this.setState({ loadingVisible: false })
  }

  loadItems(page) {
    if (!page) return; // prevent initial fetching
    
    const { 
      chat: { user, currentRoom, messages },
    } = this.props;
    const oldestMessageIdReceived = Math.min(...((messages[currentRoom] || []).map(m => m.id)))
    
    user.fetchMessages({
      roomId: currentRoom,
      initialId: oldestMessageIdReceived,
      direction: 'older',
      limit: config.numChatItems,
    })
    .then(messages => {
      this.props.updateChatRoom(parseMessages(messages))
      this.setState({ hasMore: messages && messages.length >= config.numChatItems })
      
    })
    .catch(err => {
      console.log('Error fetching messages', err)
    })
  }

  render() {
    const { 
      chat: { messages, currentRoom, currentRoomUser },
      user,
      showBackBtn,
    } = this.props;
    
    if (!currentRoom) return null; // or loader

    return (
      <div className={this.props.className}>
        { showBackBtn && 
          <div class="p-2 text-center" style={{ borderBottom: '1px solid #eee', minHeight: 50 }}>
            <h5>
              <i className="ion-chevron-left mr-2 float-left p-1 pl-2 pr-2 pointer" onClick={() => this.clearRoom(null)}></i>
              { currentRoomUser }
              </h5>
          </div> 
        }
        <div className="msg_history">
          {messages[currentRoom] && this.state.hasMore &&
            <React.Fragment>
              <Waypoint
                onEnter={this.handleWayPointEnter}
                onLeave={this.handleWayPointLeave}
              />
              {this.state.loadingVisible && <div className="text-center">loading...</div>}
            </React.Fragment>
          }

          {(messages[currentRoom] || []).map((msg, index) => {
            const isSender = msg.user && msg.user.id === user.id;
            const userHasNextMsg = this.userHasNextMsg(index);
            return (
              <div id={`_msg${msg.id}`}
                className={isSender ? 'outgoing_msg': 'incoming_msg'} key={msg.id}
                style={{ marginBottom: userHasNextMsg ? '2px' : '12px'}} >
                {!isSender && 
                  <div className="incoming_msg_img">
                    {!userHasNextMsg && <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil" />}
                  </div>
                }
                <div className={isSender ? 'sent_msg': 'received_msg'}>
                  <div className={isSender ? '' : 'received_withd_msg'}>
                    <p>{msg.text}</p>
                    <span className="time_date"> {msg.createdAt}</span></div>
                </div>
              </div>
            )
          })}
         
          <div style={{ float:"left", clear: "both" }}
              ref={(el) => { this.messagesEnd = el; }}>
          </div>
        </div>
        <div className="type_msg">
          <div className="input_msg_write">
            <TextareaAutosize innerRef={ref => this.textarea = ref}
              rows={1} className="write_msg" onChange={this.handleInputChange}/>
            <button className="msg_send_btn" type="button" onClick={this.onSend}>
              <i className="ion-paper-airplane" aria-hidden="true"></i>
            </button>
          </div>
        </div>
      </div>
    )
  }

}
const mapStateToProps = state => ({
  user: state.shared.currentUser,
  location: state.router.location,
  chat: state.chat,
});

const mapDispatchToProps = dispatch => ({
  onSendChatError: (data) => dispatch(onSendChatError(data)),
  updateChatRoom: (data) => dispatch(updateChatRoom(data)),
  setRoom: (data) => dispatch(setRoom(data)),
  onReceive: (data) => dispatch(onReceiveMessage(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ChatDetail);