import React from 'react';
import { connect } from 'react-redux';
// import moment from 'moment';
import classnames from 'classnames';
// import { setRoom } from '../../actions/chat';
// import ChatDetail from './detail';
// import './style.css';

const ChatList = ({ chats, currentRoom, onChatSelect, className }) => (
  <div className={classnames("inbox_chat", {[className]: !!className})}>
    {chats.map(chat => (
      <div className={classnames('chat_list pointer', {
        active_chat: currentRoom === chat.roomId
      })} onClick={() => onChatSelect(chat.roomId, chat.name)} key={chat.id}>
        <div className="chat_people">
          <div className="chat_img"><img src={chat.avatar} alt={`${chat.name} photo`} className="rounded-circle"/></div>
          <div className="chat_ib">
            <h5 className="chat_name">{chat.name} <span className="chat_date text-muted">{chat.createdAt}</span></h5>
            <p className="chat_txt">{chat.text}</p>
          </div>
        </div>
      </div>
    ))}
  </div>
)

export default ChatList;