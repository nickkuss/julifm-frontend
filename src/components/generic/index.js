import React, { Component } from 'react';
import { connect } from 'react-redux';
import categoryUtils, { findCategoryByName } from '../../utils/categories';
import productActions from '../../actions/product';
import BreadCrumb from '../breadcrumb';
import CatalogListing from '../catalog-listing/algolia';
import Spinner from '../spinner';
import ProductDetail from '../product-detail';
import ListErrors from '../list-error';
import Alert from '../alert';

const mapStateToProps = (state) => ({
	product: state.product.product,
	categories: state.shared.categories,
	...state.genericSlug
})

const mapDispatchToProps = (dispatch) => ({
	getCategoryItems: (category) => dispatch(productActions.getProductsForCategory(category)),
	getProductDetail: (slug) => dispatch(productActions.getProductDetail(slug)),
	onUnload: () => dispatch(productActions.onGenericPageUnload()),
})

class Generic extends Component {
	constructor(props) {
		super(props);
		this.state = {}
	}

	componentWillUnmount() {
		this.props.onUnload();
	}

	render() {
		const { isLoading, categories, error, match, product } = this.props;
		if (!categories) return null;
		if (isLoading && !error) return <Spinner />

		const isProductUrl = match.path === '/i/:slug';
		let slug = match.params.slug;
		if (isProductUrl) {
			let category;
			if (product) category = findCategoryByName(categories, product.category)
			slug = category ? category.slug : null
		}

		return(
			<div>
				{error && 
					<Alert options={{ autoClose: false, type: 'error'}}>
						<ListErrors {...error} />
					</Alert> 
				} 
				{!!slug &&
					<BreadCrumb 
						items={categoryUtils.getCrumb(categories, slug, product)} 
						pathName="slug" className="d-none d-md-flex d-lg-flex pl-0 mb-0"
					/>
				}
				{ isProductUrl ? <ProductDetail /> : <CatalogListing slug={slug} categories={categories}/> }
			</div>
		)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Generic);