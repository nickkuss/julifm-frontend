import React from 'react';
import { connect } from 'react-redux';
import Link from 'react-router-dom/Link';
import './style.css';
import FollowUserButton from './follow-user-button';
import config from '../../config'


export default connect(state => ({
	currentUser: state.shared.currentUser,
	user: state.profile.profile
}), (dispatch) => ({
	toggleFollowUser: (user) => console.log('cliekce', this)
}))((props) => (
	<div className="box profile-preview p-3">

		<img src={props.photo} alt="" className="img-thumbnail img-fluid mr-2 mr-xl-3" />
		<p className="p-info">
			<Link to={'/user/' + props.username} className="font-weight-bold">{props.displayName}</Link>
			<i className="ion-ribbon-b float-right featured-seller"></i>
		</p>
		<p className="p-info">
			<i className="ion-ios-location mr-1"></i>
			{props.location ? props.location.cityName : 'N/A'} &nbsp; | &nbsp; {props.meta.averageRating} <i className="ion-star mr-1" style={{ color: config.starRatingColor }}></i>
		</p>
		{ props.user && (!props.currentUser ||  props.currentUser.id !== props.user.id) && 
			<React.Fragment>
				<FollowUserButton/>
				<i className="ion-chatboxes p-chat" onClick={props.onChatSelect}></i>
			</React.Fragment>
		}
		{ props.user && props.user.recentProducts && props.user.recentProducts.length > 0 &&
			<div className="row m-auto w-100">
				 {props.user.recentProducts.map(p =>
					<div className="col-4 more-products p-1">
						<Link to={p.slug} style={{ backgroundImage: `url(${p.images[0].url})` }}>
							<img src={p.images[0].url} alt="blank_photo" /></Link>
					</div>
				 )}
			</div>
		}
	</div>
))