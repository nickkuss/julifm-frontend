import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'
import classnames from 'classnames';
import profileActions from '../../actions/profile';
import Alert from '../alert';
import ListErrors from '../list-error';

class FollowUserButton extends Component {

	constructor(props) {
		super(props);

		this.state = {activeTab: 1}

		this.toggleFollowUser = this.toggleFollowUser.bind(this);
	}

	toggleFollowUser() {
    const { profile, user } = this.props;
    if (!user) return this.redirectToLogin();

    if (!profile) return false;

    if (!profile.isFollowing) {
      this.props.followUser(profile.id);
    } else {
      this.props.unFollowUser(profile.id);
    }
  }
  
  redirectToLogin() {
		if (this.props.history) {
			this.props.history.push({
				pathname: '/login',
				state: { referrer: window.location.pathname }
			})
		}
	}

	render() {
    const { profile, isLoading, error} = this.props;

		if (!profile) return null;

		return (
      <React.Fragment>
        {error && 
          <Alert options={{ autoClose: false, type: 'error'}}><ListErrors {...error} />
          </Alert> 
        }
        <button className={classnames('mr-3 rounded-0', {
          'btn-primary': !profile.isFollowing,
          'btn-default': profile.isFollowing,
          'loading': isLoading,
          [this.props.className]: this.props.className
        })} disabled={isLoading} id={this.props.id}
        onClick={this.toggleFollowUser}>{profile.isFollowing ? 'Following' : 'Follow'}</button>
      </React.Fragment>
		)
	}
}

export default withRouter(
  connect((state) => ({...state.profile, user: state.shared.currentUser}), (dispatch) => ({
    followUser: (userId) => dispatch(profileActions.followUser(userId)),
    unFollowUser: (userId) => dispatch(profileActions.unFollowUser(userId))
  }))(FollowUserButton)
)