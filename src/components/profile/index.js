import React, { Component } from 'react';
import  { connect } from 'react-redux';
import ProfileHeader from './profile-header';
import Spinner from '../spinner';
import ListErrors from '../list-error';
import Alert from '../alert';
// import ProductGr
import profileActions from '../../actions/profile';
import ProductGroup from '../product/product-group';
import ChatModal from '../chat/chat-modal';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {chatOpen: false};
    this.handleInputChange = this.handleInputChange.bind(this)
    this.infiniteScrollAction = this.infiniteScrollAction.bind(this);
    this.onChatSelect = this.onChatSelect.bind(this);
    this.onModalClose = this.onModalClose.bind(this);
  }

  handleInputChange(e) {
    this.setState({ query: e.target.value })
  }

  infiniteScrollAction(page) {
    const id = this.props.match.params.id;
    const { query } = this.state
    return this.props.infiniteScrollAction(id, { page, query });    
  }

  onChatSelect() {
    this.setState({ chatOpen: true })
  }

  onModalClose() {
		this.setState({ chatOpen: false })
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.getUserInfo(id);
  }

  componentWillUnmount() {
    this.props.onUnload()
  }

  render() {
    const { profile, error, currentUser } = this.props;

    if (!profile) {
      return error ? 
      <Alert options={{ autoClose: false, type: 'error',
      }} >
        <ListErrors {...error} />
      </Alert> : <Spinner />
    }
    return (
      <div className="pt-4 pl-0 pl-md-4">
        <ChatModal isOpen={this.state.chatOpen} onModalClose={this.onModalClose}/>
        <ProfileHeader profile={profile} onChatSelect={this.onChatSelect} currentUser={currentUser}/>
        <div className="clearfix mb-3"></div>
        {false && profile.products && profile.products.count > 5 && 
          <div className="form-group">
            <input
              type="text" 
              className="ml-md-4 float-right mt-3 seller-query"
              name="query"
              placeholder="search products name"
            />
            <div className="clearfix"></div>
          </div>
        }
        { profile.products && 
          <ProductGroup products={profile.products} action={this.infiniteScrollAction} field="products"/>
        }
      </div>
    )
  }
}

export default connect(state => ({
	currentUser: state.shared.currentUser,
  profile: state.profile.profile,
  error: state.profile.error
}), (dispatch) => ({
  onUnload: () => dispatch(profileActions.onSellerPageUnload()),
  infiniteScrollAction: (id, page) => profileActions.getSellerInfoInfinite(id, page),
  getUserInfo: (id) => dispatch(profileActions.getSellerInfo(id))
}))(Profile)