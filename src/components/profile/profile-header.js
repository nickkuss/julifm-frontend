import React from 'react';
import FollowUserButton from './follow-user-button';
import config from '../../config'
import './profile-header.css';
import './style.css';

const ProfileHeader = ({ profile, onChatSelect, currentUser }) => (
  <div>
    <img className="float-left mr-3 mr-md-5" id="profile-img"
      src={profile.photo} alt="profile_photo" />
    <span>
      <h5 className="font-weight-bold">
        {profile.username}
        <i className="ion-ribbon-b featured-seller ml-2"
          style={{fontSize: 'initial'}}></i>
      </h5>

      {(!currentUser || currentUser.id !== profile.id) && 
        <React.Fragment>
          <FollowUserButton className="d-inline-block" id="BUTTON_13">
          </FollowUserButton>
          <i className="ion-chatboxes p-chat ml-3" onClick={onChatSelect}></i>
        </React.Fragment>
      }
    </span>
    <ul id="UL_20" className="mt-2">
        <li id="LI_21">
          <span id="SPAN_22"><span id="SPAN_23">{profile.productsCount}</span> listings</span>
        </li>
        <li id="LI_24">
          <span id="A_25"><span id="SPAN_26">{profile.followersCount}</span> followers</span>
        </li>
        <li id="LI_27">
          <span id="A_28"><span id="SPAN_29">
            {profile.meta.averageRating}</span> 
            <i className="ion-star mr-1" style={{ color: config.starRatingColor }}></i>
          </span>
        </li>
        {profile.meta.sellCount > 0 &&
          <li id="LI_24">
            <span id="A_25"><span id="SPAN_26">{profile.meta.sellCount}</span> Sales</span>
          </li>
        }
      </ul>
  </div>
)

export default ProfileHeader;