import React from 'react';
import Link from 'react-router-dom/Link';
import classnames from 'classnames';

import './style.css';

export default ({items, crumbName, pathName, className}) => (
	!items ? null : (
		<div className={classnames('breadcrumb', {[className]: className})}>
			{items.map((item, index) => {
				return index === items.length - 1 ? (
					<span className="breadcrumb-item active" key={item.name || crumbName}>
						{item.name || item[crumbName]}
					</span>
				) :  (
					<Link className="breadcrumb-item" to={`/catalog/${item.path || item[pathName]}`}
						key={item.path || item[pathName]}>
						{item.name || item[crumbName]}
					</Link>
				);
			})}
		</div>
	)
);
