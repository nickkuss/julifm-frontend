import React from 'react';
import { connect } from 'react-redux';
import AddToCartButton from '../add-cart-button';
import Link from 'react-router-dom/Link';
import { formatAmount } from '../../utils/app';
import './style.css';
import AuctionTimer from '../auction-timer';

const formatPrice = (price) => {
	if (!price) return;

	const unit = price.toString().length < 10 ? 'RB' : 'JT'; // value + dot + 2 decimal places
	const divisor = price.toString().length < 10 ? 1000 : 1000000;
	const float = parseFloat(price / divisor);
	return ( float.toFixed(2) + ' ' + unit )
}

class Product extends React.Component {
	constructor(props) {
		super(props);
		this.onAuctionTimerEnd = this.onAuctionTimerEnd.bind(this);

		this.state = { auctionTimerExpired: false }
	}
	
	onAuctionTimerEnd() {
		this.forceUpdate()

		if (!this.state.auctionTimerExpired) this.setState({ auctionTimerExpired: true });
	}

	render() {
		const { product, onAuctionTimerEnd, currentProfile, ...rest } = this.props;
		if (!product) return null;
		
		return (
			<div className={"product-item " + (rest.className ? rest.className : "")}>
				<div className={"product-item-wrapper " + (rest.wrapperClass ? rest.wrapperClass : "")}>
					{product.owner && 
						<div className="product-seller pl-2 pr-1 pb-1">
							<img src={product.owner.photo} alt="" className="img-thumbnail img-fluid mr-2 mr-xl-2 rounded-circle" style={{ maxWidth: 30 }}/>
							{!currentProfile || currentProfile.id !== product.owner.id ?
								<Link to={'/user/' + product.owner.username} className="font-weight-bold">{product.owner.username}</Link> :
								<span className="font-weight-bold">{product.owner.username}</span>
							}
							{product.owner.isFeatured && <i className="ion-ribbon-b ml-1 float-right featured-seller" style={{ fontSize: '1.2rem' }}></i> }
						</div>
					}
					
					<Link to={'/i/' + product.slug}>
						<div className="img-holder" style={{backgroundImage: `url(${product.image || product.images[0].url})`}}>
						</div>
						<div className="content product-caption p-2">
							<div className="product-name">{product.name.slice(0, 30)}</div>
							<div className="product-name-hover d-none d-md-up">{product.name}</div>
							<div className="product-price pt-2">
								{/* {product.onSale  && <span className="oldprice d-block d-md-none d-lg-none">Rp {formatPrice(product.originalPrice)}</span> } */}
								{product.auction ?
									<React.Fragment>
										<span className="sky-blue price__btn price__auction mr-2">
											<i className="ion-hammer text-muted mr-1"></i>
											{formatAmount(product.auction.currentAmount)}
										</span>
										
										<span className="sky-blue price__btn price__auction">
											<AuctionTimer auction={product.auction} onComplete={this.onAuctionTimerEnd}/>
										</span>
									</React.Fragment> :
									<span className="sky-blue price__btn">{formatAmount(product.salePrice)}</span>
								} 
								{/*product.onSale  && <span className="oldprice d-md-inline d-lg-inline">Rp {formatPrice(product.originalPrice)}</span>*/}
								{/* <span className="price__discount pr-1 pl-1">-10%</span> */}
							</div>
							<hr className="mt-2 mb-1" />

							<div className="product-actions d-flex justify-content-around" style={{ fontSize: '1rem' }}>
								<i className={`text-danger ion-ios-heart${product.isBookmarked ? '' : '-outline'}`}></i> 
								<span>
									<i className="ion-chatbox text-muted"></i>
									{product.meta && product.meta.reviewCount > 0 &&
										<span style={{ fontSize: '.6rem' }}>{product.meta.reviewCount}</span>
									}
								</span>
								<AddToCartButton product={product}/>
							</div>
						</div>
					</Link>
					{
						product.isShared &&
						<div className="font-weight-bold">
							Shared by <Link to={`/user/${product.sharedBy}`} className="link__display">{product.sharedBy}</Link>
						</div>
					}
				</div>
			</div>
			
		)
	}
}

export default connect(state => ({
	currentProfile: state.profile.profile,
}), null)(Product)