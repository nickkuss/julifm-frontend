import React from 'react';
import Product from './index';
import InfiniteScroll from 'react-infinite-scroller';

export class ProductGroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = { products: props.products, isSet: false };
  }

  loadItems(page) {
    console.log('pagggggeeee', page);
    const field = this.props.field

    this.props.action(page)
      .then(response => {
        console.log('response', response)
        const productData = field ? response[field] : response;
        const products = { 
          ...this.state.products,
          page: productData.page,
          hasMore: productData.hasMore,
          count: this.state.products.count + productData.count,
          items: [...this.state.products.items, ...productData.items]
        };
        
        this.setState({ products })
    })
    .catch(err => console.log('caught error', err))
  }

  // static getDerivedStateFromProps (nextProps, state) {
  //   console.log('derived', nextProps, state)
  //   if (!state.isSet && nextProps.products) {
  //     console.log('patchinggggggg');
  //     const { count, hasMore, page } = nextProps.products;
  //     return {products: nextProps.products.items, count, hasMore, page: page || 0, isSet: true };
  //   } else {
  //     console.log('aeeeeee')
  //   }

  //   return null;
  // }

  componentDidMount() {
    console.log('mounted', this.state);
  }

  componentDidUpdate() {
    console.log('group updated', this.state);
  }

  render() {
    console.log('render', this.state)
    const loader = <div className="loader">Loading ...</div>;
    const { products } = this.state;
    if (!products) return null;

    const items = products.items.map(product => <Product key={product.slug} product={product} className={this.props.className || "col-lg-3 col-md-4 col-sm-4 col-6 mb-3"} />)

    return (
      <InfiniteScroll
        pageStart={1}
        loadMore={this.loadItems.bind(this)}
        hasMore={products.hasMore}
        initialLoad={false}
        loader={loader}>
        <div className="row product-group">
          {items}
        </div>
      </InfiniteScroll>
    )
  }
}

export default ProductGroup