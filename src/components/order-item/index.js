import React from 'react';
import Link from 'react-router-dom/Link';
import AddressItem from '../backend/profile/address-item';
import classnames from 'classnames';
import { getOrderInfoBySeller } from '../../utils/order';
import { formatAmount } from '../../utils/app';

const OrderItem = ({ order, onTrack, onReport }) => {
  if (!order) return null;
  const orderInfoBySeller = getOrderInfoBySeller(order.items);

  return (
    <React.Fragment>
      <div className="row pb-1">
        <div className="col-12 col-md-4">
          <AddressItem readOnly={true} address={order.deliveryAddress} className="h-100" />
        </div>
        <div className="col-12 col-md-8 text-center table-responsive mt-1">
          <table className="mb-0">
            <thead>
              <tr>
                <th>Payment Method</th>
                <th>Payment Status</th>
                <th>Additional Info</th>
                <th>Shipping Cost</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{order.paymentType}</td>
                <td>
                  <span style={{ fontSize: order.status === 'paid' ? '.9rem' : undefined }}
                    className={classnames({ 'badge badge-success': order.status === 'paid' })}>
                  {order.status}</span>
                </td>
                <td>{order.paymentInfo}</td>
                <td>{order.shippingCost ? formatAmount(order.shippingCost) : '-'}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

  
      {Object.keys(orderInfoBySeller).map(seller => (
        <div className="card" key={seller}>
          <div className="col-12 font-weight-bold link__display card-header mb-1 bg-muted" style={{ fontSize: '1.2rem' }}>
            <Link to={`/user/${seller}`}>{seller}</Link>
            <span className="float-right"  style={{ fontSize: '.9rem' }}>
              <i className="ion-ios-telephone mr-1"></i>
              <a href={`tel:${orderInfoBySeller[seller].phone}`}>{orderInfoBySeller[seller].phone}</a>
            </span>
          </div>
          <div className="col-12 pl-3">
            <div className="row">
              {orderInfoBySeller[seller].orders.map((item, index) => (
                <React.Fragment key={item.id}>
                  <div className={classnames("col-12 col-md-6 col-lg-4", {
                    'bg-disabled': item.status === 'reported'
                  })}>
                    <Link to={'/i/' + item.url} className="float-left mr-2">
                      <p style={{
                        width: 50, height: 50, backgroundSize: 'cover',
                        background: `url(${item.image}) no-repeat center`
                      }}></p>
                    </Link>
                    <div className="d-flex flex-column">
                      <Link to={'/i/' + item.url} >
                        {item.name} &nbsp;
                        <span className="font-weight-bold">({item.quantity})</span>
                      </Link>
                      <span>{formatAmount(item.price)}</span>
                      <span className="d-flex justify-content-between">
                        <span className={classnames('p-1 badge capitalize mb-1', {
                          'badge-secondary': item.status === 'started',
                          'badge-danger': item.status === 'reported',
                          'badge-success': item.status === 'confirmed' || item.status === 'completed',
                          'badge-info': item.status === 'shipped' || item.status === 'delivered',
                          })}>{item.status}
                        </span>
                        {item.trackingInfo && item.status !== 'reported' &&
                          <span className="font-weight-bold link__display" onClick={onTrack}>
                            <i className="ion-location mr-1"></i>Track
                          </span>
                        }
                        {item.status === 'delivered' &&
                          <span className="text-danger link__display" onClick={() => onReport(order, item)}>Report</span>
                        }
                      </span>
                      {item.courier &&
                        <React.Fragment>
                          <span className="d-block">{item.courier.name}</span>
                          <span className="d-block">{item.courier.service}</span>
                        </React.Fragment>
                      }
                    </div>
                  </div>
                  <div className="col-12 pb-1 pt-1 d-flex d-md-none" style={{ borderTop: '1px solid #f5f5f5', marginLeft: '0.35rem' }}></div>
                  {(index > 0 && index % 2 === 0) && <div className="col-12 pb-1 pt-1 d-none d-md-block" style={{ borderTop: '1px solid #f5f5f5', marginLeft: '0.35rem' }}></div>}
                </React.Fragment>
              ))}
            </div>
          </div>
        </div>
      ))}
    </React.Fragment>
  )
}

export default OrderItem;