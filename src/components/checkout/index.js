import React, { Component } from 'react';
import { connect } from 'react-redux';
import DocumentTitle from 'react-document-title';
import Navbar from 'reactstrap/lib/Navbar';
import Link from 'react-router-dom/Link';
import AddressFormModal from '../backend/profile/address-form-modal';
import ListErrors from '../list-error';
import Alert from '../alert';
import Spinner from '../spinner';
import Payment from '../payment';
import classnames from 'classnames';
import userApi from '../../api/user';
import cartActions from '../../actions/cart';
import { formatAmount, pageTitleText } from '../../utils/app';
import './style.css';
import logo from '../../assets/logo.png'

class Checkout extends Component {

  constructor(props) {
    super(props);

    this.state = {
      step: 1, changeAddress: false, addNewAddress: false
    }

    this.toggleChangeAddress = this.toggleChangeAddress.bind(this);
    this.toggleChangeMethod = this.toggleChangeMethod.bind(this);
    this.showAddressModal = this.showAddressModal.bind(this);
    this.onModalClose = this.onModalClose.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleCourierSelect = this.handleCourierSelect.bind(this);
    this.handleCostSelect = this.handleCostSelect.bind(this);
    this.useAddress = this.useAddress.bind(this);
    this.choosePaymentMethod = this.choosePaymentMethod.bind(this);
    this.finalStep = this.finalStep.bind(this);
    this.makePayment = this.makePayment.bind(this);
  }

  getCheckoutInfo() {
    const { cart } = this.props;
    const { checkoutInfo } = this.state;
    if (!cart) return;

    const cartReformatted = {...cart, ...checkoutInfo}
    const items = {};
    const stateObj = {}
    cart.items.forEach(item => {
      if (items[item.sellerName]) {
        items[item.sellerName].push(item)
      } else {
        items[item.sellerName] = [item]
      }

      // not the best approach but works for now
      if (item.selfDelivery) stateObj[`selfDeliver_${item.id}`] = true;
    })
    cartReformatted.items = items;
    this.setState({
      ...stateObj,
      checkoutInfo: cartReformatted, step: 2, 
      changeAddress: false, isLoading: false 
    })
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.user) {
      const obj = {}

      if (!prevState.selectedAddress) {
        const defaultAddress = nextProps.user.addresses.find(a => a.isDefault);
        const selectedAddress = defaultAddress ? defaultAddress.id : '';
        obj.selectedAddress = selectedAddress;
      }
      if (!nextProps.user.addresses || !nextProps.user.addresses.length) obj.changeAddress = true;

      return obj;
    }

		return null
	}

  toggleChangeAddress() {
		this.setState({ 
      changeAddress: !this.state.changeAddress,
      step: 1
    })
  }

  toggleChangeMethod() {
		this.setState({ step: 3})
  }

  showAddressModal() {
		this.setState({ addNewAddress: true })
  }

  onModalClose() {
		this.setState({ addNewAddress: false })
  }

  handleChange(event, extraStateData = {}) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
    const state = { ...this.state, [name]: value, ...extraStateData }
    console.log(state);

    if (name.indexOf('selfDeliver_') > -1) {
      // radio button to self Deliver product. whenever it's clicked, set product courier cost to null
      const product = name.split('_').pop();
      state[`courier_${product}`] = '';
      state[`cost_${product}`] = '';
    }

		this.setState(state);
  }

  handleCourierSelect(event) {
    const { name, value } = event.target;
    const { cart } = this.props;
    const product = name.split('_').pop();
    const stateData = { isLoading: !!value ? true : this.state.isLoading }
    const item = cart.items.find(i => i.id === product);
    
    if (item && item.selfDelivery) stateData[`selfDeliver_${item.id}`] = value ? false : true // reset selfDeliver to false if courier selected
    stateData[`cost_${product}`] = value ? this.state[`cost_${product}`] : ''; // reset cost value if nothing selected
      
    this.handleChange(event, stateData);

    if (!value) return;

    userApi.getItemDeliveryCost({
      product,
      location: this.state.selectedAddress,
      courier: value
    })
    .then(costs => {
      
      this.setState({ 
        isLoading: false, [`costs_${product}`]: costs,
        [`cost_${product}`]: !costs ? 0 : undefined, // sometimes cost api returns null
        [`costs_${product}_null`]: !costs ? true : undefined
      })
    })
    .catch(error => this.setState({ error, isLoading: false }))
  }

  handleCostSelect(event) {
    const productId = event.target.name.split('_').pop()
    const { checkoutInfo } = this.state;
    const { cart } = this.props;
    let shippingCost = 0;

    const itemsWithCourier = cart.items.filter(i => i.couriers && i.couriers.length);
    itemsWithCourier.forEach(item => {
      if (this.state[`costs_${item.id}`]) {
        const service = item.id === productId ? event.target.value : this.state[`cost_${item.id}`]
        console.log('service', service);
        if (service) {
          const value = this.state[`costs_${item.id}`]
            .find(i => i.service === service).cost[0].value;
          if (!isNaN(value)) shippingCost += value
        }
      }
    })

    const stateData = { checkoutInfo: {...checkoutInfo, shippingCost, totalPrice: cart.totalPrice + shippingCost } }
    this.handleChange(event, stateData);
  }
  
  useAddress() {
    this.getCheckoutInfo();
  }

  finalStep() {
    this.setState({ step: 3 })
  }

  choosePaymentMethod(payment) {
    console.log('got payment');
    this.setState({ payment, step: 4 })
  }

  getDeliveryOptions() {
    const { cart } = this.props;
    const options = {}
    cart.items.forEach(item => {
      if (!item.couriers || !item.couriers.length) options[item.id] = { courier: 'self' }
      else {
        options[item.id] = {
          courier: item.selfDelivery && this.state[`selfDeliver_${item.id}`] ?
            'self' : this.state[`courier_${item.id}`],
          service: this.state[`cost_${item.id}`]
        }
      }
    })
    return options;
  }

  makePayment() {
    console.log('make payment', this.state)
    const { selectedAddress, payment }= this.state;
    this.setState({isLoading: true});
    console.log({ 
      ...payment,
      location: selectedAddress,
      deliveryOptions: this.getDeliveryOptions()

    })
    userApi.checkout({ 
      ...payment,
      location: selectedAddress,
      deliveryOptions: this.getDeliveryOptions()
    })
    .then(data => {
      console.log(data)
      this.setState({isLoading: false})
      this.props.history.push('/customer/history');
      this.props.clearCart();
    })
    .catch(error => {
      this.setState({error, isLoading: false})
    })
  }

  canProceedToPayment() {
    const { cart } = this.props;
    if (!cart) return false;

    const itemsWithCourier = cart.items.filter(i => i.couriers && i.couriers.length);

    return itemsWithCourier.every(i => !!this.state[`cost_${i.id}`] || this.state[`cost_${i.id}`] === 0 || this.state[`selfDeliver_${i.id}`])
  }
  
  render() {
    console.log('state', this.state)
    const { user, cart } = this.props;
    const { selectedAddress, error, isLoading, checkoutInfo } = this.state;
    
    if (!user || !cart) return <Spinner />;
    
    const address = user.addresses.find(a => a.id === selectedAddress);

    return (
      <DocumentTitle title={pageTitleText('Checkout')}>
        <React.Fragment>
          {error && !isLoading && <Alert options={{ autoClose: false, type: 'error'}}><ListErrors {...error} /></Alert>}
          {isLoading && <Alert options={{ autoClose: false, type: 'default'}} 
            className='loader'><Spinner /></Alert> }
          <Navbar color="faded" light style={{ backgroundColor: '#fff' }} className="sticky-top">
            <Link to="/" className="mr-auto ml-md-4 navbar-brand">
            <img src={logo} style={{ maxWidth: 75 }} />
            </Link>
          </Navbar>
          <div className="container mt-5">
            {!cart.items.length ?
              <div className="text-center">
                <p className="text-danger font-weight-bold" style={{ fontSize: '2rem' }}>
                  Cart is empty
                </p>
                <p class="font-weight-bold link__display" style={{ fontSize: '1rem' }}>
                  <Link to="/">Go Home</Link>
                </p>
              </div>:
              <div className="row">
                <div className="col-12 col-md-8">
                  <div className="card">
                    <p className="card-header">
                      <span className="font-weight-bold">Delivery Address</span>
                      {selectedAddress && 
                        <span className="underline pointer float-right"
                          onClick={this.toggleChangeAddress}>Change Address</span>
                      }
                    </p>
                    <div className='card-body'>
                      <div className="card p-2 border-0 mb-0">
                        {address ? 
                          <React.Fragment>
                            <span className="font-weight-bold">{address.firstName} {address.lastName}</span>
                            <span>{address.address}</span>
                            <span>{address.provinceName}</span>
                            <span>{address.cityName}</span>
                          </React.Fragment> : 
                          user.addresses.length > 0 ? <p>Select an address</p> : null
                        }
                      </div>
                      <div className={classnames('form-group', {
                          'd-none': !this.state.changeAddress
                        })}>
                        {user.addresses.length ?
                          <select name="selectedAddress" id="" onChange={this.handleChange}
                            value={this.state.selectedAddress}>
                            {user.addresses.map(a => <option value={a.id} key={a.id}>{a.address}</option>)}
                          </select> :
                          <p className="text-center font-weight-bold m-1">You haven't added any address</p>
                        }
                        <span className="float-left text-mute underline pointer"
                          onClick={this.showAddressModal}>Add new address</span>
                        {user.addresses.length > 0 &&
                          <span className="float-right text-mute underline pointer"
                            onClick={this.toggleChangeAddress}>Hide</span>
                        }
                        <AddressFormModal isOpen={this.state.addNewAddress} onModalClose={this.onModalClose} />
                      </div>
                    </div>
                    <div className={classnames('card-footer p-1 text-right', {
                      'd-none': this.state.step > 1
                    })}>
                      {selectedAddress &&
                        <button className="btn btn-secondary text-uppercase btn-block"
                          onClick={this.useAddress}>Use this adresss</button>
                      }
                    </div>
                  </div>

                  <div className="card">
                    <p className="card-header font-weight-bold">Order Details</p>
                    <div className={classnames('card-body', {
                      'd-none': this.state.step < 2
                      })}>
                      {checkoutInfo && Object.keys(checkoutInfo.items).map(seller => (
                        <div className="row justify-content-around" key={seller}>
                          <div className="col-12"><h5>{seller}</h5></div>
                          <div className="col-12">
                            {checkoutInfo.items[seller].map(item => (
                              <React.Fragment key={item.id}>
                                <div className="row pl-4 pb-1 pt-1">
                                  <div className={classnames('col-12', {
                                      'col-md-8': !!this.state[`costs_${item.id}`],
                                      'col-md-9': !this.state[`costs_${item.id}`]
                                    })}>
                                    <div className="row">
                                      <div className="col-3 p-0 pb-1">
                                        <img src={item.image} alt={item.name}
                                          style={{ maxWidth: '100%' }}
                                          className="thumbnail float-left mb-1 mr-2"
                                        />
                                      </div>
                                      <div className="col-9">
                                        <p className="mb-1 font-weight-bold">
                                          <Link to={'/i/' + item.url} target="_blank">{item.name}</Link>
                                        </p>
                                        <p className="mb-1"><b>quantity: </b>{item.quantity}</p>
                                        <p className="mb-1 text-warm font-weight-bold"> {formatAmount(item.price)}</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div 
                                    className={classnames('col-12 pl-0', {
                                        'col-md-4': !!this.state[`costs_${item.id}`],
                                        'col-md-3': !this.state[`costs_${item.id}`]
                                      })}>
                                    {item.selfDelivery && 
                                      <div>
                                        <input 
                                          type="radio" name={`selfDeliver_${item.id}`}
                                          value={true}
                                          onChange={this.handleChange}
                                          checked={!!this.state[`selfDeliver_${item.id}`]}
                                        />
                                        <label>Let seller ship</label>
                                      </div>
                                    }
                                    {item.selfDelivery && item.couriers && item.couriers.length > 0 &&
                                      <span>---------<b>OR</b>--------</span> 
                                    }
                                    {item.couriers && item.couriers.length > 0 && 
                                      <React.Fragment>
                                        <select value={this.state[`courier_${item.id}`]}
                                          name={`courier_${item.id}`} id="" onChange={this.handleCourierSelect}>
                                          <option value="">Choose Courier</option>
                                          {item.couriers.map(c => <option value={c.courierId} key={c.courierId}>{c.name}</option>)}
                                        </select>
                                      </React.Fragment>
                                    }
                                    { this.state[`costs_${item.id}`] &&
                                      <div>
                                        {this.state[`costs_${item.id}`].map((cost, i) =>
                                          <React.Fragment key={i}>
                                            <input id={`cost_${item.id}_${i}_id`}
                                              name={`cost_${item.id}`}
                                              type="radio" className="mb-1"
                                              value={cost.service || '-'}
                                              checked={this.state[`cost_${item.id}`] === cost.service}
                                              onChange={this.handleCostSelect}
                                            />
                                            <label htmlFor={`cost_${item.id}_${i}_id`}
                                              style={{ fontSize: '.8rem' }}
                                              className="d-inline">
                                              {cost.description}
                                            </label>
                                            {cost.cost[0] &&
                                              <p className="m-0 pl-2" style={{ fontSize: '.7rem' }}>
                                                Delivery within {cost.cost[0].etd.split(' ')[0]} days - 
                                                &nbsp;<span className="font-weight-bold">{formatAmount(cost.cost[0].value)}</span>
                                              </p>
                                            }
                                          </React.Fragment>
                                        )}
                                      </div>
                                    }
                                    { this.state[`costs_${item.id}_null`] && <p className="text-center font-weight-bold m-1">no shipping fee</p>}
                                  </div>
                                </div>
                                <hr />
                              </React.Fragment>
                            ))}
                          </div>
                        </div>
                      ))}
                    </div>
                    <div className={classnames('card-footer text-right p-1', {
                      'd-none': this.state.step !== 2
                      })}>
                      <button className="btn btn-secondary text-uppercase btn-block"
                        onClick={this.finalStep} disabled={this.isLoading || !this.canProceedToPayment()}>
                        Proceed
                      </button>
                    </div>
                  </div>
                  
                  <Payment 
                    bodyClass={this.state.step < 3 ? 'd-none': ''}
                    footerClass={this.state.step !== 3 ? 'd-none': ''}
                    onSubmit={this.choosePaymentMethod}
                    amount={checkoutInfo ? checkoutInfo.totalPrice : cart.totalPrice}
                    showToggler={this.state.step > 3}
                    toggleChangeMethod={this.toggleChangeMethod}
                    disabled={isLoading || this.state.step !== 3}
                  />
                </div>
                <div className={classnames("col-12 col-md-4", {
                    'd-flex': this.state.step === 4
                  })} id="order-summary">
                  <div className="card">
                    <p className="card-header font-weight-bold">Order Summary</p>
                    <div className="card-body">
                      <div className="p-1">
                        <span>Subtotal</span> <span className="float-right font-weight-bold">{formatAmount(cart.totalPrice)}</span>
                      </div>
                      <div className="p-1">
                        <span>Shipping</span> <span className="float-right font-weight-bold">
                          {checkoutInfo && checkoutInfo.shippingCost ? formatAmount(checkoutInfo.shippingCost) : '-'}
                        </span>
                      </div>
                      {/* <div className="p-1">
                        <span>Subtotal</span> <span className="float-right font-weight-bold">Rp 900</span>
                      </div> */}
                      <hr />
                      <div className="font-weight-bold">
                        <span>Total</span>
                        <span className="float-right">
                          {formatAmount(checkoutInfo ? checkoutInfo.totalPrice : cart.totalPrice)}
                        </span>
                      </div>
                    </div>
                    { this.state.step === 4 && 
                      <div className="card-footer p-1">
                        <button 
                          onClick={this.makePayment}
                          className="btn btn-secondary text-uppercase btn-block mt-2" 
                          disabled={isLoading}>Place Order
                        </button>
                      </div>
                    }
                  </div>
                </div>
              </div>
            }
          </div>
        </React.Fragment>
      </DocumentTitle>
    )
  }
}

const mapStateToProps = (state) => ({
	user: state.shared.currentUser,
	cart: state.cart.cart,
	couriers: state.location.couriers
})

export default connect(mapStateToProps, { clearCart: cartActions.clearCart })(Checkout)