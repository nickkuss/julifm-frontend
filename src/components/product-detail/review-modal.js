import React, { Component } from 'react';
import { connect } from 'react-redux';
import Modal  from 'reactstrap/lib/Modal';
import productActions from '../../actions/product';
import ReviewForm from './review-form';
import Alert from '../alert';
import ListErrors from '../list-error';

class ReviewFormModal extends Component {
	constructor(props) {
    super(props);
    this.state = {address:  {isDefault: true}};
		this.close = this.close.bind(this);
		this.onClosed = this.onClosed.bind(this);
  }

  close() {
    this.setState({ isOpen: false, manualClose: true});
	}

	static getDerivedStateFromProps(nextProps, state) {
		const obj = {};

		if (!nextProps.isOpen) {
			obj.isOpen = false;
			obj.manualClose = false;
		} else obj.isOpen = !state.manualClose;
		
		return obj;
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevProps.reviews !== this.props.reviews) {
			this.close()
			// this.setState({isOpen: false})
		}

	}

	onClosed() {
		this.props.onModalClose()
	}
	
	
	render() {
		const  { isLoading, error } = this.props;
		return (
		<Modal isOpen={this.state.isOpen}  onClosed={this.onClosed}
			fade={false} toggle={this.close} className={this.props.className}>
			<div className="modal-header">
				<h5 className="modal-title">Add Review</h5>
				<button type="button" className="close" 
					onClick={this.close}
					aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div className="modal-body">
				{error && !isLoading && <Alert options={{ autoClose: false, type: 'error' }}><ListErrors {...error} /></Alert>}
				<ReviewForm />
			</div>
		</Modal>
		)
	}
}

const mapStateToProps = ({ product }) => ({
	isLoading: product.fetchingReviews,
  error: product.error,
})

export default connect(mapStateToProps)(ReviewFormModal);
