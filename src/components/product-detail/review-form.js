import React, { Component } from 'react';
import { connect } from 'react-redux';
import StarRatings from 'react-star-ratings';
import classnames from 'classnames';
import productActions from '../../actions/product';
import config from '../../config';

class ReviewForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			review: { },
		}

		this.handleChange = this.handleChange.bind(this);
		this.handleRatingChange = this.handleRatingChange.bind(this);
		this.submit = this.submit.bind(this);
	}

	submit(event) {
		event.preventDefault();
		event.stopPropagation(); // prevent parent form from submitting
		
		// const formValid = this.isValid();

    this.props.submit(this.props.productId, this.state.review)
	}

	handleChange(event) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		const newState = { review: {...this.state.review, [name]: value } }
		this.setState(newState);
  }
  
  handleRatingChange( rating, name ) {
    this.setState({ review: {...this.state.review, rating } });
  }

	render() {
    console.log(this.state)
		const { review } = this.state;
		const { isLoading } = this.props;
		return (
			<React.Fragment>
				<form noValidate onSubmit={this.submit}>
					<div className="row">
						<div className="col-12">
							<div className="form-group">
								<label htmlFor="rating" className="control-label font-weight-bold">Rating*</label>
                <div className="form-control">
                  <StarRatings
                    rating={this.state.review.rating || 0}
                    numberOfStars={config.maxNumOfStars}
                    starSpacing={'0px'}
                    name='rating'
                    className="form-control"
                    starDimension="1rem"
                    changeRating={this.handleRatingChange}
                    starRatedColor={config.starRatingColor}
                  />
                </div>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							<div className="form-group">
								<label htmlFor="comment" className="control-label font-weight-bold">Review*</label>
								<textarea placeholder="comment" style={{ minHeight: 50 }}
									value={review.comment || ''} id="comment" name="comment" onChange={this.handleChange} />
							</div>
						</div>
					</div>
					<div className="form-group">
						<input type="checkbox" id="isAnonymous" name="isAnonymous"
							value={review.isAnonymous} onChange={this.handleChange} />
						<label htmlFor="isAnonymous">Submit anonymously</label>
					</div>
					<button
						className={classnames('btn btn-secondary btn-block rounded-0', {
							'loading': isLoading
						})} disabled={isLoading}>Submit Review</button>
					<div className="clearfix"></div>
				</form>
			</React.Fragment>
		)
	}
}


export default connect(({ product }) => ({
	isLoading: product.fetchingReviews,
  error: product.error,
  productId: product.product ?  product.product.id : null,
	reviews: product.reviews,
}), (dispatch) => ({
	submit: (id, data) => dispatch(productActions.addReview(id, data))
}))(ReviewForm);
