import React from 'react';
import StarRatings from 'react-star-ratings';
import config from '../../config';

const ProductRating = ({ rating }) => (
  <StarRatings
    rating={rating || 0}
    isSelectable={false}
    numberOfStars={config.maxNumOfStars}
    starSpacing={'0px'}
    name='productRating'
    starDimension="1rem"
    starRatedColor={config.starRatingColor}
  />
)

export default ProductRating