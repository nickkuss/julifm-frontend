import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'
import Link from 'react-router-dom/Link';
import UncontrolledCarousel from 'reactstrap/lib/UncontrolledCarousel';
import UncontrolledTooltip from 'reactstrap/lib/UncontrolledTooltip';
import DocumentTitle from 'react-document-title';
import classnames from 'classnames';
import Pusher from 'pusher-js';
import ProductExtra from './product-extra';
import ProfilePreview from '../profile/profile-preview';
import config from '../../config';
import productActions from '../../actions/product';
import cartActions from '../../actions/cart';
import ListErrors from '../list-error';
import Alert from '../alert';
import Spinner from '../spinner';
import ChatModal from '../chat/chat-modal';
import { getProductQtyInCart } from '../../utils/product'
import AuctionTimer from '../auction-timer';
import { formatAmount } from '../../utils/app';
import ProductRating from './rating';
import './style.css';

const Fragment = React.Fragment;

const mapStateToProps = (state) => ({
	product: state.product.product,
	isLoading: state.product.isLoading,
	error: state.product.error,
	cart: state.cart.cart,
	cartError: state.cart.error,
	isCartLoading: state.cart.isLoading,
	bookmarkError: state.cart.bookmarkError,
	user: state.shared.currentUser,
	isBookmarkLoading: state.product.bookmarkLoading,
	isShareLoading: state.product.shareLoading,
})

const mapDispatchToProps = (dispatch) => ({
	getProductDetail: (slug) => dispatch(productActions.getProductDetail(slug)),
	submitBid: (id, amount) => dispatch(productActions.submitBid(id, amount)),
	onNewBid: (data) => dispatch(productActions.updateWithNewBid(data)),
	onUnload: () => dispatch(productActions.onUnload()),
	updateCart: (data) => dispatch(cartActions.updateCart(data)),
	shareProduct: (id) => dispatch(productActions.shareProduct(id)),
	addBookmark: (id) => dispatch(productActions.addBookmark(id)),
	removeBookmark: (id) => dispatch(productActions.removeBookmark(id))
})

class ProductDetail extends Component {
	constructor(props) {
		super(props);
		this.state = {
			bidAmount: props.product && props.product.auction ? props.product.auction.minBid : '',
			quantity: 1,
			auctionTimerExpired: false, // to refresh page if auction timer completes
		}
		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleProductAction = this.handleProductAction.bind(this);
		this.updateCart = this.updateCart.bind(this);
		this.shareProduct = this.shareProduct.bind(this);
		this.toggleBookmark = this.toggleBookmark.bind(this);
		this.onChatSelect = this.onChatSelect.bind(this);
		this.onModalClose = this.onModalClose.bind(this);
		this.onAuctionTimerEnd = this.onAuctionTimerEnd.bind(this);
	}

	static getDerivedStateFromProps(nextProps, state) {
		let obj = {isCartLoading: nextProps.isCartLoading, formDirty: false};

		if (nextProps.product && nextProps.product.auction) {
			if (nextProps.product.auction.minBid > state.bidAmount && !state.formDirty) {
				obj = {...obj, bidAmount: nextProps.product.auction.minBid};
			}
		}

		if (!state.cartUpdated && state.isCartLoading && !nextProps.isCartLoading && !nextProps.cartError) {
			obj = {...obj, cartUpdated: true };
		}

		if (nextProps.isLoading || nextProps.isCartLoading || nextProps.isBookmarkLoading || nextProps.isShareLoading) {
			obj = {...obj, hasUpdate: true}; // so we can show new errors
		}

		if (state.cartUpdated) {
			obj = {...obj, cartUpdated: false };
		}

		if (nextProps.product && nextProps.cart && (!state.isMounted || !state.formDirty)) {
			const { product, cart } = nextProps;
			const cartQty = getProductQtyInCart(cart.items, product);
			obj = {...obj, cartQty, quantity: cartQty || state.quantity, isMounted: true };
		}
	
		return obj;
	}

	componentDidMount() {
		const { product, match: { params: { slug } } } = this.props;
		if (!product && !this.props.isLoading) this.props.getProductDetail(slug)
		else if (product && product.slug !== slug) this.props.getProductDetail(slug) // coming from product form

		// initialize pusher
		this.pusher = new Pusher(config.pusherKey, {
			cluster: config.pusherCluster,
			encrypted: true
		});
		
		this.channel = this.pusher.subscribe(config.pusherBidChannel);
		this.channel.bind(config.pusherBidEvent, (data) => {
			const product = product;
			console.log(data);
			if (product && data.product === product.id) {
				this.props.onNewBid(data);
			}
		});
	}

	componentDidUpdate(prevProps) {
		const { isLoading, error, match: { params: { slug } } } = this.props;

		// refetch data if url changes to another product 
		if (!isLoading && !error && (!prevProps.product || prevProps.product.slug !== slug) ) {
			this.props.getProductDetail(slug)
		}
	}

	shareProduct(event) {
		const { product, user } = this.props;

		if (!user) return this.redirectToLogin();

		!product.isShared ? this.props.shareProduct(product.id) : null
	}

	toggleBookmark(event) {
		const { product, user } = this.props;

		if (!user) return this.redirectToLogin();

		product.isBookmarked ? 
			this.props.removeBookmark(product.id) :
			this.props.addBookmark(product.id)
	}

	onChatSelect() {
		if (!this.props.user) return this.redirectToLogin();

    this.setState({ chatOpen: true })
  }

	onModalClose() {
		this.setState({ chatOpen: false })
  }

	handleInputChange(event) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		this.setState({ [name]: value, 
			hasUpdate: false,  // hasUpdate is to hide errors
			formDirty: true // to make quantity not get from cart but use this value
		});
	}

	handleProductAction() {
		if (!this.props.user) return this.redirectToLogin();

		return this.props.submitBid(this.props.product.id, String(this.state.bidAmount))
	}

	updateCart() {
		if (!this.props.user) return this.redirectToLogin();

		this.props.updateCart({product: this.props.product.id, quantity: this.state.quantity})
	}

	redirectToLogin() {
		if (this.props.history) {
			this.props.history.push({
				pathname: '/login',
				state: { referrer: window.location.pathname }
			})
		}
	}

	componentWillUnmount() {
		this.pusher.unsubscribe(config.pusherBidChannel);
		this.channel.unbind(config.pusherBidEvent);
		this.props.onUnload()
	}

	onAuctionTimerEnd() {
		this.forceUpdate()

		if (!this.state.auctionTimerExpired) this.setState({ auctionTimerExpired: true });
	}

	render() {
		const { product, isLoading, isCartLoading, isBookmarkLoading, isShareLoading, user } = this.props;
		const { cartUpdated, cartQty, hasUpdate, auctionTimerExpired } = this.state;
		const error = hasUpdate && (this.props.error || this.props.cartError || this.props.bookmarkError);
		
		if (!product) return <p className="text-center mt-3">loading...</p>;
		if (!product) return null

		const quantities = [];
		for (let i = 1; i <= 10; i++) {
			quantities.push(<option key={i} value={i}>{i}</option>)
		}

		const loading = isLoading || isCartLoading || isBookmarkLoading || isShareLoading;
		let auctionEnded = false;
		if (product.auction) {
			auctionEnded = product.auction.isEnded || (new Date().toISOString() >= product.auction.auctionEnd)
		}

		return(
			<DocumentTitle title={`Juli | ${product.name}`}>
				<Fragment>
					<ChatModal isOpen={this.state.chatOpen} onModalClose={this.onModalClose}/>				
					<div className="row pt-2 detail-view">
						{( loading || (error && !isLoading) || (!error && !isCartLoading && cartUpdated) ) && 
							<Alert options={{ autoClose: error ? false : 1000,
								type: cartUpdated ? 'success' : loading ? 'default' : 'error',
							}} className={loading ? 'loader' : ''}>
								{loading ? <Spinner /> :
									cartUpdated ? 'Cart updated' : <ListErrors {...error} />
								}
							</Alert> 
						}
						{auctionTimerExpired && 
							<Alert options={{ autoClose: false }}>
								<p class="font-weight-bold">Auction on this product has ended</p>
							</Alert> 
						}
						{ user && user.id === product.owner.id ?
							<Link class="col-12 col-sm-6 text-center m-auto ion-edit btdn btn-secondary text-dark product-edit-btn"
								to={'/customer/product/edit/' + product.slug}></Link> :
							<div className="col-12 d-md-none">
								<ProfilePreview {...product.owner} onChatSelect={this.onChatSelect}/>
							</div>
						}
						<div className="col-lg-8">
							<div className="box mb-2">
								<UncontrolledCarousel items={product.images.map(i => ({src: i.url}))} />
							</div>
							<div className="action-tab flex-column flex-lg-row">
								{/* {product.saleFormat !== "auction" && 
									<div className={classnames(" btn-primary action rounded-0 mr-md-2 d-inline-flex", {
										'has-auction': product.saleFormat === 'auction'
									})}>
										<select name="quantity" id="quantity" 
											onChange={this.handleInputChange}
											value={this.state.quantity}>
											{ quantities }
										</select>
										<button className={classnames("btn btn-primary text-uppercase rounded-0 w-100", {
											loading: this.props.cart && this.props.isCartLoading
										})} disabled={this.props.cart && this.props.isCartLoading}
											onClick={this.updateCart}>	
											ADD TO CART
										</button>
									</div>
								} */}
								{/* <div className={classnames("btn-info text-uppercase action rounded-0 d-inline-flex", {
										'has-auction': product.saleFormat === 'auction'
									})}>
									<button className={classnames("btn btn-info rounded-0 w-100", {
										'loading': false
									})}>
										BOOKMARK
									</button>
									
								</div> */}
							</div>
						</div>
						<div className="col-12 col-lg-4 p-lg-0">
							<div className="box product-info p-3">
								{!auctionEnded && (!user || user.id !== product.owner.id) && 
									<Fragment>
										<i className={classnames('ion-bookmark float-right p-2 pointer', {
												'text-muted': !product.isBookmarked
											})} id="bookmark" style={{fontSize: '2rem'}}
											onClick={this.toggleBookmark}>
										</i>
										<i className={classnames('ion-android-share float-right p-1 pointer', {
												'text-muted': !product.isShared
											})} id="share" style={{fontSize: '2rem'}}
											onClick={this.shareProduct}>
										</i>
										<UncontrolledTooltip placement="top" target="bookmark">
											{product.isBookmarked ? 'remove bookmark' : 'save for later'}
										</UncontrolledTooltip>
										<UncontrolledTooltip placement="right" target="share">
											{product.isShared ? 'product has been shared' : 'share product to followers'}
										</UncontrolledTooltip>
									</Fragment>
								}
								<p className="p-name mb-2">{product.name}</p>
								{product.saleFormat !== "auction" && 
									<p className="p-info font-weight-bold">{formatAmount(product.salePrice)}
										{/* <span className="text-danger ml-2">
											(-{Math.round(product.salePrice / product.originalPrice * 100)}%)
										</span> */}
									</p>
								}
								{ product.auction && 
									<React.Fragment>
										{product.auction.targetPrice && 
											<p className="p-info"><span className="font-weight-bold">
												Asking Price: </span>{formatAmount(product.auction.targetPrice)}
											</p>
										}
										<p className="p-info capitalize">
											<span className="font-weight-bold">
												Current Bid: </span>Rp {product.auction.currentAmount} &nbsp;
											<span>| {product.auction.bidCount} Bids</span>
										</p>
										<p className="p-info capitalize">
											<span className="font-weight-bold">Time Left: </span>
											<AuctionTimer auction={product.auction} showRealDate={true} onComplete={this.onAuctionTimerEnd}/>
										</p>
									</React.Fragment>
								}

								<p className="p-info capitalize"><span className="font-weight-bold">Condition: </span>{product.condition}</p>
								{product.meta && 
									<div className="p-info">
										<ProductRating rating={product.meta.averageRating}/>

										{product.meta.ratingCount > 0 &&
											<span>
												&nbsp; | &nbsp; {product.meta.ratingCount || 0} ratings
												{/* {product.meta.reviewCount > 0 && 
													<a className="float-right underline rev-link pointer">View reviews ({product.meta.reviewCount})</a>
												} */}
											</span>
										}
									</div>
								}
								{(!user || user.id !== product.owner.id) && 
									<Fragment>
										{!!user && product.auction && !auctionEnded && 
											<p className="font-weight-bold mb-0 text-danger" style={{fontSize: .6 + 'rem'}}>
												minimum bid: Rp {product.auction.minBid}
											</p>
										}
										{product.auction && product.auction.isWinner && 
											<p className="text-success text-uppercase font-weight-bold">
												You won this auction
											</p>
										}
										<div className={classnames("action rounded-0 mr-md-2 d-inline-flex w-100", {
											'has-auction': product.saleFormat === 'auction',
											'btn-secondary': !cartQty && (!product.auction || product.auction.isWinner),
											'btn-success': cartQty,
											'action-flex-wrap': product.auction && product.auction.escrowAmount
											})}>
											{!product.auction || product.auction.isWinner ?
												<React.Fragment>
													{!product.auction &&
														<React.Fragment>
															<select name="quantity" id="quantity" 
																style={{ color: 'initial' }}
																onChange={this.handleInputChange}
																value={this.state.quantity}>
																{ quantities }
															</select>
														</React.Fragment>
													}

													{!!cartQty && product.auction && 
														<Link to="/cart"
															className="btn text-uppercase rounded-0 w-100 btn-success">
															Go to Cart
														</Link>
													}

													{(!cartQty || !product.auction) &&
														<button className={classnames("btn text-uppercase rounded-0 w-100", {
															loading: this.props.cart && this.props.isCartLoading,
															'btn-secondary': !cartQty,
															'btn-success': cartQty
														})} disabled={this.props.cart && this.props.isCartLoading}
															onClick={this.updateCart}>	
															{cartQty ? 'UPDATE CART' : 'ADD TO CART'}
														</button>
													}
												</React.Fragment> :
												<React.Fragment>
													{!auctionEnded &&
														<React.Fragment>
															<input type="number" className="mb-0" name="bidAmount"
																value={this.state.bidAmount}
																onChange={this.handleInputChange}
																min={product.auction.minBid}
																step={config.minBidIncrement}
																/>
															<button className={classnames('btn btn-success text-uppercase rounded-0 w-100', {
																loading: isLoading
																})} onClick={this.handleProductAction}
																disabled={this.state.bidAmount < product.auction.minBid}
																>
																PLACE BID
															</button>
															{product.auction && product.auction.escrowAmount && !product.hasBid && user && 
																<React.Fragment>
																	<p className="font-weight-bold a-wrap mb-0 mt-1">
																		<i className="ion-information-circled mr-1 text-primary" id="hasEscrow"></i>
																		<span>requires one-time deposit of {formatAmount(product.auction.escrowAmount)}</span>
																	</p>
																	<UncontrolledTooltip placement="top" target="hasEscrow">
																		On first bid on this product, {formatAmount(product.auction.escrowAmount)} will be deducted from your wallet
																		and added to escrow. This will be refunded when auction ends
																	</UncontrolledTooltip>
																</React.Fragment>
															}
														</React.Fragment>
													}
												</React.Fragment>
											}
										</div>
									</Fragment>
								}
							</div>
							<div className="d-nohne d-lg-block">
								<hr/>
								<ProfilePreview {...product.owner} onChatSelect={this.onChatSelect}/>
							</div>
						</div>
					</div>
					<div className="row detail-view mt-2">
						<div className="col-12 col-lg-8">
							<ProductExtra product={product}/>
						</div>
						{/* <div className="d-lg-none col-12">
							<hr/>
							<ProfilePreview {...product.owner}/>
						</div> */}
					</div>
				</Fragment>
			</DocumentTitle>
		)
	}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProductDetail));