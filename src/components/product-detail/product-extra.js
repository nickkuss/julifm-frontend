import React, { Component } from 'react';
import { connect } from 'react-redux';
import TabContent from 'reactstrap/lib/TabContent';
import TabPane from 'reactstrap/lib/TabPane';
import classnames from 'classnames';
import DOMPurify from 'dompurify'
import Spinner from '../spinner';
import ProductRating from './rating';
import { formatDateFromNow } from '../../utils/product';
import api from '../../api';
import ReviewModal from './review-modal';
import productActions from '../../actions/product';

const Fragment = React.Fragment;

class ProductExtra extends Component {

	constructor(props) {
		super(props);

		this.state = {activeTab: 1, reviews: null, reviewModalOpen: false }

		this.setTab1 = this.setTab1.bind(this);
		this.setTab2 = this.setTab2.bind(this);
		this.setTab3 = this.setTab3.bind(this);
		this.showReviewModal = this.showReviewModal.bind(this);
		this.onModalClose = this.onModalClose.bind(this);
	}

	onModalClose() {
		this.setState({ reviewModalOpen: false })
	}

	showReviewModal() {
		this.setState({ reviewModalOpen: true })
	}

	setTab1() {
		this.setState({activeTab: 1})
	}

	setTab2() {
		this.setState({activeTab: 2})
	}

	setTab3() {
		const { reviews, product } = this.props;
		if (!reviews) this.props.getReviews(product.id);
		this.setState({activeTab: 3})
	}

	generateTabs() {
		return (
			<div className="nav-tabs">
				<div className="nav-item">
					<span onClick={this.setTab1}
						className={classnames('nav-link text-uppercase', { active: this.state.activeTab === 1 })} >
						<i className="ion-information mr-2"></i>
						Description
					</span>
				</div>

				<div className="nav-item">
					<span onClick={this.setTab2}
						className={classnames('nav-link text-uppercase', { active: this.state.activeTab === 2 })} >
						<i className="ion-android-bicycle mr-2"></i>
						Shipping
					</span>
				</div>

				<div className="nav-item">
					<span onClick={this.setTab3}
						className={classnames('nav-link text-uppercase', { active: this.state.activeTab === 3 })} >
						<i className="ion-ios-star-half mr-2"></i>
						Reviews
					</span>
				</div>
			</div>
		);
	}

	renderReviews() {
		const { reviews } = this.props;
		if (!reviews) return <Spinner topDistance={20} imgSize={40}/>

		return <div className="review-box text-center">
			{reviews.canReview && 
				<div>
					<span className="btn btn-secondary btn-sm" onClick={this.showReviewModal}>Leave Review</span>
					<ReviewModal isOpen={this.state.reviewModalOpen} onModalClose={this.onModalClose}/>
				</div>
			}
			{!reviews.items.length && <p className="text-center"> No review at the moment</p>}
			{reviews.items.map(x =>
				<div className="mb-1 review-item text-left" key={x.id}>
					<span className="font-weight-bold mr-2">{x.user.username || 'User'}</span>
					<div className="mb-1">
						<span className="float-left"><ProductRating rating={x.rating}/></span>
						<span className="float-right timestamp mr-1">{formatDateFromNow(x.createdAt)}</span>
						<div className="clearfix"></div>
						<p className="mb-1">{x.comment}</p>
					</div>
				</div>
			)}
		</div>
	}

	render() {
		const product = this.props.product;
		if (!product) return null;

		const purified = DOMPurify.sanitize(product.description);

		return (
			<Fragment>
				{this.generateTabs()}
				<TabContent activeTab={this.state.activeTab}>
					<TabPane tabId={1}>
						<div dangerouslySetInnerHTML={{__html: purified}}></div>
					</TabPane>
					<TabPane tabId={2}>
						{product.deliveryMethods.indexOf('self') > -1 && <p className="font-weight-bold">This product can be self delivered</p>}
						{product.couriers &&
							<React.Fragment>
								<h5>Available Couriers</h5>
								{product.couriers.map(c => <p key={c.courierId}>{c.name}</p>)}
							</React.Fragment>
						}
					</TabPane>
					<TabPane tabId={3}>
						{this.renderReviews()}
					</TabPane>
				</TabContent>
			</Fragment>
		)
	}
}

const mapStateToProps = ({ product }) => ({
	reviews: product.reviews,
})

const mapDispatchToProps = (dispatch) => ({
	getReviews: (id) => dispatch(productActions.getReviews(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ProductExtra)