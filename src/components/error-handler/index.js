import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import Alert from '../alert';
import ListErrors from '../list-error';


const errorWrapperHOC = (WrappedComponent) => {
  return class ErrorHoccedComp extends Component {
    render() {
      const { error } = this.props;
      console.log('got error', error);
      if (!error) {
        return <WrappedComponent {...this.props} />
      }
      return <React.Fragment>
        <Alert options={{autoClose: false, type: 'error'}}>
          <ListErrors {...error} />
        </Alert>
        <WrappedComponent {...this.props} />
      </React.Fragment>
    }
  }
}

export default compose(
  connect(state => ({
    error: state.shared.hasError
  })), errorWrapperHOC
)