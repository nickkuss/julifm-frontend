import React, { Component } from 'react';
import { connect } from 'react-redux';
import Link from 'react-router-dom/Link';
import UncontrolledCarousel from 'reactstrap/lib/UncontrolledCarousel';
import CarouselItem from 'reactstrap/lib/CarouselItem';
import homeActions from '../../actions/home';

import './style.css';
import Product from '../product';
import spinner from '../../shared/spinner.svg';

const Fragment = React.Fragment;

const mapStateToProps = (state) => ({ 
	...state.home,
	user: state.shared.currentUser,
	loadingUser: state.shared.loadingUser
});

const mapDispatchToProps = (dispatch) => ({
	getFeed: () => dispatch(homeActions.getFeed()),
	getFeatured: () => dispatch(homeActions.getFeatured()),
	getTrending: () => dispatch(homeActions.getTrending()),
});

class Home extends Component {
  constructor(props) {
		super(props)

		this.state = {};
	}

	componentDidMount() {
		const { 
			user, loadingUser,
			feedProducts, featuredProducts, trendingProducts,
			feedProductsLoading, featuredProductsLoading, trendingProductsLoading 
		} = this.props;

		if (user && !feedProducts && !feedProductsLoading) this.props.getFeed();
		if (!featuredProducts && !featuredProductsLoading) this.props.getFeatured();
		if (!trendingProducts && !trendingProductsLoading) this.props.getTrending();
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.user && !this.props.user && !this.props.feedProducts && !this.props.feedProductsLoading) this.props.getFeed();
	}

	render() {
		const { 
			user, loadingUser,
			feedProducts, featuredProducts, trendingProducts,
			feedProductsLoading, featuredProductsLoading, trendingProductsLoading 
		} = this.props;
		const classNames = "col-lg-3 col-6 col-md-4 col-sm-4 mb-3"
		const wrapperClass = "pl-0 pr-0 pt-1"

		const items = [
			{src: 'https://static.jumia.com.ng/api/8766819/images/35d52dd5637__NG_W41_S4_416a_20181021__1_-min.jpg' },
			{src: 'https://static.jumia.com.ng/api/8766819/images/10e7063456374__NG_W41_S5_4144_20181021_.jpg' },
			{src: 'https://static.jumia.com.ng/api/8766819/images/234359e225613__NG_W41_S3_414f_20181021.jpg' },
		]
		const slides = items.map(item => {
			return <CarouselItem
			onExiting={this.onExiting}
			onExited={this.onExited}
			key={item.src}
		>
			<img src={item.src} alt={'hello'} />
		</CarouselItem>
		})

		return (
			<Fragment>
				<div className="container-fluid pt-2 text-left">
					<div className="row banners mb-3">
						<div className="col-12 pl-0">
							<UncontrolledCarousel items={items} >{slides}</UncontrolledCarousel>
						</div>
					</div>
					<h5 className="group-header pl-1 d-inline-block">My Feed</h5>
					{ feedProducts && feedProducts.hasMore && 
						<span className="float-right link__display">
							<Link to="more">View More</Link>
							<i className="ion-chevron-right ml-2"></i>
						</span>
					}
					<div className="product-group row">
						{!user && !loadingUser ? 
							<Fragment>
								<p className="text-center pt-3 font-weight-bold">
									<Link className="link__display" to="/login">Login</Link> to view your feed
								</p>
							</Fragment> :
							<Fragment>
								{feedProductsLoading && <img src={spinner} alt="loading indicator text-center" id="spinner" /> }
								{ feedProducts &&
									<Fragment>
										{!feedProducts.items.length && <p className="text-center pt-2">No feed to show</p>} 
										{feedProducts.items.slice(0, 8).map(item => <Product className={classNames} key={item.slug} product={item} wrapperClass={wrapperClass}/>)}
									</Fragment>
								}
							</Fragment>
						}
					</div>
					<hr/>
					<h5 className="group-header pl-1 d-inline-block">Deals of the Week</h5>
					{featuredProducts && featuredProducts.hasMore && 
						<span className="float-right link__display">
							<Link to="more">View More</Link>
							<i className="ion-chevron-right ml-2"></i>
						</span>
					}
					<div className="product-group row">
						{featuredProductsLoading && <img src={spinner} alt="loading indicator text-center" id="spinner" />}
						{featuredProducts &&
							<Fragment>
								{!featuredProducts.items.length && <p className="text-center pt-2">No featured products to show</p>} 
								{featuredProducts.items.slice(0, 8).map(item => <Product className={classNames} key={item.slug} product={item} wrapperClass={wrapperClass}/>)}
							</Fragment>
						}
					</div>
					<div className="d-md-none">
						<hr/>
						<h5 className="group-header pl-1 d-inline-block">Trending Products</h5>
						{trendingProducts && trendingProducts.hasMore && 
							<span className="float-right link__display">
								<Link to="more">View More</Link>
								<i className="ion-chevron-right ml-2"></i>
							</span>
						}
						<div className="product-group row">
							{trendingProductsLoading && <img src={spinner} alt="loading indicator text-center" id="spinner" />}
							{trendingProducts &&
								<Fragment>
									{!trendingProducts.items.length && <p className="text-center pt-2">No trending products to show</p>} 
									{trendingProducts.items.slice(0, 8).map(item => <Product className={classNames} key={item.slug} product={item} wrapperClass={wrapperClass}/>)}
								</Fragment>
							}
						</div>
					</div>
				</div>
			</Fragment>
		)
	}


}

export default connect(mapStateToProps, mapDispatchToProps)(Home)