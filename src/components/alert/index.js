import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';

class Toast extends Component {
	toastId = null;

	notify = () => {
		this.toastId = toast(this.props.children, this.props.options)
	};

	componentDidUpdate() {
		// toast.dismiss(this.toastId); // hack to force update new component
		// this.notify(this.toastId);
		if (!toast.isActive(this.toastId)) this.notify()
		else this.update()
	}

	componentDidMount() {
		this.notify()
	}

	update = () => toast.update(this.toastId, {render: this.props.children, ...this.props.options});

	render() {
		return   <ToastContainer className={'toaster ' + this.props.className}/>
	}
}

export default Toast;