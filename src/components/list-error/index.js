import React from 'react';

export default (props) => (
  <React.Fragment>
    {!props.message && !props.errors && <li>Please try later</li>}
    {props.message && <li>{props.message}</li>}

    {props.errors && (
      Object.keys(props.errors)
        .map((message, i) => <li key={i}>{message.split('.').pop()}: {props.errors[message]}</li>)
    )}
  </React.Fragment>
)