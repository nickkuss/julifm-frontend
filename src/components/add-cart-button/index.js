import React from 'react';
import { connect } from 'react-redux';
import { getProductQtyInCart } from '../../utils/product'

const styles = {
	fontSize: '1.2rem',
}

const AddToCart = ({product, cart}) => {
	const quantity = getProductQtyInCart(cart ? cart.items : null, product)
	return (
		<span style={{ fontSize: '.6rem', marginTop: -3 }}>
			<i className="ion-ios-cart" style={{ ...styles,
				color: !cart || !quantity ? '#777272' : '#28a745'
				}}>
			</i> 
			{quantity > 0 && quantity}
		</span>
	)
}

const mapStateToProps =  (state) => ({
	cart: state.cart.cart,
	cartError: state.cart.error,
	isCartLoading: state.cart.isLoading
});

export default connect(mapStateToProps, null)(AddToCart)