import React from 'react';
import spinner from '../../shared/spinner.svg';

export default (props) => (
	<div id="spinner-holder" 
		style={{ maxWidth: props.size, top: props.topDistance }}
	>
		<img src={spinner} alt="loading indicator" id="spinner" style={{ width: props.imgSize }}/>
	</div>
)