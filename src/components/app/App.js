import React, { Component } from 'react';
import { connect } from 'react-redux';
import DocumentTitle from 'react-document-title';
import { ThemeProvider } from "styled-components";
import Layout from '../layout';
import Auth from '../auth';
import BrowserRouter from 'react-router-dom/BrowserRouter';
import Switch from 'react-router-dom/Switch';
import Route from 'react-router-dom/Route';
import PrivateRoute from '../private-route';
import config from '../../config';
import Pusher from 'pusher-js';
import headerActions from '../../actions/header';
import userActions from '../../actions/user';
import cartActions from '../../actions/cart';
import locationActions from '../../actions/locations';
import 'react-toastify/dist/ReactToastify.min.css';
import Checkout from '../checkout';
import PaymentHandler from '../payment-handler';
import storage from '../../utils/storage';
import request from '../../api/request';
import themes from "../catalog-listing/algolia/themedefault";

// const HOCWraps = {
//   Layout: ErrorHandler(Layout)
// }

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentDidMount() {
    console.log('APP MOUNT', this.props)

    this.pusher = new Pusher(config.pusherKey, {
			cluster: config.pusherCluster,
      encrypted: true,
      authEndpoint: config.pusherAuthEndpoint
    });
    
    // const path = window.location.pathname;
    // if (path !== '/login' && path !== '/register') this.props.getCart();
    
    this.props.getProvinces();
    this.props.getCities();
    this.props.getCouriers();
  }

  componentWillUnmount() {
    if (this.props.user) {
      this.unsubscribe()
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    // workaround to make sure get user action is dispatched before render 
    // private routes tend to redirect before mounting occurs, and always assumes user is unauthenticated
    if (!prevState.authStatusChecked) { // only chek just once
      const token = storage.getToken();
      if (!token) return { authStatusChecked: true }; // no need fetchin
      request.setToken(token);

      const path = window.location.pathname;
      if (path === '/login' || path === '/register') return null; // save one query by not fetching user; 

      nextProps.getCurrentUser();
      return { authStatusChecked: true }
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState) {
    console.log('APP UPDAYE', prevProps, this.props)

    const { token, user } = this.props

    if ((token && !prevProps.token) || (user && !prevProps.user)) {
      // first authentication or re-authentication
      if (token )request.setToken(token);
      
      if (!this.props.user) {
        // just got token i.e after authentication
        this.props.getCurrentUser();
      } else {
        this.subscribe();
        this.props.getNotifications();
        this.props.getCart()
      }
    } else if (this.props.userFetchError) {
      // error when fetching user e.g invalid token
      storage.removeToken();
      storage.removeCartId();
      return false;
    }
  }

  subscribe() {
    if (this.hasSubscribed) {
      console.log('unsubbing first')
      this.unsubscribe();
    }
    
    this.hasSubscribed = true; // useful to unsubscribe e.g on reauthentication
    
    this.channel = this.pusher.subscribe(`private-${this.props.user.id}`,
      (data) => {
        console.log('channeled', data);
      }, (error) => console.log('channeled e', error),
    );
		this.channel.bind(config.pusherNotificationEvent, (data) => {
      console.log('binded', data)
      this.props.onNotification(data);
			// const product = this.props.product;
			// console.log(data);
			// if (product && data.product === product.id) {
			// 	this.props.onNewBid(data);
			// }
		}, (e) => console.log('binded err', e));
  }

  unsubscribe() {
    console.log('unsubbing');
    this.pusher.unsubscribe(`private-${this.props.user.id}`);
		this.channel.unbind(config.pusherNotificationEvent);
  }

  render() {
    return(
      <DocumentTitle title={`Juli | ${config.homePageTitle}`}>
        <BrowserRouter>
          <ThemeProvider theme={themes}>
            <div id="app-container" className="App">
              <Switch>
                <Route exact path="/login" component={Auth}/>
                <Route exact path="/register" component={Auth}/>
                <PrivateRoute path='/checkout' component={Checkout} />
                <PrivateRoute path='/payment' component={PaymentHandler} />
                <Route path="/" component={Layout}/>
              </Switch>
            </div>
            </ThemeProvider>
        </BrowserRouter>
      </DocumentTitle>
    )
    
  }
}


export default connect(
  (state) => ({
    user: state.shared.currentUser,
    userFetchError: state.shared.userFetchError,
    token: state.auth.token
  }),
  (dispatch) => ({
    getCurrentUser: () => dispatch(userActions.getUser()),
    onNotification: (data) => dispatch(headerActions.onNewNotification(data)),
    getNotifications: () => dispatch(headerActions.getNotifications()),
    getCart: () => dispatch(cartActions.getCart()),
    getProvinces: () => dispatch(locationActions.getProvinces()),
    getCities: () => dispatch(locationActions.getCities()),
    getCouriers: () => dispatch(locationActions.getCouriers()),
  })
)(App);
