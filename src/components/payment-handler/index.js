import React, { Component } from 'react';
import { connect } from 'react-redux';
import DocumentTitle from 'react-document-title';
import Navbar from 'reactstrap/lib/Navbar';
import Link from 'react-router-dom/Link';
import { pageTitleText } from '../../utils/app';


const FINISH_URL = '/payment/finish';
const UNFINISH_URL = '/payment/unfinish';
const ERROR_URL = '/payment/error';
const REDIRECT_PERIOD = 3 // 3 seconds

class PaymentHandler extends React.Component {

  state = { timeLeft: REDIRECT_PERIOD }

  componentDidMount() {
    const { location: { pathname }, history } = this.props;
    if (pathname === FINISH_URL && this.state.timeLeft) {
      this.interval = setInterval(() => {
        if (this.state.timeLeft > 1) this.setState({ timeLeft: this.state.timeLeft - 1 })
        else history.push('/customer/history');
      }, 1000)
    }
  }

  componentWillUnmount() {
    if (this.interval) clearInterval(this.interval);
  }

  render() {
    const { location: { pathname } } = this.props;
    const { timeLeft } = this.state;
    return (
      <DocumentTitle title={pageTitleText('Payment')}>
        <React.Fragment>
          <Navbar color="faded" light style={{ backgroundColor: '#eee' }} className="sticky-top">
            <Link to="/" className="mr-auto ml-md-4 navbar-brand">Juli</Link>
          </Navbar>

          <div className="container mt-5">
            <div className="text-center">
              <div className="text-center">
                {pathname === '/payment/finish' &&
                  <React.Fragment>
                    <p className="text-success font-weight-bold" style={{ fontSize: '2rem' }}>
                      Your payment is successful
                    </p>
                    <span>redirecting you to your order in {timeLeft} seconds...</span>
                  </React.Fragment>
                }
                {pathname === '/payment/unfinish' &&
                  <p className="text-danger font-weight-bold" style={{ fontSize: '2rem' }}>
                    Your payment is not completed
                  </p>
                }
                {pathname === '/payment/error' &&
                  <p className="text-mute font-weight-bold" style={{ fontSize: '2rem' }}>
                    Your payment is unsuccessful
                  </p>
                }
              </div>
              <p class="font-weight-bold link__display" style={{ fontSize: '1rem' }}>
                <Link to="/">Go Home</Link>
              </p>
            </div>
          </div>
        </React.Fragment>
      </DocumentTitle>
    )
  }
}

export default PaymentHandler;