import React, { Component } from 'react';
import Collapse from 'reactstrap/lib/Collapse';
import Link from 'react-router-dom/Link';
import classnames from 'classnames';
import './style.css';
import { findCategoryBySlug } from '../../utils/categories';

const Fragment = React.Fragment;

class CategoryItem extends Component {

  constructor(props) {
    super(props);
		this._onClick = this._onClick.bind(this);
	}

  render() {
		const { name, slug, subs } = this.props.item;
		const path = window.location.pathname.slice(1);
		const isCurrentPath = path === `catalog/${slug}`

		if (isCurrentPath || (subs && subs.length)) {
			return (
				<li onClick={isCurrentPath ? null : this._onClick} 
					className={classnames('pr-1 pl-2 list-group-item rounded-0', {
						'text-primary': isCurrentPath
					})}>
					{name}
					{this.props.children}
				</li>
			);
		} else {
			return (
				<Link to={'/catalog/' + slug} 
					className='pr-1 pl-2 list-group-item'>
					{name}
					{this.props.children}
				</Link>
			);
		}
  }
  _onClick(e) {
		// somehow link click events bubbles up. Only call handler if not active ie with 'text-primary' color) or link clicked (has href)
		e.stopPropagation();
		if (e.target && e.target.classList.contains('text-primary')) return e.preventDefault();
		if (e.target && e.target.href) return;

		const item = this.props.item;
		const name = item.name || item; // 3rd level categories do not have a 'name' property
		const slug = item.slug || name; 
    this.props.onItemClick(slug);
  }
}

class Categories extends Component {

	constructor(props) {
		super(props);

		this.toggleVisibility = this.toggleVisibility.bind(this);
		this.state = {categories: {}};
	}

	toggleVisibility(currentItem) {
		const categories = {...this.state.categories};
		const isOpen = categories[currentItem];

		if (!isOpen) { // was closed. should be open now. Make sure parent is open			
			// close all categories and only expand current
			Object.keys(categories).forEach(key => {
				categories[key] = false;
			})
			let parent = currentItem;
			
			while (parent) {
				categories[parent] = true;
				const _parent = findCategoryBySlug(this.props.categories, parent, true)
				parent = _parent ? _parent.slug : null
			}
		} else categories[currentItem] = false;

		this.setState({ categories });
	}

	getClassNames(slug) {
		return classnames(
			'float-right',
			{
				'ion-chevron-up': this.state.categories[slug] === true,
				'ion-chevron-down': !this.state.categories[slug],
			}
		)
	}
	
	render() {
		if (!this.props.categories) return null;

		return (
			<ul className="pl-0 ml-0">
				{this.props.categories.map(cat1 => {
					return (
						<CategoryItem item={cat1} key={cat1.slug} onItemClick={this.toggleVisibility}>
							{
								(cat1.subs && cat1.subs.length > 0) &&
								<Fragment>
									<i className={this.getClassNames(cat1.slug)}></i>
									<Collapse isOpen={this.state.categories[cat1.slug]}>
										<ul id="sub-cats" className="pl-2">
											{cat1.subs.map(cat2 => (
												<CategoryItem item={cat2} key={cat2.slug} onItemClick={this.toggleVisibility}>
													{
														(cat2.subs && cat2.subs.length > 0) &&
														<Fragment>
															<i className={this.getClassNames(cat2.slug)}></i>
															<Collapse isOpen={this.state.categories[cat2.slug]}>
																<ul id="sub-sub-cats" className="pl-2">
																	{cat2.subs.map(cat3 => (
																		<CategoryItem item={cat3} className="list-group-item sub-item" key={cat3.slug} onItemClick={this.toggleVisibility} />
																	))}
																</ul>
															</Collapse>
														</Fragment>
													}
												</CategoryItem>
											))}
										</ul>
									</Collapse>
								</Fragment>
							}
						</CategoryItem>
					)
				})}
			</ul>
		);
	}
}


export default Categories