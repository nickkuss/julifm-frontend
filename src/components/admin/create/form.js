import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import adminActions from '../actions';
import config from '../../../config';

const priorityStyle = {
	maxHeight: 200,
	overflow: 'auto',

}

class CreateEditForm extends Component {
	constructor(props) {
		super(props);
		this.path = window.location.pathname.split('/').pop();
		this.state = {
			data: this.props.item || { level: 1 },
			form: null,
			parentCategories: null,
			formError: null
		}
		this.setParentCategories(this.state)
		this.handleChange = this.handleChange.bind(this);
		this.submit = this.submit.bind(this);
	}

	setParentCategories(state) {
		const { categories } = this.props
		const { data: { level } } = (state || this.state)
		if (categories && level > 1 && this.path === 'categories') {
			if (state) {
				state.parentCategories = categories.items.filter(c => c.level === state.data.level - 1)
			} else {
				this.setState({
					parentCategories: categories.items.filter(c => c.level === level - 1)
				})
			}
		}
	}

	componentWillReceiveProps(prevProps, nextProps) {
		if (this.path === 'categories' && !this.state.parentCategories && nextProps.categories) {
			this.setParentCategories()
		}
	}

	submit(event) {
		event.preventDefault();
		event.stopPropagation(); // prevent parent form from submitting
		
		// const formValid = this.isValid();
		const { addCategory, editCategory, addBrand, editBrand, item } = this.props
		const { data, form } = this.state;
		if (this.path === 'brands') {
			if (item) return editBrand(item._id, data)
			else return addBrand(data.isBulk ? form : data, data.isBulk)
		} else if (this.path === 'categories') {
			if (item) return editCategory(item._id, data)
			else return addCategory(data.isBulk ? form : data, data.isBulk)
		}
	}

	handleChange(event) {
		const target = event.target;
		let value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		const newState = {...this.state, data: { ...this.state.data }, formError: null }

		if (name === 'image' && target.files.length) {
			if (window.FileReader) {
				const reader = new FileReader;
				const file = target.files[0];
				if (file.size > config.categoryFileMaxSize) {
					this.setState({ formError: `image should be less than ${(config.categoryFileMaxSize / 1000).toFixed(1)}kb` })
				} else {
					reader.readAsDataURL(file);
					reader.onload = () => {
						newState.data.image = reader.result
						this.setState(newState)
					}
				}
			}  else {
				alert('This browser does not support image upload')
			}
		} else if (name === 'file') {
			const form = new FormData()
			console.log('targetee', target.files)
			form.append('file', target.files.length ? target.files[0] : null)
			newState.form = form
			this.setState(newState)
		} else {
			if (name === 'image' && !target.files.length) value = this.state.data.photo
			
			newState.data[name] = value
			if (name === 'level') {
				newState.data.level = Number(value)
				this.setParentCategories(newState)
			}
			this.setState(newState);
		}
	}
	
	renderPriorities() {
		const { data} = this.state;
		const { categories } = this.props;
		if (!categories) return null

		const items = data.level === 1 ? categories.items.filter(c => c.level === data.level && !c.parent)
			: categories.items.filter(c => c.level === data.level && c.parent === data.parent)
		return (
			<div className="col-12" style={priorityStyle}>
				<p className="font-weight-bold">Current Level {data.level} priorites {data.parent ? `for ${data.parent}` : ''}</p>
				<div className="card d-block">
					{items.map((x,i) =>
						<button key={i} className="btn btn-sm btn-default m-1">
							{x.name} <span className="badge badge-secondary">{x.priority}</span>
						</button>
					)}
				</div>
			</div>
		)
	}
  
	render() {
		const { data, parentCategories, formError } = this.state;
		const { isLoading, item } = this.props;

		return (
			<React.Fragment>
				<form noValidate onSubmit={this.submit}>
					{ !item && 
						<div className="row">
							<div className="col-12">
								<div className="form-group">
									<input type="checkbox" name="isBulk" id="isBulkId" onChange={this.handleChange}/>
									<label htmlFor="isBulkId" className="font-weight-bold">Upload bulk data</label>
								</div>
							</div>
						</div>
					}
					{!data.isBulk &&
						<div className="row">
							<div className="col-12">
								<div className="form-group">
									<label htmlFor="name" className="control-label font-weight-bold">Name*</label>
									<input className="form-control" name="name" id="name" value={data.name || ''} onChange={this.handleChange} autoFocus/>
								</div>
							</div>
						</div>
					}
					{ this.path === 'categories' && !data.isBulk && 
						<div className="row">
							<div className="col-12">
								<div className="form-group">
									<label htmlFor="level" className="control-label font-weight-bold">Level*</label>
									<select onChange={this.handleChange} name="level" id="level" value={data.level}>
										<option value={1}>1</option>
										<option value={2}>2</option>
										<option value={3}>3</option>
									</select>
								</div>
							</div>
							{!!data.level && data.level !== 1 && 
								<div className="col-12">
									<div className="form-group">
										<label htmlFor="parent" className="control-label font-weight-bold">Parent*</label>
										<select onChange={this.handleChange} name="parent" id="parent" value={data.parent}>
											<option value="">Select Parent</option>
											{!!parentCategories && parentCategories.map(c => <option value={c.name} key={c._id}>{c.name}</option>)}
										</select>
									</div>
								</div>
							}
							<div className="col-12">
								<div className="form-group">
									<label htmlFor="priority" className="control-label font-weight-bold">Priority</label>
									<input className="form-control" name="priority" id="priority" type="number" value={data.priority || ''} onChange={this.handleChange}/>
								</div>
							</div>
							{this.renderPriorities()}
							<div className="col-12">
								<div className="form-group">
									<label htmlFor="photo" className="control-label font-weight-bold">Photo (Max {(config.categoryFileMaxSize / 1000).toFixed(1)}kb)</label>
									<div>
										{ (data.photo || data.image) && <img src={data.photo || data.image} className="thumbnail mr-2" style={{ maxWidth: 100 }}/> }
										<input type="file" onChange={this.handleChange} name="image" id="photo" className="mb-1"/>
										{formError && <p className="text-danger font-weight-bold">{formError}</p> }
									</div>
								</div>
							</div>
						</div>
					}

					{data.isBulk && !item && 
						<div className="row">
							<div className="col-12">
								<div className="form-group">
									<label htmlFor="file" className="control-label font-weight-bold">File (xls or xlxs)</label>
									<div>
										<input type="file" accept=".xls, .xlsx" onChange={this.handleChange} name="file" id="file" className="mb-1"/>
										{formError && <p className="text-danger font-weight-bold">{formError}</p> }
									</div>
								</div>
							</div>
						</div>
					}
					
					<button
						className={classnames('btn btn-secondary btn-block rounded-0', {
							'loading': isLoading
						})} disabled={isLoading}>Submit</button>
					<div className="clearfix"></div>
				</form>
			</React.Fragment>
		)
	}
}


export default connect(({ admin }) => ({
	isLoading: admin.isLoading,
	error: admin.error,
	categories: admin.categories
}), (dispatch) => ({
	addCategory: (data, isBulk) => dispatch(adminActions.addCategory(data, isBulk)),
	addBrand: (data, isBulk) => dispatch(adminActions.addBrand(data, isBulk)),
	editCategory: (id, data) => dispatch(adminActions.editCategory(id, data)),
	editBrand: (id, data) => dispatch(adminActions.editBrand(id, data)),
}))(CreateEditForm);
