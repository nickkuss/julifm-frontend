import React, { Component } from 'react';
import { connect } from 'react-redux';
import Modal  from 'reactstrap/lib/Modal';
import CreateEditForm from './form';
import Alert from '../../alert';
import ListErrors from '../../list-error';

class AdminCreateModal extends Component {
	constructor(props) {
    super(props);
    this.state = {};
		this.close = this.close.bind(this);
		this.onClosed = this.onClosed.bind(this);
  }

  close() {
    this.setState({ isOpen: false, manualClose: true});
	}

	static getDerivedStateFromProps(nextProps, state) {
		const obj = {};

		if (!nextProps.isOpen) {
			obj.isOpen = false;
			obj.manualClose = false;
		} else obj.isOpen = !state.manualClose;
		
		return obj;
	}

	componentDidUpdate(prevProps, prevState) {
		const path = window.location.pathname.split('/').pop();
		if (prevProps[path] !== this.props[path]) {
			this.close()
		}

	}

	onClosed() {
		this.props.onModalClose()
	}
	
	
	render() {
		const  { isLoading, error, title, item } = this.props;
		return (
		<Modal isOpen={this.state.isOpen}  onClosed={this.onClosed}
			fade={false} toggle={this.close} className={this.props.className}>
			<div className="modal-header">
				<h5 className="modal-title capitalize">{item ? 'Edit' : 'Add New'} {title}</h5>
				<button type="button" className="close" 
					onClick={this.close}
					aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div className="modal-body">
				{error && !isLoading && <Alert options={{ autoClose: false, type: 'error' }}><ListErrors {...error} /></Alert>}
				<CreateEditForm item={item}/>
			</div>
		</Modal>
		)
	}
}

const mapStateToProps = ({ admin }) => ({
	isLoading: admin.isLoading,
	error: admin.error,
	brands: admin.brands,
	categories: admin.categories,
})

export default connect(mapStateToProps)(AdminCreateModal);
