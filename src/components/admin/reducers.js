import { 
  FETCH_ADMIN_BRANDS, FETCH_ADMIN_CATEGORIES,
  FETCH_ADMIN_USERS, FETCH_ADMIN_PRODUCTS, FETCH_ADMIN_ORDERS,
  ADMIN_PAGE_UNMOUNT, UPDATE_USER_ROLE,
} from './constants';
import * as types from './constants'
import { ASYNC_START } from '../../constants';

const defaultState = {
  error: null,
  isLoading: false
}

const initialState = {
  users: null,
  brands: null,
  categories: null,
  products: null,
  orders: null,
  ...defaultState
}

const AdminReducer = (state = initialState, { type, subtype, payload, error }) => {
  switch(type) {
    case ASYNC_START:
      const showIndicatorsFor = [
        FETCH_ADMIN_USERS, FETCH_ADMIN_BRANDS, FETCH_ADMIN_CATEGORIES, 
        types.ADD_BRAND, types.ADD_CATEGORY, types.EDIT_BRAND, types.EDIT_CATEGORY,
      ]
			let data = { ...state, error: null }
			if (showIndicatorsFor.indexOf(subtype) > -1) data.isLoading = true;
      return data;
    case FETCH_ADMIN_USERS:
      return { 
        ...state,
        isLoading: false,
        error: error ? payload : null,
        users: !error ? payload : state.users
      }
    case FETCH_ADMIN_BRANDS:
      return { 
        ...state,
        isLoading: false,
        error: error ? payload : null,
        brands: !error ? payload : state.brands
      }
    case FETCH_ADMIN_CATEGORIES:
      return { 
        ...state,
        isLoading: false,
        error: error ? payload : null,
        categories: !error ? payload : state.categories
      }
    case FETCH_ADMIN_PRODUCTS:
      return { 
        ...state,
        isLoading: false,
        error: error ? payload : null,
        products: !error ? payload : state.products
      }
    case FETCH_ADMIN_ORDERS:
      return { 
        ...state,
        isLoading: false,
        error: error ? payload : null,
        orders: !error ? payload : state.orders
      }
    case types.FETCH_ADMIN_REPORTS:
      return { 
        ...state,
        isLoading: false,
        error: error ? payload : null,
        reports: !error ? payload : state.reports
      }
    case types.FETCH_ADMIN_PAYOUTS:
      return { 
        ...state,
        isLoading: false,
        error: error ? payload : null,
        payouts: !error ? payload : state.payouts
      }
    case types.ADD_BRAND:
      return {
        ...state,
        isLoading: false,
        error: error ? payload : null,
        brands: !error ? 
          state.brands ? {... state.brands, items: payload.concat(state.brands.items) } :
          payload : state.brands
      }
    case types.ADD_CATEGORY:
      return {
        ...state,
        isLoading: false,
        error: error ? payload : null,
        categories: !error ? 
        state.categories ? {
          ...state.categories, 
          items: [...payload, ...state.categories.items],
          count: state.categories.count + payload.length
        } : payload : state.categories
      }
    case types.EDIT_CATEGORY:
      return {
        ...state,
        isLoading: false,
        error: error ? payload : null,
        categories: !error ?  {
            ... state.categories, 
            items: state.categories.items.map(c => c._id === payload._id ? { ...c, ...payload } : c)
          } : state.categories
      }
    case types.EDIT_BRAND:
      return {
        ...state,
        isLoading: false,
        error: error ? payload : null,
        brands: !error ?  {
            ... state.brands, 
            items: state.brands.items.map(b => b._id === payload._id ? payload : b)
          } : state.brands
      }
    case types.DELETE_CATEGORY:
    case types.DELETE_BRAND:
      let prop = type === types.DELETE_CATEGORY ? 'categories' : 'brands'
      return {
        ...state,
        isLoading: false,
        error: error ? payload : null,
        [prop]: !error ? {
          ...state[prop],
          items: state[prop].items.filter(x => x._id !== payload._id)
        } : state[prop]
      }
    case types.UPDATE_USER_ROLE:
      return {
        ...state,
        isLoading: false,
        error: error ? payload : null,
        users: !error ?  {
            ... state.users, 
            items: state.users.items.map(b => b.id === payload.id ? payload : b)
          } : state.users
      }
    case ADMIN_PAGE_UNMOUNT:
      return { ...state, ...defaultState }
    default: 
      return state
  }
}

export default AdminReducer;