import React from 'react';
import { connect } from 'react-redux';
import Link from 'react-router-dom/Link';
// import classnames from 'classnames';
// import ReactTable from 'react-table';
import DocumentTitle from 'react-document-title';
// import { getBrands, getCategories, getUsers, getProducts, getOrders } from '../actions';
import ListErrors from '../list-error';
import Alert from '../alert';
import Spinner from '../spinner';
// import { formatAmount, formatDate } from '../../../utils/app';
// import config from '../../../config';
import { getOrderItem } from './actions';
import OrderItem from '../order-item';

class OrderInfo extends React.Component {

  state = { isLoading: false, order: null, error: null }

  componentDidMount() {
    const { match: { params: { id } } } = this.props;
    this.setState({ isLoading: true })
    getOrderItem(id).then(order => {
      this.setState({ order, isLoading: false })
    }).catch(error => this.setState({ error, isLoading: false }))
  }

  render() {
    const { isLoading, error, order } = this.state;

    if (isLoading) return <Spinner />

    return (
      <DocumentTitle title={`Juli | ${order ? order.id.slice(0, 7) : 'Order'}`}>
        <React.Fragment>
          <div className="pt-3">
            <h5>Order #{order ? order.id : ''}</h5>
            {error && <Alert options={{ autoClose: false, type: 'error' }}>
              <ListErrors {...error} />
            </Alert>
            }
            {order && order.buyer &&
              <div className="row">
                <div className="col-12">
                  <div className="card pl-2 mb-1">
                    <h5>Buyer</h5>
                    <div>
                      <img src={order.buyer.photo} className="thumbnail img-responsive mr-2 float-left" style={{ maxWidth: 100 }} />
                      <div className="font-weight-bold d-inline">
                        <Link to={`/user/${order.buyer.username}`} className="link__display">{order.buyer.username}</Link>
                        <p className="mb-1 mt-1">
                          <a href={`tel:${order.buyer.phone}`} className="link__display">
                            <i className="ion-ios-telephone mr-1"></i>
                            {order.buyer.phone}
                          </a>
                        </p>
                        <p className="mb-1"><a href={`mailto:${order.buyer.email}`} className="link__display">{order.buyer.email}</a></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            }

            <OrderItem order={order} />
          </div>
        </React.Fragment>
      </DocumentTitle>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.shared.currentUser,
})

export default connect(mapStateToProps)(OrderInfo)