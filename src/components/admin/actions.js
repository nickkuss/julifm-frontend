import { 
  FETCH_ADMIN_BRANDS, FETCH_ADMIN_CATEGORIES, FETCH_ADMIN_USERS, FETCH_ADMIN_PRODUCTS, FETCH_ADMIN_ORDERS,
  UPDATE_USER_ROLE, FETCH_ADMIN_REPORTS, FETCH_ADMIN_PAYOUTS
} from './constants';
import * as types from './constants'
import api from "../../api";

export const getBrands = (query) => ({
  type: FETCH_ADMIN_BRANDS,
  payload: api.admin.brands(query)
})

export const getCategories = (query) => ({
  type: FETCH_ADMIN_CATEGORIES,
  payload: api.admin.categories(query)
})

export const getUsers = (query) => ({
  type: FETCH_ADMIN_USERS,
  payload: api.admin.users(query)
})

export const updateUserRole = (id, role) => ({
  type: UPDATE_USER_ROLE,
  payload: api.admin.updateUserRole(id, role)
})

export const getProducts = (query) => ({
  type: FETCH_ADMIN_PRODUCTS,
  payload: api.admin.products(query)
})

export const getOrders = (query) => ({
  type: FETCH_ADMIN_ORDERS,
  payload: api.admin.orders(query)
})

export const getReports = (query) => ({
  type: FETCH_ADMIN_REPORTS,
  payload: api.admin.reports(query)
})

export const getPayouts = (query) => ({
  type: FETCH_ADMIN_PAYOUTS,
  payload: api.admin.payouts(query)
})

export const getOrderItem = (query) => api.admin.orderItem(query)
  // type: FETCH_ADMIN_ORDER_ITEM,
  // payload: api.admin.orders(query)

export const addCategory = (data, isBulk) => ({ type: types.ADD_CATEGORY, payload: api.admin.addCategory(data, isBulk) })
export const editCategory = (id, data) => ({ type: types.EDIT_CATEGORY, payload: api.admin.editCategory(id, data) })
export const deleteCategory = (id) => ({ type: types.DELETE_CATEGORY, payload: api.admin.deleteCategory(id) })
export const addBrand = (data, isBulk) => ({ type: types.ADD_BRAND, payload: api.admin.addBrand(data, isBulk) })
export const editBrand = (id, data) => ({ type: types.EDIT_BRAND, payload: api.admin.editBrand(id, data) })
export const deleteBrand = (id) => ({ type: types.DELETE_BRAND, payload: api.admin.deleteBrand(id) })

export default { addCategory, editCategory, addBrand, editBrand }