import React from 'react';
import { connect } from 'react-redux';
import Link from 'react-router-dom/Link';
import classnames from 'classnames';
import ReactTable from 'react-table';
import 'react-table/react-table.css'
import DocumentTitle from 'react-document-title';
import { UncontrolledButtonDropdown, DropdownMenu, DropdownItem, DropdownToggle } from 'reactstrap';
import { getBrands, getCategories, getUsers, updateUserRole, getProducts, getOrders, getReports, getPayouts, deleteCategory, deleteBrand } from '../actions';
import ListErrors from '../../list-error';
import Alert from '../../alert';
import Spinner from '../../spinner';
import AdminCreateModal from '../create/modal'
import { formatAmount, formatDate } from '../../../utils/app';
import config from '../../../config';
import { formatWeight } from '../../../utils/product';
import './style.css'

export const Tips = () =>
  <div style={{ textAlign: "center" }}>
    <em>Tip: Hold shift when sorting to multi-sort!</em>
  </div>;

const btnStyle = { backgroundColor: 'rgb(245, 245, 245)' }
const clearBtnStyle = { 
  position: 'absolute',
  top: '.6rem',
  right: '.5rem',
  color: '#4c4848',
}

class DropDownItem extends React.Component {
  constructor(props) {
    super(props);
		this._onClick = this._onClick.bind(this);
  }
  _onClick(e) {
		const { item } = this.props;
    this.props.onItemClick(item);
  }
  render() {
    const { item, selected } = this.props;
    const isSelected = selected && selected.label === item.label

    return (
      <DropdownItem onClick={this._onClick} className={isSelected ? 'active' : undefined}>
        {item.label}
      </DropdownItem>
    )
  }
}

class AdminTable extends React.Component {

  state = { 
    createModalOpen: false, searchTerm: '', tableState: {}, filterField: null
  }

  constructor(props) {
    super(props);
    this.path = window.location.pathname.split('/').pop();
    this.showCreateModal = this.showCreateModal.bind(this);
    this.onModalClose = this.onModalClose.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.onSearch = this.onSearch.bind(this);
    this.clearSearch = this.clearSearch.bind(this);
    this.setFilter = this.setFilter.bind(this);

    this.tableMapping = {
      brands: { 
        fetch: this.props.getBrands,
        field: 'brands',
        title: 'Brands',
        titleSingular: 'Brand',
        canCreate: true,
        searchProps: [{name: 'name'}],
      },
      categories: {
        fetch: this.props.getCategories,
        field: 'categories',
        title: 'Categories',
        titleSingular: 'Category',
        canCreate: true,
        searchProps: [{name: 'name'}],
      },
      users: {
        fetch: this.props.getUsers,
        field: 'users',
        title: 'Users',
        searchProps: [{username: 'username'}, {email: 'email'}, {phone: 'phone'}, {firstName: 'first name'}, {lastName: 'last name'}],
        filters: [
          { label: 'All', value: {} }, 
          { label: 'Admins', value: {role: 'admin'} },
          { label: 'Editors', value: {role: 'editor'} }, 
          { label: 'Featured users', value: { isFeatured: true } },
          { label: 'Active users', value: { status: 'active'} },
          { label: 'Banned users', value: { status: 'banned'} }
        ]
      },
      products: {
        fetch: this.props.getProducts,
        field: 'products',
        title: 'Products',
        searchProps: [{name: 'name'}],
        filters: [
          { label: 'All', value: {} },
          { label: 'Within the last 24hrs', value: {createdAt: '1d'} }, 
          { label: 'Within the last month', value: { createdAt: '30d' } },
          { label: 'Within the last 3months', value: { createdAt: '90d' } },
          { label: 'Auctioned products', value: {saleFormat: 'auction'} },
          { label: 'Featured products', value: { isFeatured: true } },
          { label: 'Active products', value: { isActive: true } },
          { label: 'Deleted products', value: { status: 'deleted'} }
        ]
      },
      orders: {
        fetch: this.props.getOrders,
        field: 'orders',
        title: 'Orders',
        searchProps: [{_id: 'id'}, {paymentInfo: 'payment info'}],
        filters: [
          { label: 'All', value: {} }, 
          { label: 'Within the last 24hrs', value: {createdAt: '1d'} }, 
          { label: 'Within the last month', value: { createdAt: '30d' } },
          { label: 'Within the last 3months', value: { createdAt: '90d' } },
          { label: 'Paid Orders', value: { status: 'paid'} },
          { label: 'Closed Orders', value: { status: 'complete'} }
        ]
      },
      reports: {
        fetch: this.props.getReports,
        field: 'reports',
        title: 'Reports',
        searchProps: [{_id: 'id'}, {paymentInfo: 'payment info'}],
        filters: [
          { label: 'All', value: {} }, 
          { label: 'Open Reports', value: { isClosed: 'none'} },
          { label: 'Closed Reports', value: { isClosed: true } },
          { label: 'Created within the last 24hrs', value: {createdAt: '1d'} }, 
          { label: 'Created within the last month', value: { createdAt: '30d' } },
          { label: 'Created within the last 3months', value: { createdAt: '90d' } },
        ]
      },
      payouts: {
        fetch: this.props.getPayouts,
        field: 'payouts',
        title: 'Payouts',
        searchProps: [{_id: 'id'}, {paymentInfo: 'payment info', paymentRef: 'payment reference'}],
        filters: [
          { label: 'All', value: {} }, 
          { label: 'Unpaid Payouts', value: { isClosed: 'none'} },
          { label: 'Paid Payouts', value: { isClosed: true } },
          { label: 'Created within the last 24hrs', value: {createdAt: '1d'} }, 
          { label: 'Created within the last month', value: { createdAt: '30d' } },
          { label: 'Created within the last 3months', value: { createdAt: '90d' } },
        ]
      }
    }
    if (this.path === 'reports') this.state.filterField = this.tableMapping.reports.filters[1]
    if (this.path === 'payouts') this.state.filterField = this.tableMapping.payouts.filters[1]
  }

  componentDidMount() {
    // for categories, we fetch all at once. This means we don't need server side pagination, sorting etc
    if (this.path === 'categories') this.tableMapping[this.path].fetch({ limit: 'all' })
  }

  onModalClose() {
		this.setState({ createModalOpen: false })
	}

	showCreateModal(value, selected) {
		this.setState({ createModalOpen: true, selected: selected ? value : undefined })
  }

  deleteItem(item) {
    if (!window.confirm('Are you sure you want to delete this item?')) return

    const path = window.location.pathname.split('/').pop();
    const action = path === 'categories' ? this.props.deleteCategory : this.props.deleteBrand
    action(item._id)
  }
  
  handleChange(event) {
    const { target: { name, value } } = event;
    this.setState({ [name]: value })
  }

  clearSearch(event) {
    this.setState({ searchTerm: '' })
    this.search('', true)
  }

  search(term, filters) {
    const { tableState, searchTerm } = this.state;
    const mapping = this.tableMapping[this.path]
    const query = { s: term, ...filters }
    const field = this.props[mapping.field]

    if (field && field.perPage && 
      (tableState.pageSize !== tableState.defaultPageSize || field.perPage !== tableState.defaultPageSize)) {
        query.limit = tableState.pageSize
    }

    if (tableState.sorted && tableState.sorted.length) {
      query.sort = tableState.sorted[0].id,
      query.direction = tableState.sorted[0].desc ? 'desc' : 'asc'
    }
    console.log(query, searchTerm)
    mapping.fetch(query)
  }

  onSearch(e) {
    const { searchTerm } = this.state;
    console.log('termeee', searchTerm, e)
    e.preventDefault()
    const invalid = !searchTerm || searchTerm.trim().length < 3

    if (!invalid) this.search(searchTerm)
  }

  setFilter(filter) {
    const { filterField, searchTerm } = this.state
    if (filterField && filterField.label === filter.label) return;

    this.setState({ filterField: filter })
    this.search(searchTerm, filter.value)
  }

  renderFilterDropDown() {
    const { filterField } = this.state;
    const mapping = this.tableMapping[this.path]

    if (!mapping.filters) return null;
    const hasFilter = filterField && filterField.label !== 'All'
    return (
      <UncontrolledButtonDropdown>
        <DropdownToggle 
          caret={hasFilter} className="ion-funnel border-0" 
          style={{ backgroundColor: '#f5f5f5', color: '#312d2d' }}
          >
        {hasFilter && 
          <span className="ml-1" style={{ fontSize: '.8rem' }}>{ filterField.label.slice(0, 3) + '...' }</span> 
        }
        </DropdownToggle>
        <DropdownMenu className="admin-filter-dropdown">
          {mapping.filters.map(x => 
            <DropDownItem onItemClick={this.setFilter} selected={filterField} item={x}/>
          )}
        </DropdownMenu>
      </UncontrolledButtonDropdown>
    )
  }

  ratingCell() {
    return {
      id: 'averageRating',
      Header: 'Rating',
      accessor: v => v.meta.averageRating,
      Cell: prop => <span>
        {prop.value ?
          <React.Fragment>
            {prop.value}
            <i className="ion-star mr-1" style={{ color: config.starRatingColor }}></i>
            ({prop.original.meta.ratingCount})
          </React.Fragment> : '-'
        }
      </span>
    }
  }

  statusCell() {
    return {
      Header: 'Status',
      accessor: 'status',
      Cell: prop => <div>
        <select id="" value={prop.value}
          className={classnames({ 'text-danger': prop.value === 'banned' })}
          style={{ backgroundColor: 'transparent' }}>
          <option value="active">Active</option>
          <option value="inactive">Inactive</option>
          <option value="banned">Banned</option>
        </select>
      </div>
    }
  }

  imageCell() {
    return {
      Header: 'Photo',
      Cell: prop => (
        <Link to={'/' + prop.original.slug}>
				  <img src={prop.value} style={{ maxWidth: '50px' }} alt={prop.original.name}/>
			  </Link>
      )
    }
  }

  columns() {
    let cols = [];
    const { user } = this.props;

    switch(this.path) {
      case 'brands':
        cols = cols.concat([
          {
            Header: 'Name',
            accessor: 'name'
          }
        ])
        break;
      case 'categories':
        cols = cols.concat([
          {
            Header: '#',
            filterable: false,
            accessor: '_id',
            Cell: prop => <span>{prop.index + 1}</span>
          },
          {
            Header: 'Name',
            accessor: 'name'
          },
          {
            Header: 'Level',
            accessor: 'level'
          },
          {
            Header: 'Priority',
            accessor: 'priority'
          },
          {
            Header: 'Parent',
            accessor: 'parent',
            Cell: prop => prop.value || <span className="text-center d-block">-</span>
          },
          {
            Header: 'Photo',
            accessor: 'photo',
            className: 'text-center',
            filterable: false,
            Cell: prop => 
              <React.Fragment>
                {prop.value ? 
                  <a href="" target="_blank">
                    <img className="thumbnail" src={prop.value} style={{ width: 40, height: 40 }} />
                  </a> : '-'
                } 
              </React.Fragment> 
          }
        ])
        break;
      case 'users':
        cols = cols.concat([
          {
            Header: 'First Name',
            accessor: 'firstName'
          },
          {
            Header: 'Last Name',
            accessor: 'lastName',
          },
          {
            Header: 'Username',
            accessor: 'username',
            Cell: prop => <Link className="link__display"  to={`/user/${prop.value}`}>{prop.value}</Link>
          },
          {
            Header: 'Photo',
            accessor: 'photo',
            className: 'text-center',
            Cell: prop => prop.value ? <img className="thumbnail m-0" src={prop.value} width={40}/> : '-'
          },
          {
            Header: 'Email',
            accessor: 'email',
          },
          {
            Header: 'Phone',
            accessor: 'phone',
          },
          {
            Header: 'Role',
            accessor: 'role',
            Cell: prop => {
             if (!user || user.role !== 'admin') return <span>{prop.value}</span>

             return (
              <select id="" value={prop.value}
                disabled={user.id === prop.original.id}
                onChange={(e) => {
                  if (!window.confirm('Are you sure?')) return ;
                  this.props.updateUserRole(prop.original.id, {role: e.target.value })}
                }
                style={{ backgroundColor: 'transparent' }}>
                <option value="user">user</option>
                <option value="editor">editor</option>
                <option value="admin">admin</option>
              </select>
             )
            }
          },
          {
            Header: 'Wallet',
            accessor: 'balance',
            Cell: prop => <span>{formatAmount(prop.value)}</span>
          },
          {
            Header: 'Featured',
            accessor: 'isFeatured',
            Cell: prop => prop ? 'Yes' : 'No'
          },
          {
            id: 'sellCount',
            Header: '#Sales',
            accessor: v => v.meta.sellCount,
          },
          {...this.ratingCell() },
          {
            Header: 'Status',
            accessor: 'status',
            Cell: prop => <div>
              <select id="" value={prop.value}
                className={classnames({ 'text-danger': prop.value === 'banned' })}
                style={{ backgroundColor: 'transparent' }}>
                <option value="active">Active</option>
                <option value="inactive">Inactive</option>
                <option value="banned">Banned</option>
              </select>
            </div>
          }
        ])
        break;
      case 'orders':
        cols = cols.concat([
          {
            accessor: 'id',
            Header: 'Order ID',
            Cell: prop => <Link to={`/admin/orders/${prop.value}`} className="link__display" title={`order ID: ${prop.value}`}>
              {prop.value.slice(0, 7)} </Link>
          },
          {
            Header: 'Date',
            accessor: 'createdAt',
            Cell: prop => <span>{formatDate(prop.value)}</span>
          },
          {
            Header: 'Status',
            accessor: 'status',
            Cell: prop => {
              let text = prop.value;
              if (prop.original.isComplete) text = 'completed';
              if (prop.original.isPaid) text = 'paid';
              return <span className="capitalize">{text}</span>
            },
          },
          {
            Header: 'Amount',
            accessor: 'amount',
            Cell: prop => <span>{formatAmount(prop.value)}</span>
          },
          {
            Header: 'Shipping Cost',
            accessor: 'shippingCost',
            Cell: prop => <span className={classnames({'text-center d-block': !prop.value })}>
              {prop.value ? formatAmount(prop.value) : '-'}
            </span>
          },
          {
            Header: 'Payment Method',
            accessor: 'paymentMethod',
            Cell: prop => <span className="capitalize">{prop.value}</span>
          },
          {
            Header: 'Payment Info',
            accessor: 'paymentInfo',
            Cell: prop => <span style={{ whiteSpace: 'initial' }}>{prop.value}</span>
          },
          // {
          //   Header: 'Action',
          //   Cell: prop => (
          //     <select>
          //       <option value="" disabled>Paid</option>
          //       <option value="" disabled>Paid</option>
          //     </select>
          //   )
          // },
        ])
        break;
      case 'reports':
        cols = cols.concat([
          {
            accessor: '_id',
            Header: 'Report ID',
            Cell: prop => <Link to={`/admin/reports/${prop.value}`} className="link__display" title={`report ID: ${prop.value}`}>
              {prop.value.slice(0, 7)} </Link>
          },
          {
            accessor: 'order',
            Header: 'order ID',
            Cell: prop => <Link to={`/admin/orders/${prop.value}`} className="link__display" title={`order ID: ${prop.value}`}>
              {prop.value.slice(0, 7)} </Link>
          },
          {
            Header: 'Date',
            accessor: 'createdAt',
            Cell: prop => <span>{formatDate(prop.value)}</span>
          },
          {
            Header: 'Status',
            accessor: 'isClosed',
            Cell: prop => <span className="capitalize">{prop.value ? 'closed' : 'open'}</span>
          },
          {
            Header: 'Reason',
            accessor: 'reason',
            // Cell: prop => <span>{formatAmount(prop.value)}</span>
          },
          {
            Header: 'Note',
            accessor: 'comment',
            Cell: prop => <div className="capitalize"><textarea defaultValue={prop.value} readOnly={true}></textarea></div>
          },
          {
            Header: 'Action',
            accessor: '_id',
            Cell: prop => <button className="btn btn-sm btn-success" disabled={prop.original.isClosed}>Close</button>
          },
        ])
        break;
      case 'payouts':
        cols = cols.concat([
          {
            accessor: '_id',
            Header: 'Payout ID',
            Cell: prop => <span className="font-weight-bold">{prop.value}</span>
          },
          {
            Header: 'Date',
            accessor: 'createdAt',
            Cell: prop => <span>{formatDate(prop.value)}</span>
          },
          {
            Header: 'Amount',
            accessor: 'amount',
            Cell: prop => <span>{formatAmount(prop.value)}</span>
          },
          {
            Header: 'Status',
            accessor: 'isPaid',
            Cell: prop => <span className={classnames("capitalize", { 
              'text-success': prop.value
             })}>{prop.value ? 'paid' : 'unpaid'}</span>
          },
          {
            Header: 'Ref',
            accessor: 'paymentRef',
          },
          {
            Header: 'Notes',
            accessor: 'paymentInfo',
          Cell: prop => <div>{prop.value ? <textarea defaultValue={prop.value} readOnly={true}></textarea> : 'N/A'}</div>
          },
          {
            Header: 'Payment Date',
            accessor: 'paymentConfirmedAt',
            Cell: prop => <span>{prop.value ? formatDate(prop.value) : 'N/A'}</span>
          },
          {
            Header: 'Action',
            accessor: '_id',
            Cell: prop => <button className="btn btn-sm btn-secondary" disabled={prop.original.isPaid}>Update</button>
          },
        ])
        break;
      case 'products':
        cols = cols.concat([
          {
            accessor: 'id',
            Header: '#',
            Cell: prop => <span>{prop.index + 1} </span>
          },
          {
            ...this.imageCell(),
            id: 'productImage',
            accessor: p => p.images[0].url,
            Header: 'Photo'
          },
          {
            accessor: 'name',
            Header: 'Name',
            sort: true,
            Cell: prop => <Link to={'/i/' + prop.original.slug}>{prop.value}</Link>
          },
          {
            accessor: 'salePrice',
            Header: 'Price',
            sort: true,
            formatter: this.priceFormatter
          },
          {
            accessor: 'quantity',
            Header: 'Qty'
          },
          {
            accessor: 'weight',
            Header: 'Weight',
            Cell: prop => {
              const value = formatWeight(prop.value)
              return <span className={classnames({ 'text-center': !value })}>{value || '-'}</span>
            }
          },
          {...this.ratingCell() },
          {
            accessor: 'meta.sellCount',
            Header: 'Sales',
            sort: true,
          },
          {
            accessor: 'saleFormat',
            Header: 'Format'
          },
          {
            accessor: 'createdAt',
            Header: 'Uploaded',
            sort: true,
            Cell: prop => <span>{formatDate(prop.value)}</span>
          },
          {
            Header: 'Status',
            accessor: 'status',
            Cell: prop => {
              const status = prop.original.isDeleted ? 'isDeleted' : 
                prop.original.isFeatured ? 'isFeatured' : 'isActive'
              return (
                <div>
                  <select id="" value={status}
                    className={classnames({ 'text-danger': prop.original.isDeleted })}
                    style={{ backgroundColor: 'transparent' }}>
                    <option value="isFeatured">Featured</option>
                    <option value="isActive" disabled={true}>Active</option>
                    <option value="isDeleted">Deleted</option>
                  </select>
                </div>
              )
            }
          }
        ])
        break;
        
    }
    cols.forEach(col => col.headerClassName = 'font-weight-bold')
    return cols;
  }

  adminActions() {
    return {
      Header: 'Actions',
      className: 'text-center',
      headerClassName: ' font-weight-bold',
      filterable: false,
      Cell: prop => 
        <React.Fragment>
          <span className="link__display" onClick={() => this.ad(prop.original, true)}>Edit</span>
          <span className="text-danger font-weight-bold ion-android-delete ml-3 btn-sm pointer btn-default" onClick={() => this.deleteItem(prop.original)}></span>
        </React.Fragment> 
    }
  }

  filterCaseInsensitive(filter, row) {
    const id = filter.pivotId || filter.id;
    if (row[id] !== null && typeof row[id] === 'string') {
        return (
            row[id] !== undefined ?
            String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase()) : true
        )
    }
    else {
        return (
            String(row[filter.id]) === filter.value
        )
    }
  }
  

  render() {
    const { isLoading, error, user } = this.props;
    const { searchTerm, filterField } = this.state;
    const mapping = this.tableMapping[this.path]
    const field = this.props[mapping.field]
    const searchProps = this.tableMapping[this.path].searchProps || [];
    const searchPropsText = searchProps.reduce((a, b) => a.concat(Object.values(b)), [])

    let items, perPage, page, count, totalResults, pages;
    if (field) {
      items = field.items;
      perPage = field.perPage;
      page = field.page;
      count = field.count;
      pages = Math.ceil(field.totalResults / field.perPage)
    }
    const isCategoriesPath = this.path === 'categories'
    const columns = user.role === 'admin' && mapping.canCreate ? 
      [...this.columns(), this.adminActions()] : this.columns()
    // if (!field && isLoading && !error) return <Spinner />

    const shouldDisableBtn = !searchTerm || searchTerm.trim().length < 2
    return (
      <DocumentTitle title={`Juli | ${this.tableMapping[this.path].title}`}>
          <React.Fragment>
            {error && <Alert options={{ autoClose: false, type: 'error' }}>
              <ListErrors {...error} />
              </Alert>
            }
            { items && 
              <div className="row pt-3">
                <div className="col-7 col-md-9 pr-0">
                  {!isCategoriesPath && 
                    <form onSubmit={this.onSearch}>
                      <input 
                        type="text"
                        name="searchTerm"
                        value={searchTerm}
                        placeholder={`Search by ${searchPropsText.join(', ')}`}
                        onChange={this.handleChange}
                      />
                      <input type="submit" className="d-none" value=""/>
                      {!shouldDisableBtn &&
                        <button
                          onClick={this.clearSearch}
                          className="ion-close pl-1 pr-1 pointer" style={clearBtnStyle}>
                        </button>
                      }
                    </form>
                  }
                </div>
                <div className="col-5 col-md-3 p-0">
                  {!isCategoriesPath && 
                    <button style={btnStyle}
                      className="btn ion-search ml-2 mr-2"
                      disabled={shouldDisableBtn}
                      onClick={this.onSearch}>
                    </button>
                  }
                  {!isCategoriesPath && this.renderFilterDropDown()}
                  {user.role === 'admin' &&  mapping.canCreate && 
                    <button style={btnStyle}
                      className={classnames("btn ion-plus ml-2 mr-2 float-right",  { 'mb-3': !!isCategoriesPath })} 
                      onClick={this.showCreateModal}>
                    </button>
                  }
                </div>
              </div>
            }
            <AdminCreateModal 
              isOpen={this.state.createModalOpen} 
              onModalClose={this.onModalClose} 
              title={mapping.titleSingular}
              item={this.state.selected}
            />
            {isCategoriesPath ?
              <ReactTable
                data={items}
                filterable
                defaultPageSize={count}
                minRows={count + 1}
                pageSize={count}
                columns={columns}
                defaultFilterMethod={this.filterCaseInsensitive}
              /> :
              <ReactTable
                showPaginationTop
                showPaginationBottom
                manual
                loading={isLoading}
                data={items}
                defaultPageSize={config.adminPerPage}
                // pageSize={perPage}
                pages={pages}
                minRows={count + 1}
                className="-striped -highlight"
                columns={columns}
                onFetchData={(state, instance) => {
                  console.log('sssss', state)
                  // TODO: prevent re-fetching on click of page again
                  // if (!field || field.page !== state.page + 1) {
                    let query = {page: state.page + 1}; // library default's page to 0
                    if (state.sorted && state.sorted.length) {
                      query.sort = state.sorted[0].id,
                      query.direction = state.sorted[0].desc ? 'desc' : 'asc'
                    }
  
                    console.log('pppppppppp', perPage, state.pageSize)
                    if (perPage && 
                      // (state.pageSize !== state.defaultPageSize && state.pageSize !== perPage)
                      (state.pageSize !== state.defaultPageSize || perPage !== state.defaultPageSize)
                    ) 
                    {
                      query.limit = state.pageSize
                    }

                    if (filterField) {
                      query = { ...query, ...filterField.value }
                    }
  
                    if (state.filtered && state.filtered.length) {
                      // query.sort = state.sorted[0].id,
                      // query.direction = state.sorted[0].desc ? 'DESC' : 'ASC'
                    }
                    this.tableMapping[this.path].fetch(query)
                    this.setState({ tableState: state })
                }}
              />
            }
          </React.Fragment>
      </DocumentTitle>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.shared.currentUser,
  users: state.admin.users,
  brands: state.admin.brands,
  categories: state.admin.categories,
  products: state.admin.products,
  orders: state.admin.orders,
  reports: state.admin.reports,
  payouts: state.admin.payouts,
  isLoading: state.admin.isLoading,
  error: state.admin.error,
})

const mapDispatchToProps = dispatch => ({
  getBrands: (query) => dispatch(getBrands(query)), 
  getCategories: (query) => dispatch(getCategories(query)), 
  deleteCategory: (id) => dispatch(deleteCategory(id)),
  deleteBrand: (id) => dispatch(deleteBrand(id)),
  getUsers: (query) => dispatch(getUsers(query)),
  updateUserRole: (id, role) => dispatch(updateUserRole(id, role)),
  getProducts: (query) => dispatch(getProducts(query)),
  getOrders: (query) => dispatch(getOrders(query)),
  getReports: (query) => dispatch(getReports(query)),
  getPayouts: (query) => dispatch(getPayouts(query)),
})

export default connect(mapStateToProps, mapDispatchToProps)(AdminTable)