import React from 'react';
import { connect } from 'react-redux';
import Route from 'react-router-dom/Route';
import Redirect from 'react-router-dom/Redirect';
import Switch from 'react-router-dom/Switch';
import Link from 'react-router-dom/Link';
import Categories from '../categories';
import Home from '../home';
import Generic from '../generic';
import Profile from '../profile';
import Chat from '../chat';
import PrivateRoute from '../private-route';
import Spinner from '../spinner';

export default connect((state) => ({
  categories: state.shared.categories,
  currentUser: state.shared.currentUser,
  trendingProducts: state.home.trendingProducts,
  trendingProductsLoading: state.home.trendingProductsLoading,
})
)(({ currentUser, ...props }) => (
  <div className="row">
    {/* desktop side menu */}
    { window.location.pathname.indexOf('catalog') === -1 && 
      <div className="sticky-top col-md-3 col-lg-2 d-none d-md-block d-lg-block pr-0 pl-0 text-left pt-2" id="sidemenu">
        {currentUser &&
          <React.Fragment>
            <div className="btn-block welcome-btn mb-3 pt-2 pb-2 text-center rounded ellipsis-text">
              Hello, {currentUser.username}
            </div>
            <ul className="p-3 ml-0 rounded" style={{ backgroundColor: '#fff' }}>
              <Link to="/messages" className="list-group-item border-0 p-1">
                <i className="ion-chatbubble-working mr-2"></i>
                Messages
              </Link>
              <Link to="/customer/history" className="list-group-item border-0 p-1">
                <i className="ion-pricetags mr-2"></i> Transactions
              </Link>
              <Link to="/customer/bookmarks" className="list-group-item border-0 p-1">
                <i className="ion-bookmark mr-2 p-1"></i> Bookmarks
              </Link>
            </ul>
          </React.Fragment>
        }
        {props.trendingProductsLoading && <Spinner size={5}/>}
        {props.trendingProducts &&
          <div className="pl-4 pr-2 mt-4">
            <p className="text-muted capitalize font-weight-bold">TRENDING</p>
            {props.trendingProducts.items.slice(0, 8).map(x => 
              <p className="mb-1 trending__prod" key={x.id}>
                <Link to={'/i/' + x.slug}>
                  <i className="ion-arrow-graph-up-right mr-1 sky-blue"></i> {x.name}
                </Link>
              </p>
            )}
          </div>
        }
      </div>
    }
    {/* desktop side menu end*/}

    <div className="col-sm">
      <Switch>
        <Route exact path="/" component={Home} />
        <PrivateRoute exact path="/messages" component={Chat} />
        <Route exact path="/i/:slug" component={Generic} />
        <Route exact path="/catalog/:slug" component={Generic} />
        <Route exact path="/user/:id" component={Profile} />
        <Redirect from="*" to="/" />
      </Switch>
    </div>
  </div>
))
