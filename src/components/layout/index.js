import React, { Component } from 'react';
import { connect } from 'react-redux';
import PushMenu from 'react-burger-menu/lib/menus/push';
import Header from '../header';
import Route from 'react-router-dom/Route';
import NavLink from 'react-router-dom/NavLink';
import Switch from 'react-router-dom/Switch';
import PrivateRoute from '../private-route';
import Categories from '../categories';
import Backend from '../backend';
import Main from './main';
import Cart from '../cart';
import NotificationMenu from './notification';
import categoryActions from '../../actions/categories';
import ChatSetup from '../chat/setup';
import routes from '../backend/routes';
import './style.css';

const Fragment = React.Fragment;

const Product2 = ({ hit }) => {
  return <div>{hit.name}</div>;
}

const mapStateToProps = (state) => ({
  categories: state.shared.categories
});

const mapDispatchToProps = (dispatch) => ({
  getCategories: () => dispatch(categoryActions.getCategories())
});

const menuStyles = {
  bmMenu: {
    background: '#fff',
    fontSize: '.8rem',
    textAlign: 'left'
  }
}

class Layout extends Component {
  constructor(props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: true,
      notificationOpen: false
    };
  }

  componentDidMount() {
    this.props.getCategories();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { pathname } = nextProps.history.location;
    Layout.updateClass(pathname)
    return null;
  }

  static updateClass(pathname) {
    const mainDiv = document.getElementById('main-content');
    if (!mainDiv) return;

    if (pathname.includes('/customer/') || pathname.includes('/admin/')) {
      if (!mainDiv.classList.contains('container')) return;

      mainDiv.classList.remove('container')
      mainDiv.classList.add('container-fluid')

    } else {
      if (!mainDiv.classList.contains('container-fluid')) return;

      mainDiv.classList.remove('container-fluid')
      mainDiv.classList.add('container')
    }
  }

  toggleNavbar(e) {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  renderSidebarContent() {
    const { location: { pathname }, categories } = this.props;
    if (pathname.indexOf('/customer') !== 0 && pathname.indexOf('/admin') !== 0) {
      return (
        <div className="list-group">
          {categories ? <Categories categories={this.props.categories} /> : 'getting categories...'}
        </div>
      )
    }

    const pathRoute = (pathname.indexOf('/admin') === 0 ? routes.adminRoutes : routes.userRoutes)
    const links = pathRoute.filter(route => route.path.indexOf('/:') === -1 && !route.redirect)
    const path = pathname.split('/')[1] || ''
    return (
      <ul className="list-group mt-2 border-0 ml-0 p-2">
        {links.map((route, key) => (
          <NavLink to={`/${path}${route.path}`} key={key} className="list-group-item">{route.name}</NavLink>
        ))}
      </ul>
    )
  }

  componentDidUpdate(prevProps) {
    if (prevProps.location.pathname !== this.props.location.pathname && !this.state.collapsed) {
      this.setState({ collapsed: true })
    }
  }

  render() {
    return (
      <Fragment>
        <div id="layout-container">
          <Header toggleNavbar={this.toggleNavbar} />
          <NotificationMenu />
          <ChatSetup />


          {/* mobile side menu */}
          <PushMenu styles={menuStyles} isOpen={!this.state.collapsed}
            customBurgerIcon={false} customCrossIcon={false}
            pageWrapId={"main-content"} outerContainerId={"layout-container"} >
            { this.renderSidebarContent() }
          </PushMenu>
          {/* mobile side menu end*/}

          <div id="main-content" className="container">
            {/* <Hits hitComponent={Product2} /> */}
            <Switch>
              <PrivateRoute path='/customer' component={Backend} />
              <PrivateRoute path='/admin' component={Backend} admin />
              <PrivateRoute exact path="/cart" component={Cart} />
              <Route path="/" component={Main} />
            </Switch>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
