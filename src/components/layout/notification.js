import React from 'react';
import { connect } from 'react-redux';
import Link from 'react-router-dom/Link';
import SlideMenu from 'react-burger-menu/lib/menus/slide';
import headerActions from '../../actions/header';
import 'moment-timezone';
import moment from 'moment';
import classnames from 'classnames';

moment.updateLocale('en', {
  relativeTime : {
      future: "in %s",
      past:   "%s ago",
      s  : '%ds',
      ss : '%ds',
      m:  "%dm",
      mm: "%dm",
      h:  "%dh",
      hh: "%dh",
      d:  "%dd",
      dd: "%dd",
      M:  "%dmonth",
      MM: "%dmonths",
      y:  "a year",
      yy: "%d years"
  }
});

const notificationSlideStyle = {
  bmMenuWrap: {
    background: '#eeeeee',
    fontSize: '.8rem',
    textAlign: 'left',
    boxShadow: 'rgb(125, 116, 116) 2px 1px 10px 0px',
    padding: '10px 0',
    overflow: 'hidden',
    zIndex: '1000'
  },
  bmOverlay: {
    // top: 0,
    // zIndex: 1050
  }
}

const getNotificationText = (n) => {
	let action;
	if (n.action === 'bid') action = 'placed a new bid on ';
	if (n.action === 'purchase') action = 'purchased ';
  if (n.action === 'comment') action = 'commented on ';
  if (n.action === 'follow') action = 'followed ';
  return (<React.Fragment>
    <Link to={'/seller/' + n.data.url} className="font-weight-bold">
      {n.actor}
    </Link> {action} {n.action === 'follow' ? 'You' : 'your product'}
  </React.Fragment>
  )
}

export default connect((state) => ({
  isOpen: state.shared.notificationMenuOpen,
  notifications: state.shared.notifications
}), (dispatch) => ({
  onNotificationMenuChange: (menuState) => {
    // useful when backdrop is clicked instead of toggler
    if (!menuState.isOpen) {
      dispatch(headerActions.hideNotificationMenu())
    }
  }
})) (({isOpen, notifications, onNotificationMenuChange}) => (
  <SlideMenu isOpen={isOpen} right
    styles={notificationSlideStyle}
    onStateChange={onNotificationMenuChange}
    customBurgerIcon={false} customCrossIcon={false}
  >
    {notifications ? 
      <ul className="list-group ml-0">
        {notifications.map((n, i) => (
          <li className={classnames('list-group-item rounded-0 pl-1 mb-1 notification-item', {
            'notification-unread': !n.isRead
          })} key={i}>
            <img 
              src="https://previews.123rf.com/images/aerial3/aerial31410/aerial3141000005/32770824-vector-icon-of-user-avatar-for-web-site-or-mobile-app-man-face-in-flat-style-for-social-network-prof.jpg" alt="" className="img-thumbnail img-fluid"/>
          {getNotificationText(n)}
          <span className="n-timestamp">
            {moment(n.createdAt).fromNow(true)}
          </span>
          </li>
        ))}
      </ul> : 
      <div>
        <p className="text-center">No notification</p>
      </div>
    }
  </SlideMenu>
))