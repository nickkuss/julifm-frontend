import React from 'react';
import { connect } from 'react-redux'
import Route from 'react-router-dom/Route';
import Redirect from 'react-router-dom/Redirect';
import Spinner from './spinner';

export default connect((state) => ({
  user: state.shared.currentUser,
  loading: state.shared.loadingUser
}))(({ component: Component, user, loading, admin, ...rest }) => (
  <Route {...rest} render={(props) => {
    if (user) {
      if (!admin || (admin && (user.role === 'admin' || user.role === 'editor') )) {
        return <Component {...props} />
      }
    }
    if (loading) return <Spinner />
    return <Redirect to={{
      pathname: '/login',
      state: { referrer: window.location.pathname }
    }}/>
  }} />
))