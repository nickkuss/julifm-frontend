import React, { Component } from 'react';
import Link from 'react-router-dom/Link';
import { connect } from 'react-redux';
import DocumentTitle from 'react-document-title';
import TabContent from 'reactstrap/lib/TabContent';
import TabPane from 'reactstrap/lib/TabPane';
import classnames from 'classnames';
import authActions from '../../actions/auth';
import ErrorList from '../errors';
import { push } from 'react-router-redux';
import config from '../../config';
import storage from '../../utils/storage';

import './style.css';
import spinner from '../../shared/spinner.svg';
import logo from '../../assets/logo.png'
import { pageTitleText } from '../../utils/app';

const Fragment = React.Fragment;

const mapStateToProps = state => ({ 
	...state.auth,
	router: state.router,
	isLoading: state.shared.isLoading
});

const mapDispatchToProps = dispatch => ({
  loginSubmit: (data) => dispatch(authActions.login(data)),
  fbLogin: (data) => dispatch(authActions.fbLogin(data)),
	registerSubmit: (data) => dispatch(authActions.register(data)),
  redirect: (path) => dispatch(push(path)),
  onUnload: () => dispatch(authActions.onUnload()),
});

class Auth extends Component {

	constructor(props) {
    super(props);

		this.state = {form: { 
			terms: true
		}};
		this.isFormInvalid = this.isFormInvalid.bind(this);
		this.handleInputChange = this.handleInputChange.bind(this);
		this.submitForm = this.submitForm.bind(this);
		this.fbAuth = this.fbAuth.bind(this);
  }

	static getDerivedStateFromProps(nextProps, prevState) {
		const path = nextProps.match.path.slice(1)
		if (path === prevState.activeTab) return null;

		return {activeTab: path}
	}

	componentDidMount() {
		// TODO: use react-router-redux to redirect
		if (this.props.token) {
			// redirect if already logged in
			this.props.history.push('/');
			return null
		}
		this.loadFBAsync()
		
	}

	componentDidUpdate(prevProps, prevState) {
		if (!prevProps.token && this.props.token) {
			// we just authenticated
			storage.setToken(this.props.token)
			const url = this.props.location.state ? this.props.location.state.referrer : '/'
			this.props.history.push(url);
		}
	
	}

	loadFBAsync() {
		window.fbAsyncInit = function() {
			window.FB.init({
				appId      : config.FBAppId,
				cookie     : true,
				xfbml      : true,
				version    : 'v2.12'
			});

			window.FB.getLoginStatus(function(response) {
				if (response.status === 'connected') {
						console.log('Logged in.');
				}
				else {
						console.log('else');
						// FB.login(result => {
						//     console.log('successfully logged in', result);
						// }, function(e) {
						//     console.log('errror', e);
						// });
				}
				});
				
			window.FB.AppEvents.logPageView();   
				
		};
	
		(function(d, s, id){
			 var js, fjs = d.getElementsByTagName(s)[0];
			 if (d.getElementById(id)) {return;}
			 js = d.createElement(s); js.id = id;
			 js.src = "https://connect.facebook.net/en_US/sdk.js";
			 fjs.parentNode.insertBefore(js, fjs);
		 }(document, 'script', 'facebook-jssdk'));
	}

	generateTabs(path) {
		return (
			<div className="nav-tabs">
				<div className={classnames('nav-item', {'no-click': path === '/login'})}>
					<Link to="/login" className={classnames('nav-link', { active: path === '/login' })} >
						<i className="ion-android-exit mr-2"></i>
						Login
					</Link>
				</div>
				<div className={classnames('nav-item', {'no-click': path === '/register'})}>
					<Link to="/register" className={classnames('nav-link', { active: path === '/register' })} >
						<i className="ion-person-add mr-2"></i>
						Sign Up
					</Link>
				</div>
			</div>
		);
	}

	generateField(type, name, label, attributes) {
		return (
			<div className="form-group">
				{/* <label htmlFor={name}>{label}</label> */}
				<input type={type} name={name} placeholder={label} className="form-control mb-0" id={name} {...attributes}
					value={this.state.form[name] || ''} onChange={this.handleInputChange} required/>
				{this.state.formErrors && this.state.formErrors[name] && 
					(<div className="invalid-feedback">
          	{this.state.formErrors[name]}
					</div>)
				}
			</div>
		)
	}

	generateForm() {
		const url = this.props.match.path;
		return (
			<form noValidate  onSubmit={this.submitForm} 
				className={classnames('text-left', {'was-validated': this.state.formValidated})}>
				{ url === '/register' && 
					<Fragment>
						{ this.generateField('text', 'fullName', 'Full Name') }
						{ this.generateField('text', 'username', 'Username') }
						{ this.generateField('number', 'phone', 'Phone') }
					</Fragment>
				}

				{ this.generateField('email', 'email', 'Email', {pattern: '[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}'}) }
				{ this.generateField('password', 'password', 'Password', {minLength: '6'}) }
				{ url === '/register' && this.generateField('password', 'confirmPassword', 'Confirm Password', { minLength: '6', pattern: this.state.form.password }) }
			
				<div className={classnames({ 'form-check': url === '/register' })}>
					{
						url === '/register' ? (
							<Fragment>
								{/* <label className="form-check-label" htmlFor="terms">
								<input type="checkbox" className="form-check-input radio-default" id="terms" name="terms" onChange={this.handleInputChange} required checked={this.state.form.terms}/>
									I accept the <span className="link__display">Terms & Conditions</span>
								</label>
								{this.state.formErrors && this.state.formErrors.terms && 
									(<div className="invalid-feedback">
										{this.state.formErrors.terms}
									</div>)
								} */}
							</Fragment>
						) : (
							<Fragment>
								<span className="link__display float-right">Forgot Password?</span>
								<div className="clearfix"></div>			
							</Fragment>
						)
					}
				</div>
				<br/>
				<button type="submit" 
					className={classnames("btn btn-primary btn-block mt-2", {disabled: this.props.isLoading})}
					onClick={this.submitForm}>{url === '/login' ? 'Login' : 'Sign Up Now'}</button>
			</form>	
		);
	}

	handleInputChange(event) {
		const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
		const form = {...this.state.form, [name]: value}
    this.setState({form});
	}

	isFormInvalid(event) {
		const errors = {}
		const form = this.state.form
		let hasError = false;
		const path = this.props.match.path;

		const fields = path === '/login' ? ['email', 'password'] : ['email', 'password', 'full Name', 'username', 'phone'];

		fields.forEach(item => {
			const field = item.split(' ').join(''); // handle spaces in field names
			if (!form[field] || !form[field].trim()) {
				errors[field] = item.toLowerCase() + ' is required';
				hasError = true;
			}
		})

		if (form.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(form.email)) {
			errors.email = 'invalid email address';
			hasError = true;
		}

		if (form.password && form.password.trim() && form.password.trim().length < 6) {
			errors.password = 'password should be 6 characters or more';
			hasError = true;
		}

		if (path === '/register') {
			if (!form.confirmPassword) {
				errors.confirmPassword = 'enter valid password'
				hasError = true
			} else if (form.confirmPassword !== form.password) {
				errors.confirmPassword = 'passwords do not match'
				hasError = true
			}

			if (form.username && !/^[a-zA-Z0-9]+$/i.test(form.username)) {
				errors.username = 'invalid username';
				hasError = true;
			}
			
			if (isNaN(Number(form.phone))) {
				errors.phone = 'invalid phone number';
				hasError = true;
			} 
	
			if (!form.terms) {
				errors.terms = 'You need to accept the terms & conditions';
				hasError = true;			
			}
		}
		return (hasError ? errors : false);
	}

	submitForm(event) {
		event.preventDefault();
		
		const formErrors = this.isFormInvalid();
		this.setState({formValidated: true});
		this.setState({formErrors})

		if (formErrors) return false;
		
		console.log('form valid');
		const action = this.props.match.path === '/login' ?
			this.props.loginSubmit : this.props.registerSubmit;
		action(this.state.form);
	}

	fbAuth() {
		if (window.FB) {
			window.FB.login(result => {
				if (!result.authResponse) return; // user clicked cancel

				console.log('successfully logged in', result);
				this.props.fbLogin(result.authResponse.accessToken)
			}, function(e) {
					console.log('errror', e);
			});
		}
	}

	render() {
		const path = this.props.match.path;
		return (
			<DocumentTitle title={pageTitleText(path === '/login' ? 'Login' : 'Register')}>
				<div className="container pt-4 auth-view">
					{this.props.isLoading &&
						<div id="spinner-holder">
							<img src={spinner} alt="loading indicator" id="spinner" />
						</div>	
					}
					<div className={classnames('row', {loading: this.props.isLoading})}>
						<div className="col-10 col-sm-9 col-md-6 col-lg-4 mr-auto ml-auto pr-0 pl-0">
							<div className="header-logo mb-3 mr-auto ml-auto">
								<Link to="/">
									<img src={logo} className="logo img-fluid"/>
								</Link>
							</div>
							
							{ this.generateTabs(path) }

							<div className="card p-3">
								<div className="social-auth mb-2">
									<a className="btn btn-block mb-1" id="fb-auth" onClick={this.fbAuth}>
										<i className="ion-social-facebook float-left"></i>
										{path === '/login' ? 'Login' : 'Register'} With Facebook 
									</a>
									{/* <small><p className="mb-4">By using Facebook, you accept the <span className="link__display">Terms & Conditions</span></p></small> */}
									<hr/>
									<div id="or__divider">OR</div>
									{this.props.errors && (
										<ErrorList errors={this.props.errors}/>
									)}
								</div>
								<TabContent activeTab={this.state.activeTab} className="f4hjk">
									<TabPane tabId="login"> { path === '/login' && this.generateForm() } </TabPane>
									<TabPane tabId="register"> { path === '/register'&&  this.generateForm() } </TabPane>
								</TabContent>
							</div>
						</div>
					</div>
				</div>
			</DocumentTitle>
		);
	}

	componentWillUnmount() {
    this.props.onUnload();
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);