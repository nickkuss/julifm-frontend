import {
  SET_CHAT_USER,
  SET_ROOM,
  ON_RECEIVE_MESSAGE,
  SEND_MESSAGE,
  SEND_MESSAGE_ERROR,
  UPDATE_CHAT_ROOM
} from '../constants';

export const setChatUser = (data) => {
  return {
    type: SET_CHAT_USER,
    payload: data
  };
};

export const setRoom = (room, user) => {
  return {
    type: SET_ROOM,
    payload: { room, user }
  };
};

export const onSendChat = (data) => {
  return {
    type: SEND_MESSAGE,
    payload: data
  };
};

export const onSendChatError = (data) => {
  return {
    type: SEND_MESSAGE_ERROR,
    payload: data
  };
};

export const onReceiveMessage = (data) => {
  return {
    type: ON_RECEIVE_MESSAGE,
    payload: data
  };
};

export const updateChatRoom = (data) => {
  return {
    type: UPDATE_CHAT_ROOM,
    payload: data
  };
};
