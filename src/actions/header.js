import api from '../api';
import { GET_USER_NOTIFICATIONS, TOGGLE_NOTIFICATION_MENU,
	HIDE_NOTIFICATION_MENU, ON_NEW_NOTIFICATION, LOGOUT } from '../constants';

const getNotifications = () => {
	return {
		type: GET_USER_NOTIFICATIONS,
		payload: api.user.notifications()
	}
}

const onNewNotification = (data) => ({ type: ON_NEW_NOTIFICATION, payload: data })

const logout = () => ({ type: LOGOUT })

const toggleNotificationMenu = () => ({ type: TOGGLE_NOTIFICATION_MENU })

const hideNotificationMenu = () => ({ type: HIDE_NOTIFICATION_MENU })

export default { getNotifications, onNewNotification, toggleNotificationMenu, hideNotificationMenu, logout }