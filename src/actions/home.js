import api from '../api';
import { FETCH_FEED, FETCH_FEATURED, FETCH_TRENDING, HOME_PAGE_UNLOADED } from '../constants';

const getFeed = () => ({
	type: FETCH_FEED,
	payload: api.user.feed()
})

const getFeatured = () => ({
	type: FETCH_FEATURED,
	payload: api.product.get({isFeatured: true}),
})

const getTrending = () => ({
	type: FETCH_TRENDING,
	payload: api.product.get({isTrending: true}),

})

const onUnload = () => ({ type:  HOME_PAGE_UNLOADED})

export default { getFeed, getFeatured, getTrending,  onUnload }