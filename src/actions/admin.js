import api from '../api';
import * as types from '../constants'

export const addCategory = (data) => ({ type: types.ADD_CATEGORY, payload: api.admin.addCategory(data) })
export const editCategory = (id, data) => ({ type: types.EDIT_CATEGORY, payload: api.admin.editCategory(id, data) })
export const addBrand = (data) => ({ type: types.ADD_BRAND, payload: api.admin.addBrand(data) })
export const editBrand = (id, data) => ({ type: types.EDIT_BRAND, payload: api.admin.editBrand(id, data) })

export default { addCategory, editCategory, addBrand, editBrand }