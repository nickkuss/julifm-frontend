import api from '../api';
import {
	FETCH_CATEGORY_PRODUCTS,
	FETCH_PRODUCT_DETAIL,
	SUBMIT_BID,
	SHARE_PRODUCT,
	BOOKMARK_PRODUCT,
	UNBOOKMARK_PRODUCT,
	DELETE_PRODUCT,
	UPDATE_PRODUCT_WITH_BID,
	PRODUCT_PAGE_UNLOADED,
  GENERIC_PAGE_UNLOADED,
	FETCH_USER_PRODUCTS,
	ADD_NEW_PRODUCT,
	GET_REVIEWS,
	ADD_REVIEW,
	PRODUCT_FORM_UNLOADED
} from '../constants';

const getProductsForCategory = (category) => {
	return {
		type: FETCH_CATEGORY_PRODUCTS,
		payload: api.product.getByCategory({category})
	}
}

const getProductsForCategoryInfinite = (category, page) => {
	return api.product.getByCategory({category, page})
}

const getProductDetail = (slug) => {
	return {
		type: FETCH_PRODUCT_DETAIL,
		payload: api.product.getBySlug(slug)
	}
}

const getProductBySlug = (slug) => {
	return api.product.getBySlug(slug)
}

const getProductsForUser = (userId, pagination = {}) => {
	console.log(userId);
	return {
		type: FETCH_USER_PRODUCTS,
		// payload: api.product.get()
		payload: api.product.get({ owner: userId, ...pagination })
	}
}

const submitBid = (id, amount) => {
	return { 
		type: SUBMIT_BID,
		payload: api.product.submitBid(id, amount) 
	}
}

const updateWithNewBid = (data) => {
	return { type: UPDATE_PRODUCT_WITH_BID, payload: data }
}

const addNewProduct = (product) => {
	return {
		type: ADD_NEW_PRODUCT,
		payload: api.product.addNew(product)
	}
}

const shareProduct = (id) => {
	return {
		type: SHARE_PRODUCT,
		payload: api.product.shareProduct(id)
	}
}

const addBookmark = (id) => {
	return {
		type: BOOKMARK_PRODUCT,
		payload: api.product.addBookmark(id)
	}
}

const removeBookmark = (id) => {
	return {
		type: UNBOOKMARK_PRODUCT,
		payload: api.product.removeBookmark(id)
	}
}

const deleteProduct = (id) => ({
	type: DELETE_PRODUCT,
	payload: api.product.deleteProduct(id)
})

const getReviews = (id) => ({ type: GET_REVIEWS, payload: api.product.getReviews(id) })

const addReview = (id, data) => ({ type: ADD_REVIEW, payload: api.product.postReview(id, data) })

const onUnload = () => ({type: PRODUCT_PAGE_UNLOADED})

const onFormUnload = () => ({type: PRODUCT_FORM_UNLOADED})

const onGenericPageUnload = () => ({type: GENERIC_PAGE_UNLOADED})

export default {
	getProductsForCategory,
	getProductsForCategoryInfinite,
	getProductDetail,
	getProductBySlug,
	submitBid,
	getReviews,
	addReview,
	updateWithNewBid,
	getProductsForUser,
	addNewProduct,
	deleteProduct,
	onUnload,
	onFormUnload,
	onGenericPageUnload,
	shareProduct,
	addBookmark,
	removeBookmark
}