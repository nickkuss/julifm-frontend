import api from '../api';
import { LOGIN, FB_LOGIN, REGISTER, AUTH_PAGE_UNLOADED, SET_CURRENT_USER } from '../constants';

const login = (credentials) => {
	return { 
		type: LOGIN, 
		payload: api.auth.login(credentials) 
	}
}

const fbLogin = (credentials) => ({ 
	type: FB_LOGIN, 
	payload: api.auth.fbLogin(credentials) 
})

const register = (data) => {
	return { 
		type: REGISTER, 
		payload: api.auth.register(data) 
	}
}

const setUser = (user) => {
	return {
		type: SET_CURRENT_USER,
		payload: user
	}
}

const onUnload = () => ({ type:  AUTH_PAGE_UNLOADED})

export default { login, fbLogin, register, onUnload, setUser }