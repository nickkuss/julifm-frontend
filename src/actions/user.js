import api from '../api';
import { GET_CURRENT_USER, GET_USER_BOOKMARKS, UPDATE_USER_DATA, PROFILE_PAGE_UNLOADED, GET_TOP_UPS, TOP_UP_WALLET, REQUEST_PAYOUT, GET_PAYOUTS } from '../constants';

const getUser = () => {
	return { 
		type: GET_CURRENT_USER, 
		payload: api.user.get()
	}
}

const updateProfile = (data) => {
	return { 
		type: UPDATE_USER_DATA, 
		payload: api.user.update(data)
	}
}

const updatePassword = (data) => {
	return { 
		type: UPDATE_USER_DATA, 
		payload: api.user.updatePassword(data)
	}
}

const updatePhoto = (data) => ({
	type: UPDATE_USER_DATA,
	payload: api.user.updatePhoto(data)
})

const topUp = (data) => ({
	type: TOP_UP_WALLET, 
	payload: api.user.topUp(data) 
})
	
const getTopUpHistory = (query) => ({
	type: GET_TOP_UPS, 
	payload: api.user.topUps(query)
})

const requestPayout = () => ({
	type: REQUEST_PAYOUT, 
	payload: api.user.payout() 
})
	
const getPayouts = (query) => ({
	type: GET_PAYOUTS, 
	payload: api.user.payouts(query)
})
	
const changePassword = (user) => {
	return {
		type: UPDATE_USER_DATA,
		payload: user
	}
}

const getBookmarks = () => ({
	type: GET_USER_BOOKMARKS,
	payload: api.user.bookmarks()
})

const getOrdersByMe = (page=1) => api.user.getOrders({ page })
	
const getOrdersForMe = (page=1) => api.user.getOrders({tab: 'seller', page})

const updateOrder = (id, data) => api.user.updateOrder(id, data)
	
const onUnload = () => ({ type:  PROFILE_PAGE_UNLOADED})

export default { 
	getUser, updateProfile, updatePassword, topUp, changePassword, onUnload,
	getBookmarks, getOrdersByMe, getOrdersForMe, getTopUpHistory, updateOrder,
	updatePhoto, requestPayout, getPayouts, 
}