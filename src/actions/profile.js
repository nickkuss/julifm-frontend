import api from '../api';
import { FOLLOW_USER, UNFOLLOW_USER, FETCH_SELLER_INFO, SELLER_PAGE_UNLOADED } from '../constants';

const getSellerInfo = (username) => {
	return { 
		type: FETCH_SELLER_INFO, 
		payload: api.user.getByUsername(username)
	}
}

const getSellerInfoInfinite = (username, query) => {
	return api.user.getByUsername(username, query)
}

const followUser = (userId) => {
	return { 
		type: FOLLOW_USER, 
		payload: api.user.follow(userId)
	}
}

const unFollowUser = (userId) => {
	return { 
		type: UNFOLLOW_USER, 
		payload: api.user.unfollow(userId)
	}
}

const onSellerPageUnload = () => {
	return { 
		type: SELLER_PAGE_UNLOADED, 
	}
}

export default { getSellerInfo, getSellerInfoInfinite, followUser, unFollowUser, onSellerPageUnload }