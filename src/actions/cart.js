import api from '../api';
import storage from '../utils/storage';
import { GET_CART, UPDATE_CART, CLEAR_CART } from '../constants';

const getCart = () => {
  const cartId = storage.getCartId() || '';
  
	return { 
		type: GET_CART, 
		payload: api.user.getCart(cartId)
	}
}

const clearCart = () => ({ type: CLEAR_CART })


const updateCart = (data) => {
  const cartId = storage.getCartId() || '';
  
	return { 
		type: UPDATE_CART, 
		payload: api.product.updateCart(cartId, data)
	}
}


export default { getCart, updateCart, clearCart }
