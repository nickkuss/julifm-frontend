import api from '../api';
import { GET_PROVINCES, GET_CITIES, GET_SUBDISTRICT, GET_COURIERS } from '../constants';

const getCouriers = () => {  
	return { 
		type: GET_COURIERS, 
		payload: api.location.getCouriers()
	}
}

const getProvinces = () => {  
	return { 
		type: GET_PROVINCES, 
		payload: api.location.getProvinces()
	}
}

const getCities = (province) => {  
	return { 
		type: GET_CITIES, 
		payload: api.location.getCities(province)
	}
}

const getSubdistrict = (cityId) => {  
	return { 
		type: GET_SUBDISTRICT, 
		payload: api.location.getSubdistrict(cityId)
	}
}

export default { getCouriers, getProvinces, getCities }


