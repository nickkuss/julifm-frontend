import api from '../api';
import { FETCH_CATEGORIES } from '../constants';
import categoryUtils from '../utils/categories';

const getCategories = () => {
	return {
		type: FETCH_CATEGORIES,
		payload: api.categories.get()
	}
}

export default { getCategories }