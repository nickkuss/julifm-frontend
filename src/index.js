import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';

import { store, history} from './store';

import WebFont from 'webfontloader';
import registerServiceWorker from './registerServiceWorker';
import { validateRequiredConfig } from './utils/app';


import 'bootstrap/dist/css/bootstrap.min.css';
import 'antd/dist/antd.css';
import './shared/style.css';

import App from './components/app';

validateRequiredConfig();

WebFont.load({
  google: {
    families: ['Roboto:400,500,700', 'sans-serif']
  }
});

ReactDOM.render((
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>
  
), document.getElementById('root'));

registerServiceWorker();
