import {
  SET_CHAT_USER,
  SET_ROOM,
  IS_TYPING,
  IS_NOT_TYPING,
  ON_RECEIVE_MESSAGE,
  SEND_MESSAGE,
  UPDATE_CHAT_ROOM,
  SEND_MESSAGE_ERROR
} from '../constants';

const initialState = {
  loadEarlier: false,
  isLoadingEarlier: false,
  user: null,
  hasNewMessage: false,
  newMessageCount: 0,
  token: null,
  messages: {},
  isTyping: false,
  error: null,
  currentRoom: null,
};

const ChatReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_CHAT_USER:
      return {...state, user: payload};
    case SET_ROOM:
      return {...state, currentRoom: payload.room, currentRoomUser: payload.user, hasNewMessage: false, newMessageCount: 0};
    case IS_TYPING:
      return {...state, typing: true};
    case IS_NOT_TYPING:
      return {...state, typing: false};
    case ON_RECEIVE_MESSAGE:
      return {
        ...state,
        hasNewMessage: payload.roomId === state.currentRoom && state.user.id !== payload.user.id,
        newMessageCount: state.currentRoom ? 0 : state.newMessageCount + 1,
        messages: {
          ...state.messages, 
          [payload.roomId]: state.messages[payload.roomId] ?
            [...state.messages[payload.roomId], payload] : [payload]
        }
      };
    case SEND_MESSAGE:
      return {
        ...state,
        error: null,
        messages: {
          ...state.messages,
          [state.currentRoom]: [...state.messages[state.currentRoom], payload]
        } 
      };
    case SEND_MESSAGE_ERROR:
      console.log('send message errossr', payload)
      return {...state, error: payload};
    case UPDATE_CHAT_ROOM:
      console.log('updating chaaa')
      return {
        ...state, 
        messages: {...state.messages, [state.currentRoom]: [...payload, ...state.messages[state.currentRoom] ]}
      };
    default:
      return state;
  }
};

export default ChatReducer;
