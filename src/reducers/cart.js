import { GET_CART, ASYNC_START, UPDATE_CART, CLEAR_CART,
  PRODUCT_PAGE_UNLOADED, CART_PAGE_UNLOADED } from '../constants'

const initialState = {
  cart: null,
  error: null,
  isLoading: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_CART:
      return {
        ...state,
        error: action.error ? action.payload : null,
        cart: !action.error ? action.payload : state.cart
      };
    case ASYNC_START:
      return {
        ...state,
        isLoading: action.subtype === UPDATE_CART
      };
    case UPDATE_CART:
      return {
        ...state,
        error: action.error ? action.payload : null,
        cart: !action.error ? action.payload : state.cart,
        isLoading: false
      };
    case PRODUCT_PAGE_UNLOADED:
    case CART_PAGE_UNLOADED:
      return {...state, error: null}
    case CLEAR_CART:
      return {...state, 
        cart: {...state.cart, items: [], 
          itemCount: 0,
          totalPrice: 0,
        } 
      }
    default:
      return state;
  }
};
