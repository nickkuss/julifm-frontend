import { ASYNC_START, FETCH_PRODUCT_DETAIL, FETCH_SELLER_INFO,
FOLLOW_USER, UNFOLLOW_USER, PRODUCT_PAGE_UNLOADED, SELLER_PAGE_UNLOADED, UPDATE_PRODUCT, ADD_NEW_PRODUCT
} from '../constants' 

const initialState = {
	isLoading: false,
	profile: null,
	error: null
};

export default (state = initialState, action) => {
	switch(action.type) {
		case FOLLOW_USER:
    case UNFOLLOW_USER:
      return {
        ...state,
				error: action.error ? action.payload : null,
				profile: state.profile && !action.error ? 
					{
						...state.profile,
						isFollowing: action.type === FOLLOW_USER
					} : state.profile,
        isLoading: false,
			};
		case ADD_NEW_PRODUCT:
		case UPDATE_PRODUCT:
		case FETCH_PRODUCT_DETAIL:
			return {
				...state,
				profile: !action.error ? action.payload.owner : state.profile,
			};
		case FETCH_SELLER_INFO:
			return {
				...state,
				error: action.error ? action.payload : null,
				profile: !action.error ? action.payload : state.profile,
			};
		case ASYNC_START:
			return {
				...state,
				updated: false,
        isLoading: action.subtype === FOLLOW_USER || action.subtype === UNFOLLOW_USER ? true : state.isLoading
			};
		case PRODUCT_PAGE_UNLOADED:
		case SELLER_PAGE_UNLOADED:
			return initialState;
		default:
      return state;
	}
};
