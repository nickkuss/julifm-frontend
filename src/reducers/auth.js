import { LOGIN, FB_LOGIN,  REGISTER, AUTH_PAGE_UNLOADED
} from '../constants' 

const initialState = {
	errors: null,
	token: null
};

export default (state = initialState, action) => {
	switch(action.type) {
		case LOGIN:
		case FB_LOGIN:
    case REGISTER:
      return {
        ...state,
				errors: action.error ? action.payload : null,
				token: !action.error ? action.payload.token : null
			};
		case AUTH_PAGE_UNLOADED:
			return {};
		default:
      return state;
	}
};
