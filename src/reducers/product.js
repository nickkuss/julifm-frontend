import { FETCH_PRODUCT_DETAIL, FETCH_USER_PRODUCTS, PRODUCT_PAGE_UNLOADED, GET_REVIEWS, ADD_REVIEW,
	PRODUCT_FORM_UNLOADED, SUBMIT_BID, UPDATE_PRODUCT_WITH_BID, DELETE_PRODUCT, SHARE_PRODUCT,
	ADD_NEW_PRODUCT, UPDATE_PRODUCT, ASYNC_START, BOOKMARK_PRODUCT, UNBOOKMARK_PRODUCT
} from '../constants'

const initialState = {
	product: null,
	savedProduct: null,
	userProducts: null,
	isLoading: false,
	reviews: null,
	fetchingReviews: false,
	error: null
};

export default (state = initialState, action) => {
	switch (action.type) {
		case FETCH_PRODUCT_DETAIL:
		case ADD_NEW_PRODUCT:
		case UPDATE_PRODUCT:
		case SUBMIT_BID:
			const hasProduct = [FETCH_PRODUCT_DETAIL, SUBMIT_BID, ADD_NEW_PRODUCT, UPDATE_PRODUCT].indexOf(action.type > -1);
			return {
				...state,
				error: action.error ? action.payload : null,
				product: !action.error && hasProduct ? action.payload : state.product,
				// savedProduct: (!action.error && (action.type === ADD_NEW_PRODUCT || action.type === UPDATE_PRODUCT))
				// 	? action.payload : null,
				isLoading: false
			};
		case FETCH_USER_PRODUCTS:
			return {
				...state,
				error: action.error ? action.payload : null,
				userProducts: !action.error ? action.payload : null
			};
		case GET_REVIEWS:
			return {
				...state,
				error: action.error ? action.payload : null,
				reviews: !action.error ? action.payload : null
			};
		case ADD_REVIEW:
			return {
				...state,
				error: action.error ? action.payload : null,
				fetchingReviews: false,
				reviews: !action.error ? {...state.reviews, canReview: false, hasReviewed: true, items: [action.payload, ...state.reviews.items] } : state.reviews
			};
		case BOOKMARK_PRODUCT:
			return {
				...state,
				bookmarkLoading: false,
				product: {...state.product, 
					isBookmarked: !action.error ? true : false  
				},
				error: action.error ? action.payload : null
			}
		case SHARE_PRODUCT:
			return {
				...state,
				shareLoading: false,
				product: {...state.product, 
					isShared: !action.error ? true : false  
				},
				error: action.error ? action.payload : null
			}
		case UNBOOKMARK_PRODUCT:
			return {
				...state,
				bookmarkLoading: false,
				product: {...state.product,
					isBookmarked: !action.error ? false : true
				},
				error: action.error ? action.payload : null
			}
		case DELETE_PRODUCT:
			if (!state.userProducts) return state;

			return {
				...state,
				isLoading: false,
				userProducts: action.error ? state.userProducts : {
					...state.userProducts,
					count: state.userProducts.count - 1,
					totalResults: state.userProducts.totalResults -1,
					items: state.userProducts.items.filter(item => item.id !== action.payload.id)
				},
				error: action.error ? action.payload : null
			}
		case UPDATE_PRODUCT_WITH_BID:
			return {
				...state,
				product: {...state.product, 
					auction: {
						...state.product.auction,
						currentAmount: action.payload.currentAmount,
						bidCount: action.payload.bidCount,
						minBid: action.payload.minBid
					} 
				}
			};
		case ASYNC_START:
			let data = { ...state, 
				isLoading: action.subtype === ADD_NEW_PRODUCT || action.subtype === SUBMIT_BID || action.subtype === DELETE_PRODUCT 
			};
			if (action.subtype === BOOKMARK_PRODUCT || action.subtype === UNBOOKMARK_PRODUCT) {
				data.bookmarkLoading = true;
			}

			if (action.subtype === SHARE_PRODUCT) {
				data.shareLoading = true;
			}

			if (action.subtype === ADD_REVIEW) data.fetchingReviews = true
			return data;
		case PRODUCT_FORM_UNLOADED:
			return {...initialState, product: state.product}
		case PRODUCT_PAGE_UNLOADED:
			return initialState;
		default:
			return state;
	}
};