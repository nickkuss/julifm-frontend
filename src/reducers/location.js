import { ASYNC_START, GET_PROVINCES, GET_CITIES, GET_SUBDISTRICT, GET_COURIERS} from '../constants';

const initialState = {
	isLoading: false,
	provinces: null,
	cities: null,
	couriers: null,
  error: null,
	subdistricts: null
};

export default (state = initialState, action) => {
	switch(action.type) {
		case GET_PROVINCES:
      return {...state, 
        isLoading: false,
        error: action.error ? action.payload : null,
				provinces: !action.error ? action.payload : state.provinces
			}
		case GET_CITIES:
      return {...state,
        isLoading: false,
        error: action.error ? action.payload : null,
				cities: !action.error ? action.payload : state.cities
			}
		case GET_SUBDISTRICT:
			return {...state,
        isLoading: false,
        error: action.error ? action.payload : null,
				subdistricts: !action.error ? action.payload : state.subdistricts
			}
		case GET_COURIERS:
			return {...state,
        isLoading: false,
        error: action.error ? action.payload : null,
				couriers: !action.error ? action.payload : state.couriers
			}
		case ASYNC_START:
      return {...state, 
        isLoading: action.subtype === GET_PROVINCES || 
          action.subtype === GET_CITIES || action.subtype === GET_SUBDISTRICT
      }
		default:
      return state;
	}
};
