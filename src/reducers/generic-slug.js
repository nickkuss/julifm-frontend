import  { FETCH_CATEGORY_PRODUCTS, FETCH_PRODUCT_DETAIL, ASYNC_START, GENERIC_PAGE_UNLOADED } from '../constants';


const initialState = {
	isLoading: false,
	lastSlugType: null,
	error: null
};

export default (state = initialState, action) => {
	switch(action.type) {
		case FETCH_PRODUCT_DETAIL:
			return {...state, lastSlugType: !action.payload.error ? 'detail': state.lastSlugType,
			 isLoading: false, error: action.error ? action.payload : null }
		case ASYNC_START:
			let isType = action.subtype === FETCH_PRODUCT_DETAIL
			return { ...state, isLoading: isType ? true : false };
		case GENERIC_PAGE_UNLOADED:
			return initialState;
		default:
      return state;
	}
};
