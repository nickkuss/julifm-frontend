import { FETCH_FEED, FETCH_FEATURED, FETCH_TRENDING, ASYNC_START } from '../constants' 

const initialState = {
	feedProducts: null,
	featuredProducts: null,
	trendingProducts: null,
	feedProductsLoading: false,
	featuredProductsLoading: false,
	trendingProductsLoading: false,
	error: null,
};

export default (state = initialState, action) => {
	switch(action.type) {
		case ASYNC_START:
			let data = { ...state }
			if (action.subtype === FETCH_FEED) data.feedProductsLoading = true;
			if (action.subtype === FETCH_FEATURED) data.featuredProductsLoading = true;
			if (action.subtype === FETCH_TRENDING) data.trendingProductsLoading = true;
			return data;
		
		case FETCH_FEED:
			return {
					...state,
					error: action.error ? action.payload : null,
					feedProducts: !action.error ? action.payload: null,
					feedProductsLoading: false,
			};
		case FETCH_FEATURED:
			return {
					...state,
					error: action.error ? action.payload : null,
					featuredProducts: !action.error ? action.payload: null,
					featuredProductsLoading: false,
			};
		case FETCH_TRENDING:
			return {
					...state,
					error: action.error ? action.payload : null,
					trendingProducts: !action.error ? action.payload: null,
					trendingProductsLoading: false,
			};
		default:
		return state;
	}
};
