import { ASYNC_START, FETCH_CATEGORY_PRODUCTS, GENERIC_PAGE_UNLOADED } from '../constants'

const initialState = {
	products: null,
	error: null,
	isLoading: false
};

export default (state = initialState, action) => {
	switch (action.type) {
		case ASYNC_START:
			return { ...state, isLoading: action.subtype === FETCH_CATEGORY_PRODUCTS };
		case FETCH_CATEGORY_PRODUCTS:
			return {
				...state,
				error: action.error ? action.payload : null,
				isLoading: false,
				products: !action.error ? action.payload : null
			};
		case GENERIC_PAGE_UNLOADED:
			return initialState;
		default:
			return state;
	}
};