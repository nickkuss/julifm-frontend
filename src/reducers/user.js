import { ASYNC_START, UPDATE_USER_DATA, PROFILE_PAGE_UNLOADED, GET_TOP_UPS, TOP_UP_WALLET, GET_PAYOUTS, REQUEST_PAYOUT } from '../constants';

const initialState = {
	error: null,
  isLoading: false,
  isUpdated: false,
  topping: false,
  loadingTopups: false,
  topUps: null,
  topUpError: null,
  requesting: false,
  loadingPayouts: false,
  payouts: null,
  payoutError: null,
};

export default (state = initialState, action) => {
	switch(action.type) {
    case ASYNC_START:
			return { 
        ...state, 
        isLoading: action.subtype === UPDATE_USER_DATA,
        topping: action.subtype === TOP_UP_WALLET,
        requesting: action.subtype === REQUEST_PAYOUT,
        loadingTopups: action.subtype === GET_TOP_UPS,
        loadingPayouts: action.subtype === GET_PAYOUTS,
        isUpdated: action.subtype === UPDATE_USER_DATA ? false : state.isUpdated
      };
		case UPDATE_USER_DATA:
      return {...state, 
        error: action.error ? action.payload : null, 
        isLoading: false,
        isUpdated: !action.error
      }
    case GET_TOP_UPS:
    case GET_PAYOUTS:
      let dataProp = action.type === GET_TOP_UPS ? 'topUps' : 'payouts'
      let errorProp = action.type === GET_TOP_UPS ? 'topUpError' : 'payoutError'
      let loader = action.type === GET_TOP_UPS ? 'loadingTopups' : 'loadingPayouts'
      return {
        ...state, 
        [errorProp]: action.error ? action.payload : null,
        [dataProp]: action.error ? state[dataProp] : 
          state[dataProp] ? {
            ...state[dataProp],
            ...action.payload,
            count: state[dataProp].count + action.payload.count,
            items: [...state[dataProp].items, ...action.payload.items]
          } : action.payload,
        [loader]: false
      }
    case TOP_UP_WALLET:
    case REQUEST_PAYOUT:
      dataProp = action.type === TOP_UP_WALLET ? 'topUps' : 'payouts'
      const itemProp = action.type === TOP_UP_WALLET ? 'topUp' : 'payout'
      loader = action.type === TOP_UP_WALLET ? 'topping' : 'requesting'
      errorProp = action.type === TOP_UP_WALLET ? 'topUpError' : 'payoutError'
      if (action.error) {
        return {
          ...state,
          [errorProp]: action.payload,
          [loader]: false,
        }
      } else {
        return {
          ...state,
          [loader]: false,
          loadingUser: false,
          [dataProp]: {
            ...state[dataProp],
            items: [ action.payload[itemProp], ...state[dataProp].items],
            count: state[dataProp].count + 1,
            totalResults: state[dataProp].totalResults + 1
          }
        }
      }
    case PROFILE_PAGE_UNLOADED:
        return initialState;
    default:
      return state;
	}
};
