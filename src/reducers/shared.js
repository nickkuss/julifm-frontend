import { LOGIN, FB_LOGIN, REGISTER, ASYNC_START, ASYNC_END, 
	GET_USER_NOTIFICATIONS, TOGGLE_NOTIFICATION_MENU, GET_USER_BOOKMARKS,
	HIDE_NOTIFICATION_MENU, FETCH_CATEGORIES, LOGOUT,
	UPDATE_USER_DATA, GET_CURRENT_USER, ON_NEW_NOTIFICATION, BOOKMARKS_PAGE_UNLOADED,
	TOP_UP_WALLET, REQUEST_PAYOUT
} from '../constants';

const initialState = {
	currentUser: null,
	error: null,
	isAuthenticated: null,
	isLoading: false,
	categories: null,
	notifications: null,
	bookmarks: null,
	notificationMenuOpen: false
};

const showIndicatorForActions = [LOGIN, FB_LOGIN, REGISTER];

export default (state = initialState, action) => {
	switch(action.type) {
		case LOGIN:
		case REGISTER:
			return {
				...state,
				currentUser: !action.error ? action.payload.user : null
			};
		case GET_USER_NOTIFICATIONS:
			return {...state, 
				notifications: !action.error ? action.payload : state.notifications,
				error: action.error ? action.payload: null
			}
		case ON_NEW_NOTIFICATION:
			return {...state, 
				notifications: !action.error ? [action.payload, ...state.notifications ]: state.notifications,
				error: action.error ? action.payload: null
			}
		case TOGGLE_NOTIFICATION_MENU:
			return {...state, notificationMenuOpen: !state.notificationMenuOpen}
		case HIDE_NOTIFICATION_MENU:
			return {...state, notificationMenuOpen: false}
		case FETCH_CATEGORIES:
			return {...state, categories: !action.error ? action.payload : []}
		case GET_CURRENT_USER:
		case UPDATE_USER_DATA:
			return {
				...state,
				loadingUser: false,
				error: action.type === UPDATE_USER_DATA && action.error ? action.payload : null,
				userFetchError: action.type === GET_CURRENT_USER && action.error ? action.payload : null,
				currentUser: !action.error ? action.payload : state.currentUser
			}
		case TOP_UP_WALLET:
		case REQUEST_PAYOUT:
			return {
				...state,
				currentUser: !action.error ? action.payload.user : state.currentUser
			}
		case GET_USER_BOOKMARKS:
			return {...state, 
				bookmarks: !action.error ? action.payload : null,
				error: action.error ? action.payload: null
			}
		case BOOKMARKS_PAGE_UNLOADED:
			return {...state, bookmarks: null }
		case ASYNC_START:
			const newState = {...state}
			if (showIndicatorForActions.indexOf(action.subtype) > -1) {
				newState.isLoading =  true;
			}
			if (action.subtype === GET_CURRENT_USER) {
				newState.loadingUser = true
			}
			return newState;
		case ASYNC_END:
			if (showIndicatorForActions.indexOf(action.subtype) > -1) {
				return { ...state, isLoading: false };
			}
			return state;

			// if (showIndicatorForActions.indexOf(action.subtype) === -1 && !action.error) {
			// 	return state
			// }
			// const newState2 = {...state}
			// if (action.error) newState2.hasError = true;
			// newState2.isLoading = false;
			// return newState2;
		case LOGOUT:
			return { ...state, currentUser: null, isAuthenticated: false }	
		default:
      return state;
	}
};
