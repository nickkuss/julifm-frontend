import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import auth from './auth';
import shared from './shared';
import location from './location';
import home from './home';
import listing from './listing';
import genericSlug from './generic-slug';
import product from './product';
import user from './user';
import profile from './profile';
import cart from './cart';
import chat from './chat';
import admin from '../components/admin/reducers';
import ecommerce from '../components/catalog-listing/algolia/ecommerce/reducer';

export default combineReducers({
	auth,
	shared,
	location,
	home,
	listing,
	genericSlug,
	product,
	user,
	profile,
	cart,
	chat,
	admin,
	ecommerce,
	router: routerReducer
})

