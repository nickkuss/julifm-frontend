import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger'
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { promiseMiddleware } from './middlewares';
import reducer from './reducers';
import { routerMiddleware } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory';
import config from './config';

export const history = createHistory();

// Build the middleware for intercepting and dispatching navigation actions
const myRouterMiddleware = routerMiddleware(history);

const getMiddleware = () => {
  if (config.isDevelopment) {
    // Enable additional logging in non-production environments.
    return applyMiddleware(myRouterMiddleware, promiseMiddleware, createLogger())
  } else {
    return applyMiddleware(myRouterMiddleware, promiseMiddleware);
  }
};

export const store = createStore(
  reducer, composeWithDevTools(getMiddleware()));