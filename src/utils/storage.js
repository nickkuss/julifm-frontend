import config from '../config';

const getCurrentUser = () => {
	const user = localStorage.getItem(config.currentUserKey);
  return user ? JSON.parse(user) : null;
}

const setCurrentUser = (user) => {
  return localStorage.setItem(config.currentUserKey, JSON.stringify(user));
}

const removeCurrentUser = () => {
  return localStorage.removeItem(config.currentUserKey);
}

const getToken = () => {
  return localStorage.getItem(config.tokenKeyName);
}

const setToken = (token) => {
  return localStorage.setItem(config.tokenKeyName, token);
}

const removeToken = () => {
  return localStorage.removeItem(config.tokenKeyName);
}


const getCartId = () => {
	return localStorage.getItem(config.cartKeyName);
}

const setCartId = (cartId) => {
  return localStorage.setItem(config.cartKeyName, cartId);
}

const removeCartId = () => {
	localStorage.removeItem(config.cartKeyName);
}

export default {
	getCurrentUser,
	setCurrentUser,
	removeCurrentUser,
	getToken,
	setToken,
	removeToken,
	getCartId,
	setCartId,
	removeCartId,
}