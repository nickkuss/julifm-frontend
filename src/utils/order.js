export const getOrderInfoBySeller = (items) => {
  if (!items) return;

  const obj = {};
  items.forEach(item => {
    if (obj[item.seller.username]) {
      obj[item.seller.username].orders.push(item)
    } else {
      obj[item.seller.username] = {orders: [item], phone: item.seller.phone}
    }
  })
  return obj;
}