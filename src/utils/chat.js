import moment from 'moment';
import { momentLocale } from './app';

moment.updateLocale('en', momentLocale);

export const parseMessages = (data) =>  {
  return data.map(d => parseMessage(d))
}

export const parseMessage = (data) =>  {
  const { senderId, id, roomId, text, createdAt } = data;
    let name, avatar;
    if (data.userStore && data.userStore.store && data.userStore.store.store) {
      const msgSender = data.userStore.store.store[senderId];
      name = msgSender ? msgSender.name : null
      avatar = msgSender ? msgSender.avatarURL : null
      
    }
    return {
      id,
      // _id: id,
      text,
      roomId,
      createdAt: moment(createdAt).fromNow(true),
      user: {
        id: senderId,
        name: name || 'user',
        avatar: avatar || 
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQmXGGuS_PrRhQt73sGzdZvnkQrPXvtA-9cjcPxJLhLo8rW-sVA'
      }
    };
}

export const formatChatTimer = (data) =>  {
  return data.map(d => parseMessage(d))
}