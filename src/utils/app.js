import config from '../config';
import currencyFormatter from 'currency-formatter';
import qs from 'qs';
import { findCategoryBySlug } from './categories';

export const momentLocale = {
  relativeTime: {
    future: "in %s",
    past: "%s ago",
    s: '%ds',
    ss: '%ds',
    m: "%dm",
    mm: "%dm",
    h: "%dh",
    hh: "%dh",
    d: "%dd",
    dd: "%dd",
    M: "%dm",
    MM: "%dM",
    y: "a year",
    yy: "%d years"
  }
};



export const validateRequiredConfig = () => {
	const keys = [ 
		'baseUrl',
		'tokenKeyName',
		'currentUserKey',
		'FBAppId',
		'algoliaAppId',
		'algoliaApiKey',
		'algoliaProductIndexName',
	]; 

	const configItems = Object.keys(config);

	keys.forEach(key => {
		if (configItems.indexOf(key) === -1) {
			throw new Error('KeyNotSet: ' + key);
		}
	})
}

export const isObject = (o) => o instanceof Object && o.constructor === Object;

export const formatDate = (dateString) => {
	const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	const date = new Date(dateString);

	if (date === 'Invalid Date') return null;

	let day = date.getDate();
	day = day < 10 ? ('0' + day) : day;
	const monthIndex = date.getMonth();
	const year = date.getFullYear();
	
	return day + ' ' + monthNames[monthIndex] + ' ' + year;  
}

export const formatQueryString = () => {
	const search = window.location.search.slice(1)
	const parsed = qs.parse(search)
}

export const getCategoryFromUrlSlug = (categories, slug) => {
	const { pathname } = window.location;
	const categorySlug = slug || pathname.split('catalog/')[1]

	if (!categories || !categorySlug) return null

	const category = findCategoryBySlug(categories, categorySlug)
	return category ? category.name : ''
}

export const searchInitData = (categories) => {
	const initData = qs.parse(window.location.search.slice(1));
  if (initData.toggle) {
    initData.toggle.selfDelivery =
			initData.toggle.selfDelivery === 'true' ? true : undefined;
		initData.toggle.isAuthentic =
      initData.toggle.isAuthentic === 'true' ? true : undefined;
	}

	// const category = getCategoryFromUrlSlug(categories)
	// if (category) {
	// 	if (initData.refinementList) initData.refinementList.category = [category]
	// 	else initData.refinementList = { category : [category] }
	// }
  return initData;
}

export const searchStateToUrl = (state) => {
	const obj = {};
	if (obj.multiRange) {

	}
	if (state.range) {
		Object.keys(state.range).map(key => {
			obj[key] = `${state.range[key].min || ''}-${state.range[key].max|| ''}`
		})
		// if (state.range.price) obj.price = `${obj.price.min || ''}-${obj.price.max || ''}`
		// if (state.range.rating) obj.rating = `${obj.rating.min || ''}-${obj.rating.max || ''}`
	}

	if (state.refinementList) {
		Object.keys(state.refinementList).map(key => {
			obj[key] = `${state.range[key].min || ''}-${state.range[key].max|| ''}`
		})
	}

}

export const formatAmount = (value, code, precision) => currencyFormatter.format(
  value, {
		code: code || config.defaultCurrencyCode,
		precision: precision ||config.defaultCurrencyPrecision
	}
);

export const pageTitleText = (title) => `${config.appName} ${title ? '| ' + title : ''}`

