import slugify from './slugify';

const addSlug = (categories) => {
	categories.forEach(category => {
		category.slug = category.slug || slugify(category.name);
		
		if (category.subs) {
			category.subs.forEach(child => {
				child.slug = child.slug || slugify(child.name);

				if (child.subs) {
					child.subs.forEach(grandChild => {
						grandChild.slug = grandChild.slug || slugify(grandChild.name);						
					})
				}
			})
		}
	});
	return categories;
}

const findCategoryByProp = (prop = 'slug', categories, propValue, returnParent = false) => {
	const propLower = propValue.toLowerCase();
	let _category, _parent; // useful to track the particular category since Array.find return topMost item found
	const categoryFound = categories.find(category => {
		_category = category; 
		_parent = null; // level 1 has no parent

		let isFound = category[prop] && category[prop].toLowerCase() === propLower;
		if (isFound) return true; // break out of loop

		if (category.subs) {
			isFound = category.subs.find(child => {
				_category = child;
				_parent =  category;
				isFound = child[prop].toLowerCase() === propLower;
				
				if (isFound) return true; // break out of loop

				if (child.subs) {
					_category = child.subs.find(grandChild => grandChild[prop].toLowerCase() === propLower);
					_parent = _category ? child : _parent;
					return _category; // we found in grandChild loop;
				}
				return isFound;
			})
			return isFound; // we found in child loop;
		}
		return isFound
	});

	return categoryFound ? (returnParent ? _parent : _category) : ''
}

export const findCategoryBySlug = (categories, slug, returnParent = false) => {
	return findCategoryByProp('slug', categories, slug, returnParent)
}

export const findCategoryByName = (categories, name, returnParent = false) => {
	return findCategoryByProp('originalName', categories, name, returnParent)
}

export const findCategoryById = (categories, id, returnParent = false) => {
	return findCategoryByProp('id', categories, id, returnParent)
}

const getCrumb = (categories, slug, product = null) => {
	const crumb = [{slug: '', name: 'Home'}];
	let parent = true;
	let currentSlug = slug;

	// find parent of this category, until no more parent
	while (parent && currentSlug) {
		parent = findCategoryBySlug(categories, currentSlug, true);
		if (parent) {
			currentSlug = parent.slug;
			crumb.splice(1, 0, parent);
		}
	}

	// add this particular category to the end
	const category = findCategoryBySlug(categories, slug)
	if (category) {
		crumb.push(category);
	}

	if (product) {
		const name = product.name.length > 50 ? product.name.slice(0, 50) + ' ...' : product.name;
		crumb.push({name, slug: product.slug});
	}
	
	return crumb;
}

const isFashion = (categories, category) => {
	if (!categories || !categories.length || !category) return false;

	if (category.toLowerCase() === 'fashion') return true;

	const fashion = categories.find(c => c.name.toLowerCase() === 'fashion')
	if (!fashion || !fashion.subs) return false;

	return fashion.subs.find(item => {
		if (item.name.toLowerCase() === category.toLowerCase()) return true;

		if (item.subs) return item.subs.find(i => item.name.toLowerCase() === i.toLowerCase())
		
		return false
	})
}


export default { addSlug, findCategoryBySlug, getCrumb, isFashion };