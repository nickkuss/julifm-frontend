import moment from 'moment';
import { momentLocale } from './app';

moment.updateLocale('en', momentLocale);

export const getProductQtyInCart = (cartItems, product) => {
  if (!Array.isArray(cartItems) || !product) return 0;

  const item =  cartItems.find(i => i.id === product.id || product.objectID)
  return item ? item.quantity : 0;
}

export const formatWeight = (weight) => {
  if (!weight) return  ''
  return (weight / 1000).toFixed(2) + 'kg'
}

export const formatDateFromNow = (date) => moment(date).fromNow(true)