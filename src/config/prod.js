const baseUrl = 'https://juli.fm/api/v1/';

export default {
	baseUrl,
	FBAppId: '331219754117055',
	pusherKey: 'a2cb3475f7b2c6ec2d62',
	pusherBidChannel: 'auction-bid',
	pusherBidEvent: 'new-bid',
	pusherNotificationEvent: 'notification',
  pusherCluster: 'ap1',
	pusherAuthEndpoint: `${baseUrl}auth/pusher`, 
	pusherChatAuthEndpoint: `${baseUrl}auth/chat`,
	pusherChatInstanceLocator: 'v1:us1:26086d07-aba6-42f8-b215-3e8d1e21069e',
	midTransKey: 'Mid-client-3liVR7gNO9cM6cCN',
	algoliaAppId: 'FDPIRIQ853',
	algoliaApiKey: '62c5b2f92f14877a7446dc6d193fe5e1',
	algoliaProductIndexName: 'prod_products',
	algoliaUserIndexName: 'prod_users',
}