import dev from './dev';
import stage from './stage';
import prod from './prod';

let config = {
	appName: 'Juli',
	searchInputPlaceholder: 'Search product, category or user',
	tokenKeyName: 'juli_token',
	currentUserKey: 'currentUser',
	homePageTitle: 'Shop with confidence',
	cartKeyName: 'juli_cart',
	defaultCurrencyCode: 'IDR',
	defaultCurrencyPrecision: 0,
	starRatingColor: '#da8c26',
	maxNumOfStars: 5,
	adminPerPage: 50,
	categoryFileMaxSize: 20000, // 20kb
	adminUrl: 'juli1en3o',
	fashionSizes: ['S', 'M', 'L', 'XL', 'XXL'],
	productImageFileTypes: '.png, .jpeg, .jpg, .gif',
	minProductAmount: 10000,
	minBidIncrement: 10000,
	minTopUpAmount: 10000,
	maxTopUpAmount: 200000,
	midTransUrl: 'https://api.sandbox.midtrans.com/v2/token',
	numChatItems: 20,
	reportReasons: [
		'I got another item',
		'Item is not in good condition',
		'Delivery location is incorrect'
	]
}


switch(process.env.REACT_APP_ENV) {
	case 'prod':
		config = {...config, ...prod}
		break;
	case 'stage':
		config = {...config, ...stage}
		break;
	case 'dev':
		config = {...config, ...dev}
		break;
	default:
		config = {...config, ...dev}
}

console.log('confgggggg', config)

export default config;