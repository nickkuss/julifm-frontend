const baseUrl = 'https://boiling-mountain-63569.herokuapp.com/api/v1/';

export default {
	baseUrl,
	FBAppId: '1557811061190451',
	pusherKey: 'b137c45be17354a52e8c',
	pusherBidChannel: 'auction-bid',
	pusherBidEvent: 'new-bid',
	pusherNotificationEvent: 'notification',
  pusherCluster: 'ap1',
	pusherAuthEndpoint: `${baseUrl}auth/pusher`,
	pusherChatAuthEndpoint: `${baseUrl}auth/chat`,
	pusherChatInstanceLocator: 'v1:us1:b5610ba6-1f12-44c3-a801-399dbcb65e05',
	midTransKey: 'SB-Mid-client-LkeBJOYa6oWIPa0T',
	algoliaAppId: 'FDPIRIQ853',
	algoliaApiKey: '62c5b2f92f14877a7446dc6d193fe5e1',
	algoliaProductIndexName: 'test_products',
	algoliaUserIndexName: 'test_users',
}