const baseUrl = 'https://boiling-mountain-63569.herokuapp.com/api/v1/';

export default {
	baseUrl,
	isDevelopment: true,
	FBAppId: '1557811061190451',
	pusherKey: '62a4322ce3febace82a4',
	pusherBidChannel: 'auction-bid',
	pusherBidEvent: 'new-bid',
	pusherNotificationEvent: 'notification',
  pusherCluster: 'ap1',
	pusherAuthEndpoint: `${baseUrl}auth/pusher`,  
	pusherChatAuthEndpoint: 'https://us1.pusherplatform.io/services/chatkit_token_provider/v1/01b16ea0-7d75-49ae-9440-083af7e854ea/token',
	pusherChatInstanceLocator: 'v1:us1:01b16ea0-7d75-49ae-9440-083af7e854ea',
	midTransKey: 'SB-Mid-client-LkeBJOYa6oWIPa0T',
	algoliaAppId: 'FDPIRIQ853',
	algoliaApiKey: '62c5b2f92f14877a7446dc6d193fe5e1',
	algoliaProductIndexName: 'dev_products',
	algoliaUserIndexName: 'dev_users',
	supportId: '5c24f5fbf7eff8f3c2469456'
}